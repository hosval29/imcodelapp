package com.project.sivad.Configuracion.Seguridad.View;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.pd.chocobar.ChocoBar;
import com.project.sivad.Configuracion.Menu.View.ViewMenu;
import com.project.sivad.Configuracion.Seguridad.Interfaces.InterfacesLogin;
import com.project.sivad.Configuracion.Seguridad.Presenter.PresenterLogin;
import com.project.sivad.Configuracion.Seguridad.Vendor.FragmentValidateCodeConfig;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoCargaFurgones;
import com.project.sivad.R;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.SessionManager;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseDataCode;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseLoginUser;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A login screen that offers login via email/password.
 */
public class ViewLogin extends AppCompatActivity implements
        InterfacesLogin.InterfacesViewLogin
        , FragmentValidateCodeConfig.InterfacesFragmentValidateCodeSetting
        , SnackBarPersonalized.InterfacesResponseSnackbarWithAction {

    //Declaramos las Variables XML
    @BindView(R.id.idToolbarLogin)
    Toolbar idToolbarLogin;
    @BindView(R.id.idAppCompatImageViewLogo)
    AppCompatImageView idAppCompatImageViewLogo;
    @BindView(R.id.idCollapsingToolbarLayoutLogin)
    CollapsingToolbarLayout idCollapsingToolbarLayoutLogin;
    @BindView(R.id.idAppBarLayoutLogin)
    AppBarLayout idAppBarLayoutLogin;
    @BindView(R.id.idTextInputLayoutUser)
    TextInputLayout idTextInputLayoutUser;
    @BindView(R.id.idTextInputLayoutPass)
    TextInputLayout idTextInputLayoutPass;
    //@BindView(R.id.idAppCompatCheckBoxRemember)
    //AppCompatCheckBox idAppCompatCheckBoxRemember;
    //@BindView(R.id.idMaterialButtonRecordarPass)
    //MaterialButton idMaterialButtonRecordarPass;
    @BindView(R.id.idAppCompatButtonIngresar)
    AppCompatButton idAppCompatButtonIngresar;
    @BindView(R.id.idLinearLayoutFormSigIn)
    LinearLayoutCompat idLinearLayoutFormSigIn;
    @BindView(R.id.login_form)
    NestedScrollView loginForm;
    @BindView(R.id.idTextViewIconCodeSetting)
    AppCompatTextView idTextViewIconCodeSetting;
    @BindView(R.id.idTextInputLayoutCodeSetting)
    TextInputLayout idTextInputLayoutCodeSetting;
    @BindView(R.id.idFloatingActionButtonCodeSetting)
    FloatingActionButton idFloatingActionButtonCodeSetting;
    @BindView(R.id.idAppCompatTextViewIconUser)
    AppCompatTextView idAppCompatTextViewIconUser;
    @BindView(R.id.idAppCompatTextviewIconPass)
    AppCompatTextView idAppCompatTextviewIconPass;

    //Intanciamos la DB
    private DBSivad dbSivad;

    //Instanciamos las Utilizadades
    private Context context;
    private ResponseDataCode responseDataCode;
    ProgressDialog progressDialog;
    private SessionManager session;
    private SnackBarPersonalized snackBarPersonalized;

    //Instanciamos la Interfaces Presenter
    InterfacesLogin.InterfacesPresenterLogin interfacesPresenterLogin;

    //Declaramos las variables tipo String y Int
    private String imei = "";
    private String tokenFirebase = "";
    private final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private int networkState;

    //Declaramos las variables tipo booloean
    private boolean isFragment;
    public static boolean isConnect = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_login);

        //Inicializamo las referencias xml con la view
        ButterKnife.bind(this);

        //Inicializamos los Objetos
        context = this;
        dbSivad = new DBSivad(context);
        session = new SessionManager(context);
        snackBarPersonalized = new SnackBarPersonalized(ViewLogin.this);
        progressDialog = new ProgressDialog(context);
        if (dbSivad.getDataCodeSetting() == null) {
            dbSivad.addDataCodeSetting("null", "null");
            //dbSivad.addDataLoginUser("null", "null");
            //dbSivad.addDataPrueba("null", "null");
        }

        interfacesPresenterLogin = new PresenterLogin(null, this, context);

        //Obtenemos imei
        imei = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        //Manejamos las eventualidades de las variables XML
        idTextInputLayoutCodeSetting.getEditText().setEnabled(false);
        idFloatingActionButtonCodeSetting.setEnabled(false);

        idFloatingActionButtonCodeSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragmentValidateCode();
            }
        });

        idAppBarLayoutLogin.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    idCollapsingToolbarLayoutLogin.setTitleEnabled(true);
                    idCollapsingToolbarLayoutLogin.setCollapsedTitleTextColor(getResources().getColor(R.color.colorTextPrimary));
                    idCollapsingToolbarLayoutLogin.setTitle("Inicio de Sesión - SIVAD");
                } else {
                    idCollapsingToolbarLayoutLogin.setTitleEnabled(true);
                    idCollapsingToolbarLayoutLogin.setTitle("");
                }
            }
        });

        idTextInputLayoutUser.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                idTextInputLayoutUser.setError("");
                idTextInputLayoutUser.setErrorEnabled(false);
                idAppCompatTextViewIconUser.setTextColor(getResources().getColor(R.color.colorSecondary));
                if (idTextInputLayoutPass.isErrorEnabled()) {
                    idTextInputLayoutPass.setErrorEnabled(false);
                    idAppCompatTextviewIconPass.setTextColor(getResources().getColor(R.color.colorSecondary));
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutPass.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                idTextInputLayoutPass.setError("");
                idTextInputLayoutPass.setErrorEnabled(false);
                idAppCompatTextviewIconPass.setTextColor(getResources().getColor(R.color.colorSecondary));
                if (idTextInputLayoutUser.isErrorEnabled()) {
                    idTextInputLayoutUser.setErrorEnabled(false);
                    idAppCompatTextViewIconUser.setTextColor(getResources().getColor(R.color.colorSecondary));
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idAppCompatButtonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateInputs()) {
                    String user = idTextInputLayoutUser.getEditText().getText().toString().trim();
                    String pass = idTextInputLayoutPass.getEditText().getText().toString().trim();
                    logigUser(user, pass);
                }
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Token", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        tokenFirebase = task.getResult().getToken();
                        Log.v("token", tokenFirebase);

                    }
                });
        Log.v("estoy aqui", "onCreate");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("estoy aqui", "onStart");
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onResume() {
        super.onResume();

        Log.v("estoy aqui", "onResume");

        registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        //Log.v("responseLoginUser", responseLoginUser.toString());

        if (responseLoginUser != null) {
            Intent intent = new Intent(this, ViewMenu.class);
            startActivity(intent);
            finish();
        } else {
            ResponseDataCode responseDataCode = dbSivad.getDataCodeSetting();
            //Log.v("responseDataCode", responseDataCode.toString());
            if (responseDataCode == null) {
                loadFragmentValidateCode();
            } else {
                String codeConnect = responseDataCode.getData().getConnections().getCode();
                idTextInputLayoutCodeSetting.getEditText().setText(codeConnect);
                idFloatingActionButtonCodeSetting.setEnabled(true);
                idTextViewIconCodeSetting.setText(R.string.fa_check);
                idTextViewIconCodeSetting.setTextColor(getResources().getColor(R.color.colorSecondary));
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("estoy aqui", "onPause");
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Log.v("estoy aqui", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Log.v("estoy aqui", "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Log.v("estoy aqui", "onDestroy");
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    /**
     * @method loadFragmentValidateCode()
     * Lanza al fragment CodeSetting para el ingreso del codigo de configuracion
     */
    private void loadFragmentValidateCode() {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFrag;
        dialogFrag = new FragmentValidateCodeConfig();
        dialogFrag.setCancelable(false);
        dialogFrag.show(ft, "dialog");

    }

    @Override
    public void interfacesFragmentValidateCodeSetting(ResponseDataCode responseDataCode) {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreated = ft.format(date).toString();
        Gson gson = new Gson();
        String json = gson.toJson(responseDataCode);
        int idCodeSettingDelete = dbSivad.deleteDataCodeSetting();
        Log.v("idCodeSettingDelete", String.valueOf(idCodeSettingDelete));
        Long id = dbSivad.addDataCodeSetting(dateCreated, json);
        Log.v("idCodeSettingAdd", String.valueOf(id));
        if (id != null) {
            String codeConnect = responseDataCode.getData().getConnections().getCode();
            //Log.v("codeConnect", codeConnect);

            idTextInputLayoutCodeSetting.getEditText().setText(codeConnect);
            idFloatingActionButtonCodeSetting.setEnabled(true);
            idTextViewIconCodeSetting.setText(R.string.fa_check);
            idTextViewIconCodeSetting.setTextColor(getResources().getColor(R.color.colorSecondary));
        }

        showSnackBarResponse(responseDataCode.getCodeStatus(), responseDataCode.getMessage());
    }

    private boolean validateInputs() {
        boolean error = false;
        if (idTextInputLayoutUser.getEditText().getText().toString().trim().matches("")) {
            idTextInputLayoutUser.setError("Campo Usuario obligatorio");
            idAppCompatTextViewIconUser.setTextColor(this.getResources().getColor(R.color.colorErrorDark));
            error = true;
        }

        if (idTextInputLayoutPass.getEditText().getText().toString().trim().matches("")) {
            idTextInputLayoutPass.setError("Campo Contraseña obligatorio");
            idAppCompatTextviewIconPass.setTextColor(this.getResources().getColor(R.color.colorErrorDark));
            error = true;
        }

        return error;
    }

    private void logigUser(String user, String pass) {
        if (isConnect) {
            progressDialog.setMessage(ConstantsStrings.LOAD);
            progressDialog.show();
            interfacesPresenterLogin.getDataLoginUser(user, pass, imei, tokenFirebase);
        } else {
            snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
        }
    }

    @Override
    public void responseSuccessGetDataLoginUser(ResponseLoginUser responseLoginUser) {
        //Log.v("responseLoginUser", responseLoginUser.toString());
        //Log.v("responseLoginUser", responseLoginUser.getData().toString());
        progressDialog.dismiss();
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreated = ft.format(date).toString();
        Gson gson = new Gson();
        String json = gson.toJson(responseLoginUser);
        int fileDeletesLoginUser = dbSivad.deleteDataLoginUser();
        Log.v("fileDeleteLoginUser", String.valueOf(fileDeletesLoginUser));
        Long idAddLoginUser = dbSivad.addDataLoginUser(dateCreated, json);
        Log.v("idAddLoginUser", String.valueOf(idAddLoginUser));
        //Indicamos que la sessións e ha realizado
        session.setLogin(true);
        Intent intent = new Intent(this, ViewMenu.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void responseErrorGetDataLoginUser(ResponseError responseError) {
        //Log.v()
        progressDialog.dismiss();
        showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }


    private void showSnackBarResponse(int code, String message) {
        Log.v("code", String.valueOf(code));
        switch (code) {
            case 200:
                ChocoBar.builder().setActivity(ViewLogin.this)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .build()  // in built green ChocoBar
                        .show();
                break;
            case 201:
                ChocoBar.builder().setActivity(ViewLogin.this)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .green()  // in built green ChocoBar
                        .show();
                break;
            case 400:
                ChocoBar.builder().setActivity(ViewLogin.this)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .orange()  // in built green ChocoBar
                        .show();
                break;
            case 404:
                ChocoBar.builder().setActivity(ViewLogin.this)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .orange()  // in built green ChocoBar
                        .show();
                break;
            case 500:
                ChocoBar.builder().setActivity(ViewLogin.this)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .red()  // in built green ChocoBar
                        .show();
                break;
        }

    }

    @Override
    public void responseSnackbarWithAction() {

    }
}


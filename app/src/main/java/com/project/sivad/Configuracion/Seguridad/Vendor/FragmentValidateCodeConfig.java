package com.project.sivad.Configuracion.Seguridad.Vendor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.project.sivad.Configuracion.Seguridad.Interfaces.InterfacesLogin;
import com.project.sivad.Configuracion.Seguridad.Presenter.PresenterLogin;
import com.project.sivad.Configuracion.Seguridad.View.ViewLogin;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseError;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentValidateCodeConfig extends DialogFragment implements InterfacesLogin.InterfacesViewFragmentCodeSetting {

    //Declaramos las variables XML
    @BindView(R.id.linearLayoutCompat)
    LinearLayoutCompat linearLayoutCompat;
    @BindView(R.id.idTextInputLayoutCodeConfig)
    TextInputLayout idTextInputLayoutCodeConfig;
    @BindView(R.id.linearLayoutCompat2)
    LinearLayoutCompat linearLayoutCompat2;
    @BindView(R.id.idBtnAceptar)
    AppCompatButton idBtnAceptar;
    @BindView(R.id.idAppCompatTextviewError)
    AppCompatTextView idAppCompatTextviewError;
    @BindView(R.id.idLinearLayoutError)
    LinearLayoutCompat idLinearLayoutError;
    @BindView(R.id.idBtnCancelar)
    MaterialButton idBtnCancelar;

    //Intanciasmos las DB
    private Context context;
    private DBSivad dbSivad;

    //Intanciamos la Interfaces Presenter
    private InterfacesLogin.InterfacesPresenterLogin interfacesPresenterLogin;
    InterfacesFragmentValidateCodeSetting interfacesFragmentValidateCodeSetting;

    public interface InterfacesFragmentValidateCodeSetting {
        void interfacesFragmentValidateCodeSetting(ResponseDataCode responseDataCode);
    }

    //Instanciamos los Object Utils
    private ResponseDataCode responseDataCode;
    private ProgressDialog progressDialog;


    public static FragmentValidateCodeConfig newInstance(String param1, String param2) {
        FragmentValidateCodeConfig fragment = new FragmentValidateCodeConfig();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            interfacesFragmentValidateCodeSetting = (InterfacesFragmentValidateCodeSetting) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_validate_code_config, container, false);
        ButterKnife.bind(this, view);

        context = this.getContext().getApplicationContext();
        progressDialog = new ProgressDialog(getContext());
        interfacesPresenterLogin = new PresenterLogin(this, null, context);
        dbSivad = new DBSivad(context);
        ResponseDataCode responseDataCode = dbSivad.getDataCodeSetting();
        if (responseDataCode != null) {
            idTextInputLayoutCodeConfig.getEditText().setText(responseDataCode.getData().getConnections().getCode());
            idBtnCancelar.setVisibility(View.VISIBLE);
        }

        idTextInputLayoutCodeConfig.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Log.d("1", "Estoa aqui");
                idTextInputLayoutCodeConfig.setErrorEnabled(false);
                idLinearLayoutError.setVisibility(View.GONE);
                idAppCompatTextviewError.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Log.d("2", "Estoa aqui");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Log.d("3", "Estoa aqui");
            }
        });

        idBtnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (idTextInputLayoutCodeConfig.getEditText().getText().toString().matches("")) {
                    idTextInputLayoutCodeConfig.setError("Campo Código Obligatorio");
                } else if (idTextInputLayoutCodeConfig.getEditText().getText().toString().trim().length() != 6) {
                    idTextInputLayoutCodeConfig.setError("Número de caracteres invalidos");
                } else {
                    String codeSetting = idTextInputLayoutCodeConfig.getEditText().getText().toString().trim();
                    if (ViewLogin.isConnect){
                        progressDialog.setMessage(ConstantsStrings.LOAD);
                        progressDialog.show();
                        interfacesPresenterLogin.postDataCodeSetting(codeSetting);
                    }else{
                        idLinearLayoutError.setVisibility(View.VISIBLE);
                        idAppCompatTextviewError.setText(ConstantsStrings.NO_CONEXCION_INTERNET);
                    }

                }
            }
        });

        idBtnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

    @Override
    public void responseErrorGetDataCodeSetting(ResponseError responseError) {
        //Log.d("responseError", responseError.toString());
        progressDialog.dismiss();
        idLinearLayoutError.setVisibility(View.VISIBLE);
        idAppCompatTextviewError.setText(responseError.getMessage());
    }

    @Override
    public void responseSuccessgetDataCodeSetting(ResponseDataCode responseDataCode) {
        //Log.v("responseDataCode", responseDataCode.toString());
        //Log.v("response", responseDataCode.getData().toString());
        progressDialog.dismiss();
        interfacesFragmentValidateCodeSetting.interfacesFragmentValidateCodeSetting(responseDataCode);
        dismiss();
    }
}

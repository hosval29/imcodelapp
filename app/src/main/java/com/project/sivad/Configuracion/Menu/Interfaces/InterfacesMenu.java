package com.project.sivad.Configuracion.Menu.Interfaces;

import com.project.sivad.http.Response.ResponseDataNotificationByVehicle;
import com.project.sivad.http.Response.ResponseDataNoveltiesInspections;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseSuccessUpdateMeleage;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONObject;

import java.util.List;

public interface InterfacesMenu {
    interface InterfacesViewMenu {
        void responseErrorPutSignOut(ResponseError responseError);

        void responseSuccessPutSignOut(ResponseJson responseJson);

    }

    interface InterfaceFragmentHome {
        void responseErrorGetDataTypesVehicularInspections(ResponseError responseError);

        void responseSuccesGetDataTypesVehicularInspections(ResponseDataTypesVehicularInspectios responseDataTypesVehicularInspectios);

        void responseErrorGetValidateInspectionEnabled(ResponseError responseError);

        void responseSuccessGetValidateInspectionEnabled(ResponseValideteInspectionEnabled responseValideteInspectionEnabled);

        /*void responseErrorPutInspectionEnabled(ResponseError responseError);

        void responseSuccessPutInspectionEnabled(ResponseJson responseJson);*/
    }

    interface InterfacesFragmentProfile {
        void responseErrorPutDataProfile(ResponseError responseError);

        void responseSuccesPutDataProfile(ResponseLoginUser responseLoginUser);
    }

    interface InterfacesFragmentValidatePlateMeleage {
        void responseErrorGetDataPlacaKilometraje(ResponseError responseError);

        void responseSuccessGetDataPlacaKilometraje(ResponseValidatePlateMileage responseValidatePlateMileage);
    }

    interface InterfacesFragmnetNotifications {
        void responseErrorGetDataNotificationByVehicle(ResponseError responseError);

        void responseSuccessGetDataNotificationByVehicle(ResponseDataNotificationByVehicle responseDataNotificationByVehicle);
    }

    interface InterfacesFragmentNovelties {
        void responseErrorGetDataNoveltiesInspection(ResponseError responseError);

        void responseSuccessGetDataNoveltiesInspection(ResponseDataNoveltiesInspections responseDataNoveltiesInspections);
    }

    interface InterfacesFragmentOffile {
        void responseErrorPostDataOffline(ResponseError responseError);

        void responseSuccessPostDataOfflineVehicleInspection(ResponseVehicleInspecion responseVehicleInspecion);

        void responseSuccessPostDataOfflineNoveltiesInspection(ResponseJson responseJson);

        void responseSuccessPostDataOfflineNotificationAlertByToggleBad(ResponseJson responseJson);

        void responseSuccessPostDataOfflineUMileageUpdate(ResponseValidatePlateMileage responseValidatePlateMileage);
    }

    interface InterfaceFragmenUpdateMeleage {
        void responseErrorPutDataMeleage(ResponseError responseError, JSONObject jsonObject);

        void responseSuccessPutDataMeleade(ResponseValidatePlateMileage responseValidatePlateMileage);
    }

    interface InterfacesAdapterMenuItem{
        void validateInspectionEnabled(String plateVehicle, int numRowInspectionsPerformed, int numInspectionsVehicles);

        //void updateInspectionEnabled(String plate);
    }

    interface InterfacesPresenterMenu {
        void responseErrorGetDataPlacaKilometraje(ResponseError responseError);

        void responseSuccessGetDataPlacaKilometraje(ResponseValidatePlateMileage responseValidatePlateMileage);

        void getDataPlacaKilometraje(String plate, int mileage);

        void responseErrorPutDataProfile(ResponseError responseError);

        void responseSuccesPutDataProfile(ResponseLoginUser responseLoginUser);

        void putDataProfile(int idUser
                , String name
                , String lastName
                , String email
                , int phone
                , String passwordOld
                , String passwordNew
                , int idUserSession
                , int idHistoryLog);

        //Metodos Interfaces ViewMenu SignOut
        void responseErrorPutSignOut(ResponseError responseError);

        void responseSuccessPutSignOut(ResponseJson responseJson);

        void putDataSignOut(int idUser, int idUserSession, int idHistoryLog);

        //Metodos Interfaces Fragment Notifications
        void responseErrorGetDataNotificationByVehicle(ResponseError responseError);

        void responseSuccessGetDataNotificationByVehicle(ResponseDataNotificationByVehicle responseDataNotificationByVehicle);

        void getDataNotificationsByVehicle(int idVehicle);

        //Metodos Interfaces Fragment Novelties
        void responseErrorGetDataNoveltiesInspection(ResponseError responseError);

        void responseSuccessGetDataNoveltiesInspection(ResponseDataNoveltiesInspections responseDataNoveltiesInspections);

        void getDataNoveltiesInspections(int idVehicle);

        //Metodos Interfaces Fragment Offline
        void responseErrorPostDataOffline(ResponseError responseError);

        void responseSuccessPostDataOfflineVehicleInspection(ResponseVehicleInspecion responseVehicleInspecion);

        void responseSuccessPostDataOfflineNoveltiesInspection(ResponseJson responseJson);

        void responseSuccessPostDataOfflineNotificationAlertByToggleBad(ResponseJson responseJson);

        void responseSuccessPostDataOfflineUMileageUpdate(ResponseValidatePlateMileage responseValidatePlateMileage);

        void postDataOffline(int idRegister, String jsonObject, String nameModule);

        //Metodos Interfaces Fragment Home
        void responseErrorGetDataTypesVehicularInspections(ResponseError responseError);

        void responseSuccesGetDataTypesVehicularInspections(ResponseDataTypesVehicularInspectios responseDataTypesVehicularInspectios);

        void getDataTypesVehicularInspection();

        void responseErrorGetValidateInspectionEnabled(ResponseError responseError);

        void responseSuccessGetValidateInspectionEnabled(ResponseValideteInspectionEnabled responseValideteInspectionEnabled);

        void getValidateInspectionEnabled(String plateVehicle);

        //void putInspectionEnabled(String plateVehicle);

        //Metodo Interface Fragment UpdateMeleage
        void responseErrorPutDataMeleage(ResponseError responseError, JSONObject jsonObject);

        void responseSuccessPutDataMeleade(ResponseValidatePlateMileage responseValidatePlateMileage);

        void putDataMeleage(String dateRegister, int idUser, int idVehicle, int meleage);
    }

    interface InterfacesModelMenu {
        void getDataPlacaKilometraje(String plate, int mileage);

        void putDataProfile(int idUser
                , String name
                , String lastName
                , String email
                , int phone
                , String passwordOld
                , String passwordNew
                , int idUserSession
                , int idHistoryLog);

        //Metodos Interfaces ViewMenu SignOut
        void putDataSignOut(int idUser, int idUserSession, int idHistoryLog);

        //Metodos Interfaces Fragment Notifications
        void getDataNotificationsByVehicle(int idVehicle);

        //Metodos Interfaces Fragment Novelties
        void getDataNoveltiesInspections(int idVehicle);

        //Metodos Interfaces Fragment Offline
        void postDataOffline(int idRegister, String jsonObject, String nameModule);

        //Metodos Interface Fragment Home
        void getDataTypesVehicularInspection();

        void getValidateInspectionEnabled(String plateVehicle);

        //Metodos Interface Fragment UpdateMeleage
        void putDataMeleage(String dateRegister, int idUser, int idVehicle, int meleage);
    }

    interface InterfacesSendDataOfflineById {
        void sendDataOfflineById(int position);
    }


}

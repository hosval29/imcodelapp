package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.transition.Fade;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Adapter.AdapterFragmentNovelties;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataNovelties;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseDataNoveltiesInspections;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentNovedades extends Fragment implements AdapterFragmentNovelties.OnClickListener
        , InterfacesMenu.InterfacesFragmentNovelties {

    //Variables XML
    @BindView(R.id.idRecyclerViewFragmentNovelties)
    RecyclerView idRecyclerViewFragmentNovelties;
    @BindView(R.id.idLinearLayoutNoNovelties)
    LinearLayoutCompat idLinearLayoutNoNovelties;

    //Utils Object
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Declaramos las variables String y int
    private int viewPosition;
    private int networkState;

    //Declaramos las variables boolean
    private boolean launchedActivity;
    public static boolean isConnect = false;

    //Declaramos las variables de rvCalendario
    private List<PojoDataNovelties> pojoDataNoveltiesList = new ArrayList<>();
    private AdapterFragmentNovelties adapterFragmentNovelties;
    private LinearLayoutManager linearLayoutManager;

    //Instanciamos la Interfaces
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        dbSivad = new DBSivad(context);
        progressDialog = new ProgressDialog(context);
        snackBarPersonalized = new SnackBarPersonalized(getActivity());
        interfacesPresenterMenu = new PresenterMenu(null
                , null
                , null
                , null
                , this
                , null
                , null
                , null
                , context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_novedades, container, false);
        ButterKnife.bind(this, view);
        setupRecyclerView();
        setupTransitions();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && launchedActivity) {
            //startRippleTransitionUnreveal();
            launchedActivity = false;
        }
        getDataNoveltiesInspections();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            //snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    @Override
    public void onItemClick(View sharedView, String transitionName, int position) {
        viewPosition = position;
    }


    private void setupTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setExitTransition(new Fade());
    }

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        idRecyclerViewFragmentNovelties.setHasFixedSize(true);
        idRecyclerViewFragmentNovelties.setLayoutManager(linearLayoutManager);
        idRecyclerViewFragmentNovelties.setItemAnimator(new DefaultItemAnimator());
        adapterFragmentNovelties = new AdapterFragmentNovelties(pojoDataNoveltiesList, this::onItemClick);
        idRecyclerViewFragmentNovelties.setAdapter(adapterFragmentNovelties);
    }

    private void getDataNoveltiesInspections() {
        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        if (responseValidatePlateMileage != null) {
            if (isConnect){
                progressDialog.setMessage(ConstantsStrings.LOAD);
                progressDialog.show();
                interfacesPresenterMenu.getDataNoveltiesInspections(responseValidatePlateMileage.getData().getIdVehicle());
            }else{
                snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
            }
        }
    }

    @Override
    public void responseErrorGetDataNoveltiesInspection(ResponseError responseError) {
        progressDialog.dismiss();
        idLinearLayoutNoNovelties.setVisibility(View.VISIBLE);
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccessGetDataNoveltiesInspection(ResponseDataNoveltiesInspections responseDataNoveltiesInspections) {
        progressDialog.dismiss();
        pojoDataNoveltiesList.clear();
        JsonArray jsonArrayDataNoveltiesInspections = responseDataNoveltiesInspections.getData();
        for (int i = 0; i < jsonArrayDataNoveltiesInspections.size(); i++) {
            JsonObject jsonObjectNoveltieInspection = jsonArrayDataNoveltiesInspections.get(i).getAsJsonObject();
            PojoDataNovelties pojoDataNovelties = new PojoDataNovelties();
            pojoDataNovelties.setIdInspection(jsonObjectNoveltieInspection.has("idInspection") ? jsonObjectNoveltieInspection.get("idInspection").getAsInt() : 0);
            pojoDataNovelties.setDateRegister(jsonObjectNoveltieInspection.has("dateRegister") ? jsonObjectNoveltieInspection.get("dateRegister").getAsString() : "Vacio");
            pojoDataNovelties.setPlateVehicle(jsonObjectNoveltieInspection.has("plateVehicle") ? jsonObjectNoveltieInspection.get("plateVehicle").getAsString() : "Vacio");
            pojoDataNovelties.setDateNoveltie(jsonObjectNoveltieInspection.has("dateNoveltie") ? jsonObjectNoveltieInspection.get("dateNoveltie").getAsString() : "Vacio");
            pojoDataNovelties.setTimeNoveltie(jsonObjectNoveltieInspection.has("timeNoveltie") ? jsonObjectNoveltieInspection.get("timeNoveltie").getAsString() : "Vacio");
            pojoDataNovelties.setDescriptionNoveltie(jsonObjectNoveltieInspection.has("descriptionNoveltie") ? jsonObjectNoveltieInspection.get("descriptionNoveltie").getAsString() : "Vacio");
            pojoDataNovelties.setObservationNoveltie(jsonObjectNoveltieInspection.has("observationNoveltie") ? jsonObjectNoveltieInspection.get("observationNoveltie").getAsString() : "Vacio");
            pojoDataNoveltiesList.add(pojoDataNovelties);
        }
        idLinearLayoutNoNovelties.setVisibility(View.GONE);
        adapterFragmentNovelties.notifyDataSetChanged();
        snackBarPersonalized.showSnackBarResponse(responseDataNoveltiesInspections.getCodeStatus(), responseDataNoveltiesInspections.getMessage());
    }
}

package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseLoginUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FragmentProfile extends Fragment implements InterfacesMenu.InterfacesFragmentProfile {

    //Declaramos las variables XML
    @BindView(R.id.HomeFragmentProfileImageView)
    CircleImageView HomeFragmentProfileImageView;
    @BindView(R.id.inner_relativelayout1)
    RelativeLayout innerRelativelayout1;
    @BindView(R.id.idTextInputLayoutName)
    TextInputLayout idTextInputLayoutName;
    @BindView(R.id.idTextInputLayoutLastName)
    TextInputLayout idTextInputLayoutLastName;
    @BindView(R.id.idTextInputLayoutEmail)
    TextInputLayout idTextInputLayoutEmail;
    @BindView(R.id.idTextInputLayoutPhone)
    TextInputLayout idTextInputLayoutPhone;
    @BindView(R.id.idTextInputLayoutPasswordOld)
    TextInputLayout idTextInputLayoutPasswordOld;
    @BindView(R.id.idTextInputLayoutPasswordNew)
    TextInputLayout idTextInputLayoutPasswordNew;
    @BindView(R.id.idAppCompatButtonIngresar)
    AppCompatButton idAppCompatButtonIngresar;
    @BindView(R.id.viewB)
    LinearLayoutCompat viewB;

    //Declaramos las variables String y int
    private int idUser;
    private int networkState;

    //Declaramos las variables boolean
    public static boolean isConnect = false;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Utilidades Object
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;

    //Instanciamos la interfaces
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getContext();
        dbSivad = new DBSivad(context);
        progressDialog = new ProgressDialog(context);
        snackBarPersonalized = new SnackBarPersonalized(getActivity());
        interfacesPresenterMenu = new PresenterMenu(null
                , null
                , this
                , null
                , null
                , null
                , null
                , null
                , context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        if (responseLoginUser != null) {
            idTextInputLayoutName.getEditText().setText(responseLoginUser.getData().getName());
            idTextInputLayoutLastName.getEditText().setText(responseLoginUser.getData().getLastName());
            idTextInputLayoutEmail.getEditText().setText(responseLoginUser.getData().getEmail());
            idTextInputLayoutPhone.getEditText().setText(String.valueOf(responseLoginUser.getData().getPhone()));
            idUser = responseLoginUser.getData().getIdUser();
        }

        idTextInputLayoutName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutName.isErrorEnabled()) {
                    idTextInputLayoutName.setEnabled(false);
                }

                if (idTextInputLayoutLastName.isErrorEnabled()) {
                    idTextInputLayoutLastName.setEnabled(false);
                }

                if (idTextInputLayoutEmail.isErrorEnabled()) {
                    idTextInputLayoutEmail.setEnabled(false);
                }

                if (idTextInputLayoutPhone.isErrorEnabled()) {
                    idTextInputLayoutPhone.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutLastName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutName.isErrorEnabled()) {
                    idTextInputLayoutName.setEnabled(false);
                }

                if (idTextInputLayoutLastName.isErrorEnabled()) {
                    idTextInputLayoutLastName.setEnabled(false);
                }

                if (idTextInputLayoutEmail.isErrorEnabled()) {
                    idTextInputLayoutEmail.setEnabled(false);
                }

                if (idTextInputLayoutPhone.isErrorEnabled()) {
                    idTextInputLayoutPhone.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutEmail.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutName.isErrorEnabled()) {
                    idTextInputLayoutName.setEnabled(false);
                }

                if (idTextInputLayoutLastName.isErrorEnabled()) {
                    idTextInputLayoutLastName.setEnabled(false);
                }

                if (idTextInputLayoutEmail.isErrorEnabled()) {
                    idTextInputLayoutEmail.setEnabled(false);
                }

                if (idTextInputLayoutPhone.isErrorEnabled()) {
                    idTextInputLayoutPhone.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutPhone.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutName.isErrorEnabled()) {
                    idTextInputLayoutName.setEnabled(false);
                }

                if (idTextInputLayoutLastName.isErrorEnabled()) {
                    idTextInputLayoutLastName.setEnabled(false);
                }

                if (idTextInputLayoutEmail.isErrorEnabled()) {
                    idTextInputLayoutEmail.setEnabled(false);
                }

                if (idTextInputLayoutPhone.isErrorEnabled()) {
                    idTextInputLayoutPhone.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutPasswordOld.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutPasswordNew.isErrorEnabled()) {
                    idTextInputLayoutPasswordNew.setEnabled(false);
                }

                if (idTextInputLayoutPasswordOld.isErrorEnabled()) {
                    idTextInputLayoutPasswordOld.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        idTextInputLayoutPasswordNew.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutPasswordNew.isErrorEnabled()) {
                    idTextInputLayoutPasswordNew.setEnabled(false);
                }

                if (idTextInputLayoutPasswordOld.isErrorEnabled()) {
                    idTextInputLayoutPasswordOld.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idAppCompatButtonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validatesInputs()) {
                    ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                    int idUserSession = responseLoginUser.getData().getIdUserSession();
                    int idHistoryLog = responseLoginUser.getData().getIdHistoryLog();
                    String name = idTextInputLayoutName.getEditText().getText().toString();
                    String lastName = idTextInputLayoutLastName.getEditText().getText().toString();
                    String email = idTextInputLayoutEmail.getEditText().getText().toString().trim();
                    int phone = Integer.parseInt(idTextInputLayoutPhone.getEditText().getText().toString().trim());
                    String password = idTextInputLayoutPasswordOld.getEditText().getText().toString().trim();
                    String passwordNew = idTextInputLayoutPasswordNew.getEditText().getText().toString().trim();

                    if (isConnect){
                        progressDialog.setMessage(ConstantsStrings.LOAD);
                        progressDialog.show();
                        interfacesPresenterMenu.putDataProfile(idUser, name, lastName, email, phone, password, passwordNew, idUserSession, idHistoryLog);
                    }else{
                        snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
                    }
                }
            }
        });
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            //snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    private boolean validatesInputs() {
        boolean error = false;

        if (idTextInputLayoutName.getEditText().getText().toString().matches("")) {
            idTextInputLayoutName.getEditText().setError("Campo Nombre/s obligatorio");
            error = true;
        }

        if (idTextInputLayoutName.getEditText().getText().toString().length() > 0) {
            Pattern patron = Pattern.compile("^[a-zA-Z ]+$");
            if (!patron.matcher(idTextInputLayoutName.getEditText().getText().toString()).matches() || idTextInputLayoutName.getEditText().getText().toString().length() > 30) {
                idTextInputLayoutName.setError("Nombre inválido");
                error = true;
            }
        }

        if (idTextInputLayoutLastName.getEditText().getText().toString().matches("")) {
            idTextInputLayoutLastName.getEditText().setError("Campo Apellido/s obligatorio");
            error = true;
        }


        if (idTextInputLayoutLastName.getEditText().getText().toString().length() > 0) {
            Pattern patron = Pattern.compile("^[a-zA-Z ]+$");
            if (!patron.matcher(idTextInputLayoutLastName.getEditText().getText().toString()).matches() || idTextInputLayoutLastName.getEditText().getText().toString().length() > 30) {
                idTextInputLayoutLastName.setError("Apellido inválido");
                error = true;
            }
        }

        if (idTextInputLayoutEmail.getEditText().getText().toString().matches("")) {
            idTextInputLayoutEmail.getEditText().setError("Campo Correo obligatorio");
            error = true;
        }

        if (idTextInputLayoutEmail.getEditText().getText().toString().length() > 0) {
            if (!Patterns.EMAIL_ADDRESS.matcher(idTextInputLayoutEmail.getEditText().getText().toString()).matches()) {
                idTextInputLayoutEmail.setError("Formato correo inválido.");
                error = true;
            }
        }

        if (idTextInputLayoutPhone.getEditText().getText().toString().matches("")) {
            idTextInputLayoutPhone.getEditText().setError("Campo Télefono obligatorio");
            error = true;
        }

        if (idTextInputLayoutPhone.getEditText().getText().toString().length() > 0) {
            if (!Patterns.PHONE.matcher(idTextInputLayoutPhone.getEditText().getText().toString()).matches()) {
                idTextInputLayoutPhone.setError("Teléfono inválido");
                return false;
            }
        }

        if (idTextInputLayoutPasswordOld.getEditText().getText().toString().length() > 0) {
            if (idTextInputLayoutPasswordNew.getEditText().getText().toString().matches("")) {
                idTextInputLayoutPasswordNew.getEditText().setError("Campo Contraseña Nueva obligatorio");
                error = true;
            }
        }

        if (idTextInputLayoutPasswordNew.getEditText().getText().toString().length() > 0) {
            if (idTextInputLayoutPasswordOld.getEditText().getText().toString().matches("")) {
                idTextInputLayoutPasswordOld.getEditText().setError("Campo Contraseña Nueva obligatorio");
                error = true;
            }
        }
        return error;
    }

    @Override
    public void responseErrorPutDataProfile(ResponseError responseError) {
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccesPutDataProfile(ResponseLoginUser responseLoginUser) {
        progressDialog.dismiss();
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreated = ft.format(date).toString();
        Gson gson = new Gson();
        String json = gson.toJson(responseLoginUser);
        int fileDeletesLoginUser = dbSivad.deleteDataLoginUser();
        Log.v("fileDeleteLoginUser", String.valueOf(fileDeletesLoginUser));
        Long idAddLoginUser = dbSivad.addDataLoginUser(dateCreated, json);
        Log.v("idAddLoginUser", String.valueOf(idAddLoginUser));

        if (responseLoginUser != null) {
            idTextInputLayoutName.getEditText().setText(responseLoginUser.getData().getName());
            idTextInputLayoutLastName.getEditText().setText(responseLoginUser.getData().getLastName());
            idTextInputLayoutEmail.getEditText().setText(responseLoginUser.getData().getEmail());
            idTextInputLayoutPhone.getEditText().setText(String.valueOf(responseLoginUser.getData().getPhone()));
            idUser = responseLoginUser.getData().getIdUser();
        }

        snackBarPersonalized.showSnackBarResponse(responseLoginUser.getCodestatus(), responseLoginUser.getMessage());
    }
}

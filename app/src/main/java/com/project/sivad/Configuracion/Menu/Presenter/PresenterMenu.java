package com.project.sivad.Configuracion.Menu.Presenter;

import android.content.Context;

import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Model.ModelMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentValidacionPlacaKilometraje;
import com.project.sivad.http.Response.ResponseDataNotificationByVehicle;
import com.project.sivad.http.Response.ResponseDataNoveltiesInspections;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseSuccessUpdateMeleage;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONObject;

import java.util.List;

public class PresenterMenu implements InterfacesMenu.InterfacesPresenterMenu {

    private InterfacesMenu.InterfacesViewMenu interfacesViewMenu;
    private InterfacesMenu.InterfacesFragmentValidatePlateMeleage interfacesFragmentValidatePlacaKilometraje;
    private InterfacesMenu.InterfacesModelMenu interfacesModelMenu;
    private InterfacesMenu.InterfacesFragmentProfile interfacesFragmentProfile;
    private InterfacesMenu.InterfacesFragmnetNotifications interfacesFragmnetNotifications;
    private InterfacesMenu.InterfacesFragmentNovelties interfacesFragmentNovelties;
    private InterfacesMenu.InterfacesFragmentOffile interfacesFragmentOffile;
    private InterfacesMenu.InterfaceFragmentHome interfaceFragmentHome;
    private InterfacesMenu.InterfaceFragmenUpdateMeleage interfaceFragmenUpdateMeleage;

    public PresenterMenu(InterfacesMenu.InterfacesViewMenu interfacesViewMenu
            , InterfacesMenu.InterfacesFragmentValidatePlateMeleage interfacesFragmentValidatePlacaKilometraje
            , InterfacesMenu.InterfacesFragmentProfile interfacesFragmentProfile
            , InterfacesMenu.InterfacesFragmnetNotifications interfacesFragmnetNotifications
            , InterfacesMenu.InterfacesFragmentNovelties interfacesFragmentNovelties
            , InterfacesMenu.InterfacesFragmentOffile interfacesFragmentOffile
            , InterfacesMenu.InterfaceFragmentHome interfaceFragmentHome
                         , InterfacesMenu.InterfaceFragmenUpdateMeleage interfaceFragmenUpdateMeleage
            , Context context) {
        this.interfacesViewMenu = interfacesViewMenu;
        this.interfacesFragmentValidatePlacaKilometraje = interfacesFragmentValidatePlacaKilometraje;
        this.interfacesFragmentProfile = interfacesFragmentProfile;
        this.interfacesFragmnetNotifications = interfacesFragmnetNotifications;
        this.interfacesFragmentNovelties = interfacesFragmentNovelties;
        this.interfacesFragmentOffile = interfacesFragmentOffile;
        this.interfaceFragmentHome = interfaceFragmentHome;
        this.interfaceFragmenUpdateMeleage = interfaceFragmenUpdateMeleage;
        this.interfacesModelMenu = new ModelMenu(this, context);
    }

    @Override
    public void responseErrorGetDataPlacaKilometraje(ResponseError responseError) {
        if (interfacesFragmentValidatePlacaKilometraje != null) {
            interfacesFragmentValidatePlacaKilometraje.responseErrorGetDataPlacaKilometraje(responseError);
        }
    }

    @Override
    public void responseSuccessGetDataPlacaKilometraje(ResponseValidatePlateMileage responseValidatePlateMileage) {
        if (interfacesFragmentValidatePlacaKilometraje != null) {
            interfacesFragmentValidatePlacaKilometraje.responseSuccessGetDataPlacaKilometraje(responseValidatePlateMileage);
        }
    }

    @Override
    public void getDataPlacaKilometraje(String plate, int mileage) {
        if (interfacesFragmentValidatePlacaKilometraje != null) {
            interfacesModelMenu.getDataPlacaKilometraje(plate, mileage);
        }
    }

    @Override
    public void responseErrorPutDataProfile(ResponseError responseError) {
        if (interfacesFragmentProfile != null) {
            interfacesFragmentProfile.responseErrorPutDataProfile(responseError);
        }
    }

    @Override
    public void responseSuccesPutDataProfile(ResponseLoginUser responseLoginUser) {
        if (interfacesFragmentProfile != null) {
            interfacesFragmentProfile.responseSuccesPutDataProfile(responseLoginUser);
        }
    }

    @Override
    public void putDataProfile(int idUser
            , String name
            , String lastName
            , String email
            , int phone
            , String passwordOld
            , String passwordNew
            , int idUserSession
            , int idHistoryLog) {
        if (interfacesFragmentProfile != null) {
            interfacesModelMenu.putDataProfile(idUser, name, lastName, email, phone, passwordOld, passwordNew, idUserSession, idHistoryLog);
        }
    }

    /**
     * Estos los metodo para Fragment SignOut
     */

    @Override
    public void responseErrorPutSignOut(ResponseError responseError) {
        if (interfacesViewMenu != null) {
            interfacesViewMenu.responseErrorPutSignOut(responseError);
        }
    }

    @Override
    public void responseSuccessPutSignOut(ResponseJson responseJson) {
        if (interfacesViewMenu != null) {
            interfacesViewMenu.responseSuccessPutSignOut(responseJson);
        }
    }

    @Override
    public void putDataSignOut(int idUser, int idUserSession, int idHistoryLog) {
        if (interfacesViewMenu != null) {
            interfacesModelMenu.putDataSignOut(idUser, idUserSession, idHistoryLog);
        }
    }

    /**
     * Estos los metodo para Fragment Notifications
     */

    @Override
    public void responseErrorGetDataNotificationByVehicle(ResponseError responseError) {
        if (interfacesFragmnetNotifications != null) {
            interfacesFragmnetNotifications.responseErrorGetDataNotificationByVehicle(responseError);
        }
    }

    @Override
    public void responseSuccessGetDataNotificationByVehicle(ResponseDataNotificationByVehicle responseDataNotificationByVehicle) {
        if (interfacesFragmnetNotifications != null) {
            interfacesFragmnetNotifications.responseSuccessGetDataNotificationByVehicle(responseDataNotificationByVehicle);
        }
    }

    @Override
    public void getDataNotificationsByVehicle(int idVehicle) {
        if (interfacesFragmnetNotifications != null) {
            interfacesModelMenu.getDataNotificationsByVehicle(idVehicle);
        }
    }

    /**
     * Estos los metodo para Fragment Novelties
     */

    @Override
    public void responseErrorGetDataNoveltiesInspection(ResponseError responseError) {
        if (interfacesFragmentNovelties != null) {
            interfacesFragmentNovelties.responseErrorGetDataNoveltiesInspection(responseError);
        }
    }

    @Override
    public void responseSuccessGetDataNoveltiesInspection(ResponseDataNoveltiesInspections responseDataNoveltiesInspections) {
        if (interfacesFragmentNovelties != null) {
            interfacesFragmentNovelties.responseSuccessGetDataNoveltiesInspection(responseDataNoveltiesInspections);
        }
    }

    @Override
    public void getDataNoveltiesInspections(int idVehicle) {
        if (interfacesFragmentNovelties != null) {
            interfacesModelMenu.getDataNoveltiesInspections(idVehicle);
        }
    }

    /**
     * Estos los metodo para Fragment Offline
     */

    @Override
    public void responseErrorPostDataOffline(ResponseError responseError) {
        if (interfacesFragmentOffile != null) {
            interfacesFragmentOffile.responseErrorPostDataOffline(responseError);
        }
    }

    @Override
    public void responseSuccessPostDataOfflineVehicleInspection(ResponseVehicleInspecion responseVehicleInspecion) {
        if (interfacesFragmentOffile != null) {
            interfacesFragmentOffile.responseSuccessPostDataOfflineVehicleInspection(responseVehicleInspecion);
        }
    }

    @Override
    public void responseSuccessPostDataOfflineNoveltiesInspection(ResponseJson responseJson) {
        if (interfacesFragmentOffile != null) {
            interfacesFragmentOffile.responseSuccessPostDataOfflineNoveltiesInspection(responseJson);
        }
    }

    @Override
    public void responseSuccessPostDataOfflineNotificationAlertByToggleBad(ResponseJson responseJson) {
        if (interfacesFragmentOffile != null) {
            interfacesFragmentOffile.responseSuccessPostDataOfflineNotificationAlertByToggleBad(responseJson);
        }
    }

    @Override
    public void responseSuccessPostDataOfflineUMileageUpdate(ResponseValidatePlateMileage responseValidatePlateMileage) {
        if (interfacesFragmentOffile != null) {
            interfacesFragmentOffile.responseSuccessPostDataOfflineUMileageUpdate(responseValidatePlateMileage);
        }
    }

    @Override
    public void postDataOffline(int idRegister, String jsonObject, String nameModule) {
        if (interfacesFragmentOffile != null) {
            interfacesModelMenu.postDataOffline(idRegister, jsonObject, nameModule);
        }
    }

    /**
     * Estos los metodos para Fragment Home
     */
    @Override
    public void responseErrorGetDataTypesVehicularInspections(ResponseError responseError) {
        if (interfaceFragmentHome != null){
            interfaceFragmentHome.responseErrorGetDataTypesVehicularInspections(responseError);
        }
    }

    @Override
    public void responseSuccesGetDataTypesVehicularInspections(ResponseDataTypesVehicularInspectios responseDataTypesVehicularInspectios) {
        if (interfaceFragmentHome != null){
            interfaceFragmentHome.responseSuccesGetDataTypesVehicularInspections(responseDataTypesVehicularInspectios);
        }
    }

    @Override
    public void getDataTypesVehicularInspection() {
        if (interfaceFragmentHome != null){
            interfacesModelMenu.getDataTypesVehicularInspection();
        }
    }

    @Override
    public void responseErrorGetValidateInspectionEnabled(ResponseError responseError) {
        if (interfaceFragmentHome != null){
            interfaceFragmentHome.responseErrorGetValidateInspectionEnabled(responseError);
        }
    }

    @Override
    public void responseSuccessGetValidateInspectionEnabled(ResponseValideteInspectionEnabled responseValideteInspectionEnabled) {
        if(interfaceFragmentHome != null){
            interfaceFragmentHome.responseSuccessGetValidateInspectionEnabled(responseValideteInspectionEnabled);
        }
    }

    @Override
    public void getValidateInspectionEnabled(String plateVehicle) {
        if (interfaceFragmentHome != null){
            interfacesModelMenu.getValidateInspectionEnabled(plateVehicle);
        }
    }

    /**
     * Estos los metodos interfaces de Fragment UpdateMeleage
     */
    @Override
    public void responseErrorPutDataMeleage(ResponseError responseError, JSONObject jsonObject) {
        if (interfaceFragmenUpdateMeleage != null){
            interfaceFragmenUpdateMeleage.responseErrorPutDataMeleage(responseError, jsonObject);
        }
    }

    @Override
    public void responseSuccessPutDataMeleade(ResponseValidatePlateMileage responseValidatePlateMileage) {
        if (interfaceFragmenUpdateMeleage != null){
            interfaceFragmenUpdateMeleage.responseSuccessPutDataMeleade(responseValidatePlateMileage);
        }
    }

    @Override
    public void putDataMeleage(String dateRegister, int idUser, int idVehicle, int meleage) {
        if (interfaceFragmenUpdateMeleage != null){
            interfacesModelMenu.putDataMeleage(dateRegister, idUser, idVehicle, meleage);
        }
    }
}

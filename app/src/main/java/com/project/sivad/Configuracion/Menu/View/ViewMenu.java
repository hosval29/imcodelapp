package com.project.sivad.Configuracion.Menu.View;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentDashboard;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentHome;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentNotificaciones;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentNovedades;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentOffline;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentProfile;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentUpdateMeleage;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentValidacionPlacaKilometraje;
import com.project.sivad.Configuracion.Menu.Vendor.Fragment.FragmentWelcome;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataOffline;
import com.project.sivad.Configuracion.Seguridad.View.ViewLogin;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewNovedades;
import com.project.sivad.R;
import com.project.sivad.Util.Components.FormatDates;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.SessionManager;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ViewMenu extends AppCompatActivity implements
        InterfacesMenu.InterfacesViewMenu
        , FragmentValidacionPlacaKilometraje.InterfacesFragmentValidatePlacaKilometraje
        , SnackBarPersonalized.InterfacesResponseSnackbarWithAction
        , ViewNovedades.InterfaceViewNovelties
        , FragmentUpdateMeleage.InterfaceFragmentUpdateMeleage {

    @BindView(R.id.idFrameLayoutMenu)
    FrameLayout idFrameLayoutMenu;
    @BindView(R.id.idBtnDashMenu)
    FloatingActionButton idBtnDashMenu;
    @BindView(R.id.idBootomTolbar)
    BottomAppBar idBootomTolbar;
    @BindView(R.id.idFloatingActionButtonMenuNovelties)
    com.getbase.floatingactionbutton.FloatingActionButton idFloatingActionButtonMenuNovelties;
    @BindView(R.id.idFloatingActionButtonMenuMeleage)
    com.getbase.floatingactionbutton.FloatingActionButton idFloatingActionButtonMenuMeleage;
    @BindView(R.id.floatingActionButtonMenu)
    FloatingActionsMenu floatingActionButtonMenu;
    //Declaramos las variables xml
    private FloatingActionButton btnDashMenu;
    private BottomAppBar bottomAppBar;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Instanciamos los Object utilidades
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private FormatDates formatDates;

    //Variables String y int
    private int networkState;

    //Variables de validacion
    private boolean isFabTapped;
    private boolean isNoveltiesInspectionEventual = false;
    private boolean isNotification = false;
    public static boolean isConnect = false;

    //Instanciamos los fragments
    private FragmentHome fragmentHome;
    private FragmentDashboard fragmentDashboard;
    private FragmentNotificaciones fragmentNotificaciones;
    private FragmentNovedades fragmentNovedades;
    private FragmentProfile fragmentProfile;
    private FragmentOffline fragmentOffline;
    private FragmentWelcome fragmentWelcome;

    //Instanciamos la Interfaces
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    //Declaramos la variable list
    private List<PojoDataOffline> pojoDataOfflineList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_menu);
        ButterKnife.bind(this);

        btnDashMenu = (FloatingActionButton) findViewById(R.id.idBtnDashMenu);
        bottomAppBar = (BottomAppBar) findViewById(R.id.idBootomTolbar);
        setSupportActionBar(bottomAppBar);

        //Inicializamos las variables tipo Objects
        context = ViewMenu.this;
        dbSivad = new DBSivad(context);
        formatDates = new FormatDates();
        progressDialog = new ProgressDialog(context);
        interfacesPresenterMenu = new PresenterMenu(this
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , context);
        snackBarPersonalized = new SnackBarPersonalized(ViewMenu.this);
        sessionManager = new SessionManager(context);
        fragmentHome = new FragmentHome();
        fragmentDashboard = new FragmentDashboard();
        fragmentNotificaciones = new FragmentNotificaciones();
        fragmentNovedades = new FragmentNovedades();
        fragmentProfile = new FragmentProfile();
        fragmentOffline = new FragmentOffline();
        fragmentWelcome = new FragmentWelcome();

        /*if (savedInstanceState == null) {
            loadFragment(new FragmentHome());
        }*/

        /*btnDashMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFabTapped = !isFabTapped;
                if (isFabTapped){
                    bottomAppBar.setFabAlignmentMode(BottomAppBar.FAB_ALIGNMENT_MODE_END);
                    loadFragment(new FragmentHome());
                    btnDashMenu.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_dashboard_black_24dp));
                }else{
                    bottomAppBar.setFabAlignmentMode(BottomAppBar.FAB_ALIGNMENT_MODE_CENTER);
                    loadFragment(new FragmentDashboard());
                    btnDashMenu.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_home_black_24dp));
                }
            }
        });*/

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isNotification = bundle.get("isNotification") != null ? bundle.getBoolean("isNotification") : false;
        }

        if (isNotification) {
            loadFragment(fragmentNotificaciones);
            isNotification = !isNotification;
        }

        idFloatingActionButtonMenuMeleage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.v("isConnect", String.valueOf(isConnect));
                if (isConnect) {
                    validateDataOfflineMileageUpdate();
                } else {
                    loadFragmentMileageUpdate();
                }
            }
        });

        idFloatingActionButtonMenuNovelties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                if (responseLoginUser != null) {
                    ResponseVehicleInspecion responseVehicleInspecion = dbSivad.getDataVehicleInspection(responseLoginUser.getData().getIdUser());
                    if (responseVehicleInspecion != null) {
                        isNoveltiesInspectionEventual = true;
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                        if (prev != null) {
                            ft.remove(prev);
                        }
                        ft.addToBackStack(null);
                        DialogFragment dialogFrag = new ViewNovedades();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("isNoveltiesInspectionEventual", isNoveltiesInspectionEventual);
                        bundle.putInt("idInspection", responseVehicleInspecion.getData().getIdInspection());
                        dialogFrag.setArguments(bundle);
                        dialogFrag.setCancelable(false);
                        dialogFrag.show(ft, "dialog");
                    } else {
                        snackBarPersonalized.showSnackBarErrorCualquiera("No existen datos de la inspección");
                    }

                } else {
                    snackBarPersonalized.showSnackBarErrorCualquiera("No existen datos del usuario");
                }
            }
        });

        bottomAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                if (responseLoginUser != null) {
                    ResponseVehicleInspecion responseVehicleInspecion = dbSivad.getDataVehicleInspection(responseLoginUser.getData().getIdUser());
                    String dateResponseAlertNotificationEmail = dbSivad.getDateResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser());
                    if (responseVehicleInspecion != null || !dateResponseAlertNotificationEmail.isEmpty()) {
                        floatingActionButtonMenu.setVisibility(View.VISIBLE);
                    } else {
                        floatingActionButtonMenu.setVisibility(View.GONE);
                    }
                }

                loadFragment(fragmentHome);
            }
        });

        bottomAppBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.idItemOffline:
                        floatingActionButtonMenu.setVisibility(View.GONE);
                        loadFragment(fragmentOffline);
                        break;

                    case R.id.idItemNovedades:
                        floatingActionButtonMenu.setVisibility(View.GONE);
                        loadFragment(fragmentNovedades);
                        break;

                    case R.id.idItemNotificaciones:
                        floatingActionButtonMenu.setVisibility(View.GONE);
                        loadFragment(fragmentNotificaciones);
                        break;

                    case R.id.idItemProfile:
                        floatingActionButtonMenu.setVisibility(View.GONE);
                        loadFragment(fragmentProfile);
                        break;

                    case R.id.idItemSignUp:
                        floatingActionButtonMenu.setVisibility(View.GONE);
                        signOut();
                        break;

                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("estoy aqui", "onStart");
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.v("estoy aqui", "onResume");

        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        //Log.v("responseDataCode", responseDataCode.toString());
        if (responseValidatePlateMileage == null) {
            loadFragmentValidacionPlacaKilometraje();
        } else {
            if (getValuePreference(context)) {
                //floatingActionButtonMenu.setVisibility(View.VISIBLE);
                loadFragment(fragmentHome);
            } else {
                loadFragment(fragmentWelcome);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //saveValuePreference(context, true);
                        loadFragment(fragmentHome);
                    }
                }, 5000);
            }
        }

        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        if (responseLoginUser != null) {
            ResponseVehicleInspecion responseVehicleInspecion = dbSivad.getDataVehicleInspection(responseLoginUser.getData().getIdUser());
            String dateResponseAlertNotificationEmail = dbSivad.getDateResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser());
            if (responseVehicleInspecion != null || !dateResponseAlertNotificationEmail.isEmpty()) {
                floatingActionButtonMenu.setVisibility(View.VISIBLE);
            } else {
                floatingActionButtonMenu.setVisibility(View.GONE);
            }
        }

        getDataOffline();

        registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("estoy aqui", "onPause");
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Log.v("estoy aqui", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Log.v("estoy aqui", "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Log.v("estoy aqui", "onDestroy");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    private void getDataOffline() {
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        pojoDataOfflineList.clear();
        pojoDataOfflineList.addAll(dbSivad.getDataOffline(responseLoginUser.getData().getIdUser()));
    }

    private void loadFragmentMileageUpdate(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFrag = new FragmentUpdateMeleage();
        dialogFrag.setCancelable(false);
        dialogFrag.show(ft, "dialog");
    }

    private void validateDataOfflineMileageUpdate(){
        if (isConnect) {
            if (pojoDataOfflineList != null){
                Log.v("pojoDataOfflineList", pojoDataOfflineList.toString());
            }

            if (pojoDataOfflineList.size() > 0) {
                boolean isDataOfflineMileageUpdate = false;
                for (int i = 0; i < pojoDataOfflineList.size(); i++) {
                    if (pojoDataOfflineList.get(i).getTitleModule().equals(ConstantsStrings.MODULE_UPDATE_MILEAGE)) {
                        isDataOfflineMileageUpdate = true;
                    }else{
                        isDataOfflineMileageUpdate = false;
                    }
                }

                Log.v("isDataOfflineMileageUp", String.valueOf(isDataOfflineMileageUpdate));

                if (isDataOfflineMileageUpdate){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Existen registros de actualización de kilometrajes almacenados offline.\n" +
                            "Diríjase al módulo offline y sincronice primero los kilometrajes registrados.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setTitle("Bloqueo Actualizión Kilometraje");
                    dialog.show();
                }else{
                    loadFragmentMileageUpdate();
                }

            } else {
                loadFragmentMileageUpdate();
            }
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.idFrameLayoutMenu, fragment);
        fragmentTransaction.commit();
    }

    private void loadFragmentValidacionPlacaKilometraje() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFrag;
        dialogFrag = new FragmentValidacionPlacaKilometraje();
        dialogFrag.setCancelable(false);
        dialogFrag.show(ft, "dialog");
    }

    @Override
    public void responseGetPlateKilometraje(ResponseValidatePlateMileage responseValidatePlateMileage) {
        String resgistrationDate = formatDates.getDateRegisterFormatDateWithTime();
        Gson gson = new Gson();
        String json = gson.toJson(responseValidatePlateMileage);
        int idDelete = dbSivad.deleteDataVehicle();
        Long idAdd = dbSivad.addDataVehicle(resgistrationDate, json);
        if (idAdd != null) {
            snackBarPersonalized.showSnackBarResponse(responseValidatePlateMileage.getCodeStatus(), responseValidatePlateMileage.getMessage());
            loadFragment(fragmentWelcome);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //saveValuePreference(context, true);
                    loadFragment(fragmentHome);
                }
            }, 5000);
        }
    }

    private void signOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Esta seguro que desea cerrar sesión?").setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                        if (responseLoginUser != null) {
                            if (isConnect) {
                                progressDialog.setMessage(ConstantsStrings.LOAD);
                                progressDialog.show();
                                interfacesPresenterMenu.putDataSignOut(responseLoginUser.getData().getIdUser()
                                        , responseLoginUser.getData().getIdUserSession()
                                        , responseLoginUser.getData().getIdHistoryLog());
                            } else {
                                snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
                            }
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Cerrar Sesión");
        dialog.show();
    }

    @Override
    public void responseErrorPutSignOut(ResponseError responseError) {
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccessPutSignOut(ResponseJson responseJson) {
        progressDialog.dismiss();
        dbSivad.deleteDataLoginUser();
        dbSivad.deleteDataNoveltiesInspection();
        //dbSivad.deleteDataInspection();
        dbSivad.deleteDataTypesVehicularInspection();
        dbSivad.deleteDataVehicle();
        sessionManager.setLogin(false);
        saveValuePreference(context, false);
        Intent intent = new Intent(this, ViewLogin.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void responseSnackbarWithAction() {

    }

    private String PREFS_KEY = "MySharedPreferences";

    public boolean getValuePreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        return preferences.getBoolean("welcome", false);
    }

    public void saveValuePreference(Context context, boolean isWelcome) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putBoolean("welcome", isWelcome);
        editor.commit();
    }

    @Override
    public void responsePostDataNoveltiesInspecion(int code, String message) {
        snackBarPersonalized.showSnackBarResponse(code, message);
    }

    @Override
    public void responsePutDataMeleage(int codeStatus, String message) {
        snackBarPersonalized.showSnackBarResponse(codeStatus, message);
    }
}

package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Adapter.AdapterFragmentOffline;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataOffline;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces.InterfacesInspeccionPreoperation;
import com.project.sivad.Modulos.InspeccionPreoperacional.Presenter.PresenterInspectionPreoperational;
import com.project.sivad.R;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentOffline extends Fragment implements
        InterfacesMenu.InterfacesFragmentOffile
        , InterfacesMenu.InterfacesSendDataOfflineById {

    @BindView(R.id.idLinearLayoutNoOffline)
    LinearLayoutCompat idLinearLayoutNoOffline;
    @BindView(R.id.idRecyclerViewFragmentOffline)
    RecyclerView idRecyclerViewFragmentOffline;

    //Vairables String y int
    private int networkState;

    //Variables Boolean
    public static boolean isConnect = false;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Utils Objects
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;

    //Declaramos las variables de rvCalendario
    private List<PojoDataOffline> pojoDataOfflineList = new ArrayList<>();
    private AdapterFragmentOffline adapterFragmentOffline;
    private LinearLayoutManager linearLayoutManager;

    //Instanciamos la Interfaces
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        dbSivad = new DBSivad(context);
        snackBarPersonalized = new SnackBarPersonalized(getActivity());
        progressDialog = new ProgressDialog(context);
        interfacesPresenterMenu = new PresenterMenu(null
                , null
                , null
                , null
                , null
                , this
                , null
                , null
                , context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offline, container, false);
        ButterKnife.bind(this, view);
        setupRecyclerView();
        getDataOffline();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            //snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        idRecyclerViewFragmentOffline.setHasFixedSize(true);
        idRecyclerViewFragmentOffline.setLayoutManager(linearLayoutManager);
        adapterFragmentOffline = new AdapterFragmentOffline(pojoDataOfflineList, this::sendDataOfflineById);
        idRecyclerViewFragmentOffline.setAdapter(adapterFragmentOffline);
    }

    private void getDataOffline() {
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        if (responseLoginUser != null) {
            pojoDataOfflineList.clear();
            pojoDataOfflineList.addAll(dbSivad.getDataOffline(responseLoginUser.getData().getIdUser()));
            if (pojoDataOfflineList.size() > 0) {
                idLinearLayoutNoOffline.setVisibility(View.GONE);
                adapterFragmentOffline.notifyDataSetChanged();
            } else {
                idLinearLayoutNoOffline.setVisibility(View.VISIBLE);
            }
        } else {
            idLinearLayoutNoOffline.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void sendDataOfflineById(int position) {
        if (isConnect){
            if(pojoDataOfflineList.get(position).getTitleModule().equals(ConstantsStrings.MODULE_INSPECCION_PREOPERACIONAL_NOTIFICATION_ALERT)){
                progressDialog.setMessage(ConstantsStrings.LOAD_NOTIFICATION_ALERT);
            }else{
                progressDialog.setMessage(ConstantsStrings.LOAD);
            }

            progressDialog.show();
            interfacesPresenterMenu.postDataOffline(pojoDataOfflineList.get(position).getIdRegister(), pojoDataOfflineList.get(position).getData(), pojoDataOfflineList.get(position).getTitleModule());
        }else{
            snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
        }
    }

    @Override
    public void responseErrorPostDataOffline(ResponseError responseError) {
        getDataOffline();
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccessPostDataOfflineVehicleInspection(ResponseVehicleInspecion responseVehicleInspecion) {
        getDataOffline();
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseVehicleInspecion.getCodeStatus(), responseVehicleInspecion.getMessage());
    }

    @Override
    public void responseSuccessPostDataOfflineNoveltiesInspection(ResponseJson responseJson) {
        getDataOffline();
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseJson.getCodeStatus(), responseJson.getMessage());
    }

    @Override
    public void responseSuccessPostDataOfflineNotificationAlertByToggleBad(ResponseJson responseJson) {
        getDataOffline();
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseJson.getCodeStatus(), responseJson.getMessage());
    }

    @Override
    public void responseSuccessPostDataOfflineUMileageUpdate(ResponseValidatePlateMileage responseValidatePlateMileage) {
        getDataOffline();
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseValidatePlateMileage.getCodeStatus(), responseValidatePlateMileage.getMessage());
    }
}

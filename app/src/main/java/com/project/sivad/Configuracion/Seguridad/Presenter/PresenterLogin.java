package com.project.sivad.Configuracion.Seguridad.Presenter;

import android.content.Context;

import com.project.sivad.Configuracion.Seguridad.Interfaces.InterfacesLogin;
import com.project.sivad.Configuracion.Seguridad.Model.ModelLogin;
import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseLoginUser;

public class PresenterLogin implements InterfacesLogin.InterfacesPresenterLogin {
    private InterfacesLogin.InterfacesViewFragmentCodeSetting interfacesViewFragmentCodeSetting;
    private InterfacesLogin.InterfacesViewLogin interfacesViewLogin;
    private InterfacesLogin.InterfacesModelLogin interfacesModelLogin;

    public PresenterLogin(InterfacesLogin.InterfacesViewFragmentCodeSetting interfacesViewFragmentCodeSetting
            , InterfacesLogin.InterfacesViewLogin interfacesViewLogin
            , Context context) {

        this.interfacesViewFragmentCodeSetting = interfacesViewFragmentCodeSetting;
        this.interfacesViewLogin = interfacesViewLogin;
        interfacesModelLogin = new ModelLogin(this, context);
    }

    @Override
    public void responseErrorGetDataCodeSetting(ResponseError responseError) {
        if(interfacesViewFragmentCodeSetting != null){
            interfacesViewFragmentCodeSetting.responseErrorGetDataCodeSetting(responseError);
        }
    }

    @Override
    public void responseSuccessgetDataCodeSetting(ResponseDataCode responseDataCode) {
        if(interfacesViewFragmentCodeSetting != null){
            interfacesViewFragmentCodeSetting.responseSuccessgetDataCodeSetting(responseDataCode);
        }
    }

    @Override
    public void postDataCodeSetting(String codeSetting) {
        if(interfacesViewFragmentCodeSetting != null){
            interfacesModelLogin.postDataCodeSetting(codeSetting);
        }
    }

    @Override
    public void responseSuccessGetDataLoginUser(ResponseLoginUser responseLoginUser) {
        if(interfacesViewLogin != null){
            interfacesViewLogin.responseSuccessGetDataLoginUser(responseLoginUser);
        }
    }

    @Override
    public void responseErrorGetDataLoginUser(ResponseError responseError) {
        if(interfacesViewLogin != null){
            interfacesViewLogin.responseErrorGetDataLoginUser(responseError);
        }
    }

    @Override
    public void getDataLoginUser(String user, String pass, String imei, String token) {
        if(interfacesViewLogin != null){
            interfacesModelLogin.getDataLoginUser(user, pass, imei, token);
        }
    }

    /*@Nullable
    private InterfacesLogin.InterfacesViewLogin interfacesViewLogin;
    private InterfacesLogin.InterfacesModelLogin interfacesModelLogin;

    public PresenterLogin(InterfacesLogin.InterfacesModelLogin interfacesModelLogin) {
        this.interfacesModelLogin = interfacesModelLogin;
    }

    @Override
    public void setView(ViewLogin view) {
        this.interfacesViewLogin = view;
    }

    @Override
    public void onClickListenerButtonIngresar() {
        if (interfacesViewLogin != null){
            if (interfacesViewLogin.getUser().equals("") || interfacesViewLogin.getPasss().equals("")){
                interfacesViewLogin.showInputError();
            }else{
                interfacesModelLogin.createUser(interfacesViewLogin.getUser(), interfacesViewLogin.getPasss());
                interfacesViewLogin.showUserSuccesExistis();
            }
        }
    }

    @Override
    public void getCurrentUser() {
        User user = interfacesModelLogin.getUser();

        if (user == null){
            if(interfacesViewLogin != null){
                interfacesViewLogin.showUserErrorExists();
            }
        }else{
            if (interfacesViewLogin != null){
                interfacesViewLogin.setPass(user.getLastName());
                interfacesViewLogin.setUser(user.getFirstName());
            }
        }
    }*/
}

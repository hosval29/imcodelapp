package com.project.sivad.Configuracion.Menu.Vendor.Pojo;

import android.content.Context;

public class PojoItemsMenu {
    String nameItem;
    int idIcon;
    String typeInspectionsOperational;
    private Context context;

    public PojoItemsMenu(String nameItem, int idIcon, String typeInspectionsOperational) {
        this.nameItem = nameItem;
        this.idIcon = idIcon;
        this.typeInspectionsOperational = typeInspectionsOperational;
    }

    public String getTypeInspectionsOperational() {
        return typeInspectionsOperational;
    }

    public void setTypeInspectionsOperational(String typeInspectionsOperational) {
        this.typeInspectionsOperational = typeInspectionsOperational;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public int getIdIcon() {
        return idIcon;
    }

    public void setIdIcon(int idIcon) {
        this.idIcon = idIcon;
    }


}

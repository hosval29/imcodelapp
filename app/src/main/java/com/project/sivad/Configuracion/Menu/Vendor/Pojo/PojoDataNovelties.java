package com.project.sivad.Configuracion.Menu.Vendor.Pojo;

public class PojoDataNovelties {

    int idInspection;
    String dateRegister;
    String plateVehicle;
    String dateNoveltie;
    String timeNoveltie;
    String descriptionNoveltie;
    String observationNoveltie;

    public int getIdInspection() {
        return idInspection;
    }

    public void setIdInspection(int idInspection) {
        this.idInspection = idInspection;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getPlateVehicle() {
        return plateVehicle;
    }

    public void setPlateVehicle(String plateVehicle) {
        this.plateVehicle = plateVehicle;
    }

    public String getDateNoveltie() {
        return dateNoveltie;
    }

    public void setDateNoveltie(String dateNoveltie) {
        this.dateNoveltie = dateNoveltie;
    }

    public String getTimeNoveltie() {
        return timeNoveltie;
    }

    public void setTimeNoveltie(String timeNoveltie) {
        this.timeNoveltie = timeNoveltie;
    }

    public String getDescriptionNoveltie() {
        return descriptionNoveltie;
    }

    public void setDescriptionNoveltie(String descriptionNoveltie) {
        this.descriptionNoveltie = descriptionNoveltie;
    }

    public String getObservationNoveltie() {
        return observationNoveltie;
    }

    public void setObservationNoveltie(String observationNoveltie) {
        this.observationNoveltie = observationNoveltie;
    }

    @Override
    public String toString() {
        return "PojoDataNovelties{" +
                "idInspection=" + idInspection +
                ", dateRegister='" + dateRegister + '\'' +
                ", plateVehicle='" + plateVehicle + '\'' +
                ", dateNoveltie='" + dateNoveltie + '\'' +
                ", timeNoveltie='" + timeNoveltie + '\'' +
                ", descriptionNoveltie='" + descriptionNoveltie + '\'' +
                ", observationNoveltie='" + observationNoveltie + '\'' +
                '}';
    }
}

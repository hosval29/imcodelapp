package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Adapter.AdapterFragmentNotifications;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataNotifications;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseDataNotificationByVehicle;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentNotificaciones extends Fragment implements InterfacesMenu.InterfacesFragmnetNotifications {

    //Variables XML
    @BindView(R.id.idRecyclerViewFragmentNotifications)
    RecyclerView idRecyclerViewFragmentNotifications;
    @BindView(R.id.idLinearLayoutNoNotifications)
    LinearLayoutCompat idLinearLayoutNoNotifications;

    //Declaramos las variables String y int
    private int networkState;

    //Declaramos las variables boolean
    public static boolean isConnect = false;

    //Utils Objects
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Instanciamos la Interfaces
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    //Declaramos las variables de rvCalendario
    private List<PojoDataNotifications> pojoDataNotificationsList = new ArrayList<>();
    private AdapterFragmentNotifications adapterFragmentNotifications;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        dbSivad = new DBSivad(context);
        progressDialog = new ProgressDialog(context);
        snackBarPersonalized = new SnackBarPersonalized(getActivity());
        interfacesPresenterMenu = new PresenterMenu(null
                , null
                , null
                , this
                , null
                , null
                , null
                , null
                , context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificaciones, container, false);
        ButterKnife.bind(this, view);
        setupRecyclerView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        getDataNotificationsByVehicle();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            //snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        idRecyclerViewFragmentNotifications.setHasFixedSize(true);
        idRecyclerViewFragmentNotifications.setLayoutManager(linearLayoutManager);
        adapterFragmentNotifications = new AdapterFragmentNotifications(pojoDataNotificationsList);
        idRecyclerViewFragmentNotifications.setAdapter(adapterFragmentNotifications);
    }

    private void getDataNotificationsByVehicle() {
        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        if (responseValidatePlateMileage != null) {
            if (isConnect){
                progressDialog.setMessage(ConstantsStrings.LOAD);
                progressDialog.show();
                interfacesPresenterMenu.getDataNotificationsByVehicle(responseValidatePlateMileage.getData().getIdVehicle());
            }else{
                snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
            }
        }
    }

    @Override
    public void responseErrorGetDataNotificationByVehicle(ResponseError responseError) {
        Log.v("responseError", String.valueOf(responseError));
        progressDialog.dismiss();
        idLinearLayoutNoNotifications.setVisibility(View.VISIBLE);
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccessGetDataNotificationByVehicle(ResponseDataNotificationByVehicle responseDataNotificationByVehicle) {
        progressDialog.dismiss();
        pojoDataNotificationsList.clear();
        JsonArray jsonArrayDataNotifications = responseDataNotificationByVehicle.getData();
        for (int i = 0; i < jsonArrayDataNotifications.size(); i++) {
            JsonObject jsonObjectDataNotification = jsonArrayDataNotifications.get(i).getAsJsonObject();
            JsonObject jsonObjectDataItemNotification = jsonObjectDataNotification.get("data").getAsJsonObject();
            PojoDataNotifications pojoDataNotifications = new PojoDataNotifications();
            pojoDataNotifications.setTitle(jsonObjectDataItemNotification.has("typeAlert") && !jsonObjectDataItemNotification.isJsonNull() ? jsonObjectDataItemNotification.get("typeAlert").getAsString() : "Vacio");
            pojoDataNotifications.setDateExpirated(jsonObjectDataItemNotification.has("dateExpiration") && !jsonObjectDataItemNotification.isJsonNull() ? jsonObjectDataItemNotification.get("dateExpiration").getAsString() : "Vacio");
            pojoDataNotifications.setPlateVehicle(jsonObjectDataItemNotification.has("plateVehicle") && !jsonObjectDataItemNotification.isJsonNull() ? jsonObjectDataItemNotification.get("plateVehicle").getAsString() : "Vacio");
            pojoDataNotificationsList.add(pojoDataNotifications);
        }
        adapterFragmentNotifications.notifyDataSetChanged();
        idLinearLayoutNoNotifications.setVisibility(View.GONE);
        snackBarPersonalized.showSnackBarResponse(responseDataNotificationByVehicle.getCodeStatus(), responseDataNotificationByVehicle.getMessage());
    }
}

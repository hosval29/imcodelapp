package com.project.sivad.Configuracion.Menu.Vendor.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.google.gson.Gson;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoItemsMenu;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoCargaFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoPasajerosFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoPasajerosIntermunicipal;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientosPasajerosLujo;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientosPasajerosUrbano;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewInspecionPreoperacional;
import com.project.sivad.R;
import com.project.sivad.Util.Components.FormatDates;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class AdapterMenuItem extends RecyclerView.Adapter<AdapterMenuItem.ViewHolder> {

    private List<PojoItemsMenu> pojoItemsMenuList = new ArrayList<>();
    private InterfacesMenu.InterfacesAdapterMenuItem interfacesAdapterMenuItem;
    private DBSivad dbSivad;
    private FormatDates formatDates;

    public AdapterMenuItem(List<PojoItemsMenu> pojoItemsMenuList, InterfacesMenu.InterfacesAdapterMenuItem interfacesAdapterMenuItem) {
        this.pojoItemsMenuList = pojoItemsMenuList;
        this.interfacesAdapterMenuItem = interfacesAdapterMenuItem;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView appCompatTextViewTitleItem;
        AppCompatImageView appCompatImageViewIconItem;
        CardView cardViewItem;
        Context context;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            appCompatImageViewIconItem = (AppCompatImageView) itemView.findViewById(R.id.idAppCompatImageViewItemMenu);
            appCompatTextViewTitleItem = (AppCompatTextView) itemView.findViewById(R.id.idAppCompatTextViewItemMenu);
            cardViewItem = (CardView) itemView.findViewById(R.id.idCardViewItemMenu);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_menu, parent, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    String TAG = "MenuItemAda";
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.appCompatTextViewTitleItem.setText(pojoItemsMenuList.get(position).getNameItem());
        holder.appCompatImageViewIconItem.setImageResource(pojoItemsMenuList.get(position).getIdIcon());

        holder.cardViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbSivad = new DBSivad(holder.context);
                formatDates = new FormatDates();

                ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
                ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                int idUser = responseLoginUser.getData().getIdUser();
                String dateRegisterVehicleInspection = dbSivad.getLastRegistrationDateInspectionPreoperational(idUser);
                String dateResponseAlertNotificationEmail = dbSivad.getDateResponseAlertNotificationEmail(idUser);

                SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");
                Date dateOld = null;
                Date dateCurrent = null;

                Log.v("dateInspection", dateRegisterVehicleInspection);
                Log.v("dateAlert", dateResponseAlertNotificationEmail);
                Log.d(TAG, "onClick: " + responseValidatePlateMileage.getData());
                Log.v("numberInspections", String.valueOf(responseValidatePlateMileage.getData().getNumberInspections()));

                int numInspectionVehicle = responseValidatePlateMileage.getData().getNumberInspections();
                int numRowInspections = dbSivad.getNumRowsOfInspections(responseLoginUser.getData().getIdUser());

                if (numRowInspections == numInspectionVehicle || numRowInspections > numInspectionVehicle) {

                    //Este es el codigo por inspecciones realizadas bien hechas ya sea offline u online
                    try {
                        dateOld = format2.parse(dateRegisterVehicleInspection);

                        Date date = new Date();
                        String dateCurrentCompare = format2.format(date).toString();
                        dateCurrent = format2.parse(dateCurrentCompare);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    int resultCompareDates2 = dateOld.compareTo(dateCurrent);

                    if (resultCompareDates2 == 0) {
                        interfacesAdapterMenuItem.validateInspectionEnabled(responseValidatePlateMileage.getData().getPlate(), numRowInspections, numInspectionVehicle);
                    } else if (resultCompareDates2 < 0) {//Esta parte valida si es un dia despues de la ultima inspeccion realizada
                        //dbSivad.deleteDataResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser());
                        dbSivad.deleteDataVehicleInspection(responseLoginUser.getData().getIdUser());
                        dbSivad.deleteDataInspection(responseLoginUser.getData().getIdUser());

                        //Actualizo el numero de inspecciones por dia
                        responseValidatePlateMileage.getData().setNumberInspections(2);
                        Gson gson = new Gson();
                        String json = gson.toJson(responseValidatePlateMileage);
                        int idDelete = dbSivad.deleteDataVehicle();
                        Long idAdd = dbSivad.addDataVehicle(formatDates.getDateRegisterFormatDateWithTime(), json);

                        //saveValuePreference(holder.context, false);
                        Intent intent = null;

                        if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento))) {
                            intent = new Intent(holder.context, ViewInspecionPreoperacional.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientofurgones))) {
                            intent = new Intent(holder.context, ViewAlistamientoPasajerosFurgones.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientointermunicipal))) {
                            intent = new Intent(holder.context, ViewAlistamientoPasajerosIntermunicipal.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientourbano))) {
                            intent = new Intent(holder.context, ViewAlistamientosPasajerosUrbano.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento_carga_furgones))) {
                            intent = new Intent(holder.context, ViewAlistamientoCargaFurgones.class);

                        } else {
                            intent = new Intent(holder.context, ViewAlistamientosPasajerosLujo.class);
                        }

                        holder.context.startActivity(intent);
                    }

                } else {
                    Intent intent = null;

                    if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento))) {
                        intent = new Intent(holder.context, ViewInspecionPreoperacional.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientofurgones))) {
                        intent = new Intent(holder.context, ViewAlistamientoPasajerosFurgones.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientointermunicipal))) {
                        intent = new Intent(holder.context, ViewAlistamientoPasajerosIntermunicipal.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientourbano))) {
                        intent = new Intent(holder.context, ViewAlistamientosPasajerosUrbano.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento_carga_furgones))) {
                        intent = new Intent(holder.context, ViewAlistamientoCargaFurgones.class);

                    } else {
                        intent = new Intent(holder.context, ViewAlistamientosPasajerosLujo.class);
                    }

                    holder.context.startActivity(intent);
                }


                /*if (!dateRegisterVehicleInspection.isEmpty()) {


                    Log.v("dateRegisterVehicleIn", dateRegisterVehicleInspection);
                    try {
                        dateOld = format2.parse(dateRegisterVehicleInspection);

                        Date date = new Date();
                        String dateCurrentCompare = format2.format(date).toString();
                        dateCurrent = format2.parse(dateCurrentCompare);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    int resultCompareDates2 = dateOld.compareTo(dateCurrent);
                    Log.v("resultCompareDates2", String.valueOf(resultCompareDates2));
                    if (resultCompareDates2 == 0) {
                        interfacesAdapterMenuItem.validateInspectionEnabled(responseValidatePlateMileage.getData().getPlate());
                    } else if (resultCompareDates2 < 0) {
                        dbSivad.deleteDataResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser());
                        dbSivad.deleteDataVehicleInspection(responseLoginUser.getData().getIdUser());
                        //interfacesAdapterMenuItem.updateInspectionEnabled(responseValidatePlateMileage.getData().getPlate());
                        saveValuePreference(holder.context, false);

                        Intent intent = null;

                        if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento))) {
                            intent = new Intent(holder.context, ViewInspecionPreoperacional.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientofurgones))) {
                            intent = new Intent(holder.context, ViewAlistamientoPasajerosFurgones.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientointermunicipal))) {
                            intent = new Intent(holder.context, ViewAlistamientoPasajerosIntermunicipal.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientourbano))) {
                            intent = new Intent(holder.context, ViewAlistamientosPasajerosUrbano.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento_carga_furgones))) {
                            intent = new Intent(holder.context, ViewAlistamientoCargaFurgones.class);

                        } else {
                            intent = new Intent(holder.context, ViewAlistamientosPasajerosLujo.class);
                        }

                        holder.context.startActivity(intent);
                    }


                } else if (!dateResponseAlertNotificationEmail.isEmpty()) {


                    try {
                        dateOld = format2.parse(dateResponseAlertNotificationEmail);

                        Date date = new Date();
                        String dateCurrentCompare = format2.format(date).toString();
                        dateCurrent = format2.parse(dateCurrentCompare);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    int resultCompareDates2 = dateOld.compareTo(dateCurrent);
                    Log.v("resultCompareDates3", String.valueOf(resultCompareDates2));
                    if (resultCompareDates2 == 0) {

                        interfacesAdapterMenuItem.validateInspectionEnabled(responseValidatePlateMileage.getData().getPlate());

                    } else if (resultCompareDates2 < 0) {
                        dbSivad.deleteDataResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser());
                        dbSivad.deleteDataVehicleInspection(responseLoginUser.getData().getIdUser());
                        //interfacesAdapterMenuItem.updateInspectionEnabled(responseValidatePlateMileage.getData().getPlate());
                        saveValuePreference(holder.context, false);
                        Intent intent = null;

                        if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento))) {
                            intent = new Intent(holder.context, ViewInspecionPreoperacional.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientofurgones))) {
                            intent = new Intent(holder.context, ViewAlistamientoPasajerosFurgones.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientointermunicipal))) {
                            intent = new Intent(holder.context, ViewAlistamientoPasajerosIntermunicipal.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientourbano))) {
                            intent = new Intent(holder.context, ViewAlistamientosPasajerosUrbano.class);

                        } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento_carga_furgones))) {
                            intent = new Intent(holder.context, ViewAlistamientoCargaFurgones.class);

                        } else {
                            intent = new Intent(holder.context, ViewAlistamientosPasajerosLujo.class);
                        }

                        holder.context.startActivity(intent);
                    }

                } else {
                    Intent intent = null;

                    if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento))) {
                        intent = new Intent(holder.context, ViewInspecionPreoperacional.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientofurgones))) {
                        intent = new Intent(holder.context, ViewAlistamientoPasajerosFurgones.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientointermunicipal))) {
                        intent = new Intent(holder.context, ViewAlistamientoPasajerosIntermunicipal.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamientourbano))) {
                        intent = new Intent(holder.context, ViewAlistamientosPasajerosUrbano.class);

                    } else if (pojoItemsMenuList.get(position).getNameItem().equals(holder.context.getResources().getString(R.string.text_title_item_dash_alistamiento_carga_furgones))) {
                        intent = new Intent(holder.context, ViewAlistamientoCargaFurgones.class);

                    } else {
                        intent = new Intent(holder.context, ViewAlistamientosPasajerosLujo.class);
                    }

                    holder.context.startActivity(intent);
                }*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return pojoItemsMenuList.size();
    }

}

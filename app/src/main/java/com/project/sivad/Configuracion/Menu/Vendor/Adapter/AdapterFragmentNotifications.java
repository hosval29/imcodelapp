package com.project.sivad.Configuracion.Menu.Vendor.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataNotifications;
import com.project.sivad.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFragmentNotifications extends RecyclerView.Adapter<AdapterFragmentNotifications.ViewHolder> {

    List<PojoDataNotifications> pojoDataNotificationsList = new ArrayList<>();

    public AdapterFragmentNotifications(List<PojoDataNotifications> pojoDataNotificationsList) {
        this.pojoDataNotificationsList = pojoDataNotificationsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_notifications, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String typeAlert = "Vacio";
        String dateExpirated = "Vacio";
        String plateVehicle = "Vacio";

        if (pojoDataNotificationsList.get(position).getDateExpirated() != null){
          typeAlert = pojoDataNotificationsList.get(position).getTitle();
        }

        if(pojoDataNotificationsList.get(position).getDateExpirated() != null){
           dateExpirated = pojoDataNotificationsList.get(position).getDateExpirated();
        }

        if(pojoDataNotificationsList.get(position).getPlateVehicle() != null){
            plateVehicle = pojoDataNotificationsList.get(position).getPlateVehicle();
        }

        holder.idAppCompatTextviewTypeAlert.setText(typeAlert);
        holder.idAppCompatTextviewDateExpirated.setText(dateExpirated);
        holder.idAppCompatTextviewPlateVehicle.setText(plateVehicle);
    }

    @Override
    public int getItemCount() {
        return pojoDataNotificationsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.idAppCompatImageViewIconNovelties)
        AppCompatImageView idAppCompatImageViewIconNovelties;
        @BindView(R.id.idAppCompatTextviewTypeAlert)
        AppCompatTextView idAppCompatTextviewTypeAlert;
        @BindView(R.id.idAppCompatTextviewDateExpirated)
        AppCompatTextView idAppCompatTextviewDateExpirated;
        @BindView(R.id.idAppCompatTextviewPlateVehicle)
        AppCompatTextView idAppCompatTextviewPlateVehicle;
        @BindView(R.id.idLinearLayoutCompatExpandle)
        LinearLayoutCompat idLinearLayoutCompatExpandle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

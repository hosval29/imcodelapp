package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Adapter.AdapterMenuItem;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoItemsMenu;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoCargaFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoPasajerosFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientoPasajerosIntermunicipal;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientosPasajerosLujo;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewAlistamientosPasajerosUrbano;
import com.project.sivad.Modulos.InspeccionPreoperacional.View.ViewInspecionPreoperacional;
import com.project.sivad.R;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class FragmentHome extends Fragment implements InterfacesMenu.InterfaceFragmentHome, InterfacesMenu.InterfacesAdapterMenuItem {

    @BindView(R.id.idRecyclerViewItemsMenu)
    RecyclerView idRecyclerViewItemsMenu;

    //Declaramos las variables String y int
    private int networkState;

    //Declaramos las variables boolean
    public static boolean isConnect = false;

    private View view;
    private Context context;
    private GridLayoutManager gridLayoutManager;

    private AdapterMenuItem adapterMenuItem;
    private List<PojoItemsMenu> pojoItemsMenuList = new ArrayList<>();

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Utils Object
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;
    private ResponseDataTypesVehicularInspectios responseDataTypesVehicularInspectios;

    //Instanciamos la Interfaces
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        dbSivad = new DBSivad(context);
        progressDialog = new ProgressDialog(context);
        snackBarPersonalized = new SnackBarPersonalized(getActivity());
        interfacesPresenterMenu = new PresenterMenu(null
                , null
                , null
                , null
                , null
                , null
                , this
                , null
                , context);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        idRecyclerViewItemsMenu.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(context, 2);
        gridLayoutManager.setOrientation(RecyclerView.VERTICAL);
        idRecyclerViewItemsMenu.setLayoutManager(gridLayoutManager);
        adapterMenuItem = new AdapterMenuItem(pojoItemsMenuList, this);
        idRecyclerViewItemsMenu.setAdapter(adapterMenuItem);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("estoy aqui", "onResume");
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        getDataTypesVehicularInspection();
        setupMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };


    private void getDataTypesVehicularInspection() {
        responseDataTypesVehicularInspectios = dbSivad.getDataTypesVehicularInspection();
        if (responseDataTypesVehicularInspectios == null || responseDataTypesVehicularInspectios.getData().size() == 0) {
            interfacesPresenterMenu.getDataTypesVehicularInspection();
        }
    }

    private void setupMenu() {

        pojoItemsMenuList.clear();
        responseDataTypesVehicularInspectios = dbSivad.getDataTypesVehicularInspection();
        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        Log.d("setupMenu", "setupMenu: " + responseValidatePlateMileage.toString());
        if (responseDataTypesVehicularInspectios != null) {

            JsonArray jsonArray = responseDataTypesVehicularInspectios.getData().getAsJsonArray();
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                String typeVehicularInspection = jsonObject.get("type").getAsString();
                Log.v("typeVehicularInspection", typeVehicularInspection);

                ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                Log.v("responseLoginUser", responseLoginUser.toString());
                if (responseLoginUser != null) {
                    JsonArray jsonArrayDataModules = responseLoginUser.getData().getModules();
                    Log.v("jsonArrayDataModules", jsonArrayDataModules.toString());
                    for (int j = 0; j < jsonArrayDataModules.size(); j++) {
                        JsonObject jsonObjectModule = jsonArrayDataModules.get(j).getAsJsonObject();
                        JsonArray jsonArrayDataItems = jsonObjectModule.get("items").getAsJsonArray();

                        for (int k = 0; k < jsonArrayDataItems.size(); k++) {
                            JsonObject jsonObjectItem = jsonArrayDataItems.get(k).getAsJsonObject();
                            String itemModule = jsonObjectItem.get("itemModule").getAsString();
                            Log.v("itemModule", itemModule);

                            if (typeVehicularInspection.equals(itemModule) && responseValidatePlateMileage.getData().getTypeInspection().equals(itemModule)) {
                                PojoItemsMenu pojoItemsMenu = new PojoItemsMenu(itemModule
                                        , R.drawable.item_menu_alistamiento
                                        , "Carga");

                                pojoItemsMenuList.add(pojoItemsMenu);
                            }
                        }
                    }

                } else {
                    snackBarPersonalized.showSnackBarErrorCualquiera("No existen datos del Usuario.");
                }
            }

            adapterMenuItem.notifyDataSetChanged();
        } else {
            Log.v("esta vacio", "jajaja");
        }
    }

    @Override
    public void responseErrorGetDataTypesVehicularInspections(ResponseError responseError) {
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccesGetDataTypesVehicularInspections(ResponseDataTypesVehicularInspectios responseDataTypesVehicularInspectios) {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreated = ft.format(date).toString();
        Gson gson = new Gson();
        String json = gson.toJson(responseDataTypesVehicularInspectios);
        Long rowFilesAffected = dbSivad.addDataTypesVehicularInspectios(dateCreated, json);
        Log.v("rowFilesAffected", String.valueOf(rowFilesAffected));
        setupMenu();
    }

    @Override
    public void validateInspectionEnabled(String plateVehicle, int numRowInspectionsPerformed, int numInspectionsVehicles) {
        //if (isConnect) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Usted sobrepasó el limite de inspecciones habilitadas para este vehículo\n"+
                    "Inspccciones Realizadas (" + numRowInspectionsPerformed +")" + " || Inspeciones Habilitadas Vehículo (" + numInspectionsVehicles + ")\n" +
                    "Si desea realizar una siguiente inspección, presione (Ok) para solicitar la petición.\n" +
                    "O presione (No) para salir")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.setMessage(ConstantsStrings.LOAD);
                            progressDialog.show();
                            interfacesPresenterMenu.getValidateInspectionEnabled(plateVehicle);
                            dialog.dismiss();
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.setTitle("Bloqueo de Inspección");
            dialog.show();

        /*} else {
            snackBarPersonalized.showSnackBarErrorCualquiera(ConstantsStrings.NO_CONEXCION_INTERNET);
        }*/
    }

    @Override
    public void responseErrorGetValidateInspectionEnabled(ResponseError responseError) {
        progressDialog.dismiss();
        snackBarPersonalized.showSnackBarResponse(responseError.getErrorcode(), responseError.getMessage());
        saveValuePreference(context, false);
    }

    @Override
    public void responseSuccessGetValidateInspectionEnabled(ResponseValideteInspectionEnabled responseValideteInspectionEnabled) {
        //aqui debe guardarlo en true
        progressDialog.dismiss();
        Log.v("responseVaIsInsEHo", String.valueOf(responseValideteInspectionEnabled.getData().isEnabled()));
        saveValuePreference(context, responseValideteInspectionEnabled.getData().isEnabled());

        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();

        for (int position = 0; position < pojoItemsMenuList.size(); position++) {
            Log.v("isInspectionEnabledAd", String.valueOf(getValuePreference(context)));
            if (getValuePreference(context)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Opción bloqueda. Esta opcion aun no se encuentra habilitada.\n" +
                        "Contactese con el administrador de la plataforma para habilitar dicha opción")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.setTitle("Bloqueo de Inspección");
                dialog.show();
            } else {
                dbSivad.deleteDataResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser());
                dbSivad.deleteDataVehicleInspection(responseLoginUser.getData().getIdUser());
                Intent intent = null;

                if (pojoItemsMenuList.get(position).getNameItem().equals(context.getResources().getString(R.string.text_title_item_dash_alistamiento))) {
                    intent = new Intent(context, ViewInspecionPreoperacional.class);

                } else if (pojoItemsMenuList.get(position).getNameItem().equals(context.getResources().getString(R.string.text_title_item_dash_alistamientofurgones))) {
                    intent = new Intent(context, ViewAlistamientoPasajerosFurgones.class);

                } else if (pojoItemsMenuList.get(position).getNameItem().equals(context.getResources().getString(R.string.text_title_item_dash_alistamientointermunicipal))) {
                    intent = new Intent(context, ViewAlistamientoPasajerosIntermunicipal.class);

                } else if (pojoItemsMenuList.get(position).getNameItem().equals(context.getResources().getString(R.string.text_title_item_dash_alistamientourbano))) {
                    intent = new Intent(context, ViewAlistamientosPasajerosUrbano.class);

                } else if (pojoItemsMenuList.get(position).getNameItem().equals(context.getResources().getString(R.string.text_title_item_dash_alistamiento_carga_furgones))) {
                    intent = new Intent(context, ViewAlistamientoCargaFurgones.class);

                } else {
                    intent = new Intent(context, ViewAlistamientosPasajerosLujo.class);
                }

                context.startActivity(intent);
            }
        }
    }


    /*@Override
    public void updateInspectionEnabled(String plate) {
        interfacesPresenterMenu.putInspectionEnabled(plate);
    }

    @Override
    public void responseErrorPutInspectionEnabled(ResponseError responseError) {
        snackBarPersonalized.showSnackbarWithAction(responseError.getErrorcode(), responseError.getMessage());
    }

    @Override
    public void responseSuccessPutInspectionEnabled(ResponseJson responseJson) {
        snackBarPersonalized.showSnackBarResponse(responseJson.getCodeStatus(), responseJson.getMessage());
    }*/

    private String PREFS_KEY = "MySharedPreferences";

    public void saveValuePreference(Context context, boolean isInspectionEnabled) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putBoolean("isInspectionEnabled", isInspectionEnabled);
        editor.commit();
    }

    public boolean getValuePreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        return preferences.getBoolean("isInspectionEnabled", false);
    }

}

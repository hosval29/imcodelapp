package com.project.sivad.Configuracion.Menu.Vendor.Pojo;

public class PojoDetailNovelties {
    String date;
    String observation;
    String typeNoveltie;

    public PojoDetailNovelties(String date, String observation, String typeNoveltie) {
        this.date = date;
        this.observation = observation;
        this.typeNoveltie = typeNoveltie;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getTypeNoveltie() {
        return typeNoveltie;
    }

    public void setTypeNoveltie(String typeNoveltie) {
        this.typeNoveltie = typeNoveltie;
    }
}

package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.http.Response.ResponseLoginUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class FragmentWelcome extends Fragment {

    @BindView(R.id.idAppCompatTextViewNameUser)
    AppCompatTextView idAppCompatTextViewNameUser;
    @BindView(R.id.idConstraintLayoutSplah)
    CoordinatorLayout idConstraintLayoutSplah;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Utils Objects
    private Context context;

    public FragmentWelcome() {
        // Required empty public constructor
    }

    public static FragmentWelcome newInstance(String param1, String param2) {
        FragmentWelcome fragment = new FragmentWelcome();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        context = getContext();
        dbSivad = new DBSivad(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        ButterKnife.bind(this, view);

        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        String nameUser = responseLoginUser.getData().getName();
        String lastNameUser = responseLoginUser.getData().getLastName();

        idAppCompatTextViewNameUser.setText(nameUser + " " + lastNameUser);

        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        saveValuePreference(context, true);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private String PREFS_KEY = "MySharedPreferences";

    public void saveValuePreference(Context context, boolean isWelcome) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putBoolean("welcome", isWelcome);
        editor.commit();
    }


}

package com.project.sivad.Configuracion.Menu.Vendor.Adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataNovelties;
import com.project.sivad.R;
import com.project.sivad.Util.ExpandAnimation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFragmentNovelties extends RecyclerView.Adapter<AdapterFragmentNovelties.ViewHolder> {

    private OnClickListener listener;

    private List<PojoDataNovelties> pojoDataNoveltiesList;

    public AdapterFragmentNovelties(List<PojoDataNovelties> pojoDataNoveltiesList, OnClickListener listener) {
        this.pojoDataNoveltiesList = pojoDataNoveltiesList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_fragment_novelties, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return pojoDataNoveltiesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.idAppCompatImageViewIconNovelties)
        AppCompatImageView idAppCompatImageViewIconNovelties;
        @BindView(R.id.idAppCompatTextviewIdInspection)
        AppCompatTextView idAppCompatTextviewIdInspection;
        @BindView(R.id.idAppCompatTextviewDate)
        AppCompatTextView idAppCompatTextviewDate;
        @BindView(R.id.idAppCompatTextviewPlateVehicle)
        AppCompatTextView idAppCompatTextviewPlateVehicle;
        @BindView(R.id.idAppCompatTextviewDateNoveltie)
        AppCompatTextView idAppCompatTextviewDateNoveltie;
        @BindView(R.id.idAppCompatTextviewTimeNoveltie)
        AppCompatTextView idAppCompatTextviewTimeNoveltie;
        @BindView(R.id.idAppCompatTextviewTypeNoveltie)
        AppCompatTextView idAppCompatTextviewTypeNoveltie;
        @BindView(R.id.idAppCompatTextviewObservationNoveltie)
        AppCompatTextView idAppCompatTextviewObservationNoveltie;
        @BindView(R.id.idLinearLayoutCompatExpandleDetail)
        LinearLayoutCompat idLinearLayoutCompatExpandleDetail;
        @BindView(R.id.idLinearLayoutCompatExpandle)
        LinearLayoutCompat idLinearLayoutCompatExpandle;

        boolean expanded;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);

        }

        public void bind(int position) {
            ViewGroup.LayoutParams params = idLinearLayoutCompatExpandle.getLayoutParams();
            params.height = expanded ?
                    itemView.getResources().getDimensionPixelOffset(R.dimen.card_expanded_height)
                    : itemView.getResources().getDimensionPixelOffset(R.dimen.card_default_height);
            idLinearLayoutCompatExpandle.setLayoutParams(params);
            idLinearLayoutCompatExpandleDetail.setAlpha(expanded ? 1.0f : 0.0f);

            idAppCompatTextviewIdInspection.setText(String.valueOf(pojoDataNoveltiesList.get(position).getIdInspection()));
            idAppCompatTextviewDate.setText(pojoDataNoveltiesList.get(position).getDateRegister());
            idAppCompatTextviewPlateVehicle.setText(pojoDataNoveltiesList.get(position).getPlateVehicle());
            idAppCompatTextviewTypeNoveltie.setText(pojoDataNoveltiesList.get(position).getDescriptionNoveltie());
            idAppCompatTextviewDateNoveltie.setText(pojoDataNoveltiesList.get(position).getDateNoveltie());
            idAppCompatTextviewTimeNoveltie.setText(pojoDataNoveltiesList.get(position).getTimeNoveltie());
            idAppCompatTextviewObservationNoveltie.setText(pojoDataNoveltiesList.get(position).getObservationNoveltie());
        }

        @Override
        public void onClick(View v) {
            expandOrCollapse();
        }

        private void expandOrCollapse() {
            int expandedHeight = itemView.getResources()
                    .getDimensionPixelOffset(R.dimen.card_expanded_height);
            int defaultHeight = itemView.getResources()
                    .getDimensionPixelOffset(R.dimen.card_default_height);

            if (expanded) {
                int tmp = defaultHeight;
                defaultHeight = expandedHeight;
                expandedHeight = tmp;
            }

            ExpandAnimation expandAnimation = new ExpandAnimation(idLinearLayoutCompatExpandle, defaultHeight,
                    expandedHeight);
            expandAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
            expandAnimation.setDuration(375);
            expandAnimation.start();

            idLinearLayoutCompatExpandleDetail.animate().alpha(expanded ? 0.0f : 1.0f).setStartDelay(75);
            expanded = !expanded;
        }
    }

    public interface OnClickListener {
        void onItemClick(View sharedView, String transitionName, int position);
    }
}

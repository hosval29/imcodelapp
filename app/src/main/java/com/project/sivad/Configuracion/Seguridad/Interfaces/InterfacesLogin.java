package com.project.sivad.Configuracion.Seguridad.Interfaces;

import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseLoginUser;

public interface InterfacesLogin {
    interface InterfacesViewFragmentCodeSetting{
        void responseErrorGetDataCodeSetting(ResponseError responseError);
        void responseSuccessgetDataCodeSetting(ResponseDataCode responseDataCode);
    }
    interface InterfacesViewLogin{

        void responseSuccessGetDataLoginUser(ResponseLoginUser responseLoginUser);
        void responseErrorGetDataLoginUser(ResponseError responseError);

    }
    interface InterfacesPresenterLogin{

        void responseErrorGetDataCodeSetting(ResponseError responseError);
        void responseSuccessgetDataCodeSetting(ResponseDataCode responseDataCode);
        void postDataCodeSetting(String codeSetting);

        void responseSuccessGetDataLoginUser(ResponseLoginUser responseLoginUser);
        void responseErrorGetDataLoginUser(ResponseError responseError);
        void getDataLoginUser(String user, String pass, String imei, String token);

    }
    interface InterfacesModelLogin{

        void postDataCodeSetting(String codeSetting);
        void getDataLoginUser(String user, String pass, String imei, String token);
    }
}

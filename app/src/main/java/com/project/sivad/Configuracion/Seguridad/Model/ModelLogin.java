package com.project.sivad.Configuracion.Seguridad.Model;

import android.content.Context;
import android.util.Log;

import com.project.sivad.Configuracion.Seguridad.Interfaces.InterfacesLogin;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.http.ApiAdapter;
import com.project.sivad.http.ApiAdapterClient;
import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.sivad.http.Response.ResponseLoginUser;

import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModelLogin implements InterfacesLogin.InterfacesModelLogin {
    private InterfacesLogin.InterfacesPresenterLogin interfacesPresenterLogin;
    private ApiAdapter apiAdapter;
    ApiAdapterClient apiAdapterClient;
    private final JSONObject jsonBody;
    Context context;

    public ModelLogin(InterfacesLogin.InterfacesPresenterLogin interfacesPresenterLogin, Context context) {
        this.interfacesPresenterLogin = interfacesPresenterLogin;
        this.apiAdapter = new ApiAdapter(context);
        this.jsonBody = new JSONObject();
        this.context = context;
        this.apiAdapterClient = new ApiAdapterClient(context);
    }

    @Override
    public void postDataCodeSetting(String codeSetting) {

        Call<ResponseDataCode> call = apiAdapter.getDataByCodeSetting(codeSetting);
        call.enqueue(new Callback<ResponseDataCode>() {
            @Override
            public void onResponse(Call<ResponseDataCode> call, Response<ResponseDataCode> response) {

                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {
                    interfacesPresenterLogin.responseSuccessgetDataCodeSetting(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterLogin.responseSuccessgetDataCodeSetting(response.body());
                } else {
                    if(response.code() == 404 && response.message().equals("Not Found")){
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Sin conexión al servidor");
                        interfacesPresenterLogin.responseErrorGetDataCodeSetting(responseError1);*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        interfacesPresenterLogin.responseErrorGetDataCodeSetting(responseError);
                        //return;
                    }else{
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterLogin.responseErrorGetDataCodeSetting(responseError);
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponseDataCode> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage(ConstantsStrings.NO_CONEXCION_INTERNET_SERVER);
                interfacesPresenterLogin.responseErrorGetDataCodeSetting(responseError);
            }
        });
    }

    @Override
    public void getDataLoginUser(String user, String pass, String imei, String token) {
        DBSivad dbSivad = new DBSivad(context);
        ResponseDataCode responseDataCode = dbSivad.getDataCodeSetting();
        Log.v("responsecodemodel", responseDataCode.toString());
        if(responseDataCode != null){
            Call<ResponseLoginUser> call = apiAdapterClient.getDataLoginUser(user, pass, imei, token);
            call.enqueue(new Callback<ResponseLoginUser>() {
                @Override
                public void onResponse(Call<ResponseLoginUser> call, Response<ResponseLoginUser> response) {
                    Gson gson = new GsonBuilder().create();
                    ResponseError responseError = null;
                    if (response.code() == 200) {
                        interfacesPresenterLogin.responseSuccessGetDataLoginUser(response.body());
                    } else if (response.code() == 201) {
                        interfacesPresenterLogin.responseSuccessGetDataLoginUser(response.body());
                    } else {
                        if(response.code() == 404 && response.message().equals("Not Found")){
                            try {
                                responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            interfacesPresenterLogin.responseErrorGetDataLoginUser(responseError);
                            return;
                        }else if(response.code() == 502){
                            ResponseError responseError1 = new ResponseError();
                            responseError1.setErrorcode(500);
                            responseError1.setMessage(ConstantsStrings.NO_CONEXCION_INTERNET_SERVER);
                            interfacesPresenterLogin.responseErrorGetDataLoginUser(responseError1);
                        }else{
                            try {
                                responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            interfacesPresenterLogin.responseErrorGetDataLoginUser(responseError);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseLoginUser> call, Throwable t) {
                    ResponseError responseError = new ResponseError();
                    responseError.setErrorcode(500);
                    responseError.setMessage(ConstantsStrings.NO_CONEXCION_INTERNET_SERVER);
                    interfacesPresenterLogin.responseErrorGetDataLoginUser(responseError);
                }
            });
        }else{
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Este Cliente aún no tiene Url asignada, Valide con el administrador de la plataforma.");
            interfacesPresenterLogin.responseErrorGetDataLoginUser(responseError);
        }

    }

}

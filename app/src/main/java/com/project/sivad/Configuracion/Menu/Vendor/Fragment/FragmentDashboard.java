package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;
import com.github.mikephil.charting.charts.PieChart;


public class FragmentDashboard extends Fragment {

    private PieChart pieChart;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        PieView pieView = (PieView) view.findViewById(R.id.pieView);

        PieAngleAnimation animation = new PieAngleAnimation(pieView);
        animation.setDuration(5000); //This is the duration of the animation in millis
        pieView.startAnimation(animation);

        pieView.setPercentageBackgroundColor(getResources().getColor(R.color.colorPrimary));
        pieView.setMainBackgroundColor(getResources().getColor(R.color.colorSecondary));
        pieView.setTextColor(getResources().getColor(R.color.colorTextPrimary));
        pieView.setInnerTextVisibility(View.VISIBLE);

        return view;
    }

}

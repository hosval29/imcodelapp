package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.View.ViewMenu;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentValidacionPlacaKilometraje extends DialogFragment implements InterfacesMenu.InterfacesFragmentValidatePlateMeleage {

    //Variables XML
    @BindView(R.id.linearLayoutCompat)
    LinearLayoutCompat linearLayoutCompat;
    @BindView(R.id.idTextInputLayoutPlaca)
    TextInputLayout idTextInputLayoutPlaca;
    @BindView(R.id.idTextInputLayoutKilometraje)
    TextInputLayout idTextInputLayoutKilometraje;
    @BindView(R.id.linearLayoutCompat2)
    LinearLayoutCompat linearLayoutCompat2;
    @BindView(R.id.idBtnAceptar)
    AppCompatButton idBtnAceptar;
    @BindView(R.id.idAppCompatTextviewError)
    AppCompatTextView idAppCompatTextviewError;
    @BindView(R.id.idLinearLayoutError)
    LinearLayoutCompat idLinearLayoutError;
    @BindView(R.id.idBtnCancelar)
    MaterialButton idBtnCancelar;

    //Declaramos las Interfaces
    InterfacesFragmentValidatePlacaKilometraje interfacesFragmentValidatePlacaKilometraje;
    InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    //Objects Utils
    private ProgressDialog progressDialog;
    private Context context;

    public interface InterfacesFragmentValidatePlacaKilometraje {
        void responseGetPlateKilometraje(ResponseValidatePlateMileage responseValidatePlateMileage);
    }

    public FragmentValidacionPlacaKilometraje() {
        // Required empty public constructor
    }


    public static FragmentValidacionPlacaKilometraje newInstance(String param1, String param2) {
        FragmentValidacionPlacaKilometraje fragment = new FragmentValidacionPlacaKilometraje();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InterfacesFragmentValidatePlacaKilometraje) {
            interfacesFragmentValidatePlacaKilometraje = (InterfacesFragmentValidatePlacaKilometraje) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        context = getContext();
        progressDialog = new ProgressDialog(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_validacion_placa_kilometraje, container, false);
        ButterKnife.bind(this, view);

        interfacesPresenterMenu = new PresenterMenu(null
                , this
                , null
                , null
                , null
                , null
                , null
                , null
                , getContext());

        idTextInputLayoutKilometraje.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                idTextInputLayoutKilometraje.setErrorEnabled(false);
                if (idTextInputLayoutPlaca.isErrorEnabled()) {
                    idTextInputLayoutPlaca.setErrorEnabled(false);
                }


                idLinearLayoutError.setVisibility(View.GONE);
                idAppCompatTextviewError.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                idTextInputLayoutPlaca.getEditText().setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutPlaca.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                idTextInputLayoutPlaca.setErrorEnabled(false);
                if (idTextInputLayoutKilometraje.isErrorEnabled()) {
                    idTextInputLayoutKilometraje.setErrorEnabled(false);
                }

                idLinearLayoutError.setVisibility(View.GONE);
                idAppCompatTextviewError.setText("");

                idTextInputLayoutPlaca.getEditText().setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                idTextInputLayoutPlaca.getEditText().setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            }

            @Override
            public void afterTextChanged(Editable editable) {
                editable.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            }
        });

        idBtnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validatesInpust()) {
                    String plate = idTextInputLayoutPlaca.getEditText().getText().toString().trim().toUpperCase();
                    int mileage = Integer.parseInt(idTextInputLayoutKilometraje.getEditText().getText().toString().trim());
                    if (ViewMenu.isConnect){
                        idAppCompatTextviewError.setText("");
                        idLinearLayoutError.setVisibility(View.GONE);
                        progressDialog.setMessage(ConstantsStrings.LOAD);
                        progressDialog.show();
                        interfacesPresenterMenu.getDataPlacaKilometraje(plate, mileage);
                    }else{
                        idAppCompatTextviewError.setText(ConstantsStrings.NO_CONEXCION_INTERNET);
                        idLinearLayoutError.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interfacesFragmentValidatePlacaKilometraje = null;
    }

    private boolean validatesInpust() {
        boolean error = false;

        if (idTextInputLayoutKilometraje.getEditText().getText().toString().trim().matches("")) {
            idTextInputLayoutKilometraje.setErrorEnabled(true);
            idTextInputLayoutKilometraje.setError("Campo Placa obligatorio");
            error = true;
        }

        if (idTextInputLayoutPlaca.getEditText().getText().toString().trim().length() > 0){
            Log.v("length plate", String.valueOf(idTextInputLayoutPlaca.getEditText().getText().toString().trim().length()));
            if(idTextInputLayoutPlaca.getEditText().getText().toString().trim().length() > 6){
                idTextInputLayoutPlaca.setErrorEnabled(true);
                idTextInputLayoutPlaca.setError("Número de caracteres no validos!");
                error = true;
            }
        }

        if (idTextInputLayoutPlaca.getEditText().getText().toString().trim().matches("")) {
            idTextInputLayoutPlaca.setErrorEnabled(true);
            idTextInputLayoutPlaca.setError("Campo Kilometraje obligatorio");
            error = true;
        }

        return error;
    }

    @Override
    public void responseErrorGetDataPlacaKilometraje(ResponseError responseError) {
        progressDialog.dismiss();
        idLinearLayoutError.setVisibility(View.VISIBLE);
        idAppCompatTextviewError.setText(responseError.getMessage());
    }

    @Override
    public void responseSuccessGetDataPlacaKilometraje(ResponseValidatePlateMileage responseValidatePlateMileage) {
        progressDialog.dismiss();
        interfacesFragmentValidatePlacaKilometraje.responseGetPlateKilometraje(responseValidatePlateMileage);
        dismiss();
    }

}

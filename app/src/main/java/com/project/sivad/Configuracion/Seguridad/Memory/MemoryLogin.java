package com.project.sivad.Configuracion.Seguridad.Memory;

import com.project.sivad.Configuracion.Seguridad.Pojo.User;

//en esta clase se llaman los metodos que interactuan con la base de datos
//es decir permite una relacion con los metodos de la BD
public class MemoryLogin implements RepositoryLogin {

    private User user;
    @Override
    public void saveUser(User user) {
        //aqui hay que quitar esto y poner el metodo de guaradado de la base de dato
        if(user == null){
            user = getUser();
        }
        this.user = user;
    }

    @Override
    public User getUser() {
        if (user == null){
            user = new User("Imcodel", "1234");
            user.setId(0);
            return user;
        }else {
            return user;
        }

    }
}

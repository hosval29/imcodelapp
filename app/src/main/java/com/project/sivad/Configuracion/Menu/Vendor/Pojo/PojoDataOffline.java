package com.project.sivad.Configuracion.Menu.Vendor.Pojo;

public class PojoDataOffline {
    int idRegister;
    String dateRegister;
    String titleModule;
    String data;

    public PojoDataOffline(int idRegister, String dateRegister, String titleModule, String data) {
        this.idRegister = idRegister;
        this.dateRegister = dateRegister;
        this.titleModule = titleModule;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getIdRegister() {
        return idRegister;
    }

    public void setIdRegister(int idRegister) {
        this.idRegister = idRegister;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getTitleModule() {
        return titleModule;
    }

    public void setTitleModule(String titleModule) {
        this.titleModule = titleModule;
    }
}

package com.project.sivad.Configuracion.Menu.Vendor.Pojo;

public class PojoDataNotifications {
    String date;
    String title;
    String dateExpirated;
    String plateVehicle;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateExpirated() {
        return dateExpirated;
    }

    public void setDateExpirated(String dateExpirated) {
        this.dateExpirated = dateExpirated;
    }

    public String getPlateVehicle() {
        return plateVehicle;
    }

    public void setPlateVehicle(String plateVehicle) {
        this.plateVehicle = plateVehicle;
    }

    @Override
    public String toString() {
        return "PojoDataNotifications{" +
                "date='" + date + '\'' +
                ", title='" + title + '\'' +
                ", dateExpirated='" + dateExpirated + '\'' +
                ", plateVehicle='" + plateVehicle + '\'' +
                '}';
    }
}

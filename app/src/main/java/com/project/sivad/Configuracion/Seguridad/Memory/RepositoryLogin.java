package com.project.sivad.Configuracion.Seguridad.Memory;

import com.project.sivad.Configuracion.Seguridad.Pojo.User;

public interface RepositoryLogin {
    void saveUser(User user);
    User getUser();
}

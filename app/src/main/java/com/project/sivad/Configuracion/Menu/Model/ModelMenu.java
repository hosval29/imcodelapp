package com.project.sivad.Configuracion.Menu.Model;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.http.ApiAdapter;
import com.project.sivad.http.ApiAdapterClient;
import com.project.sivad.http.Response.ResponseDataNotificationByVehicle;
import com.project.sivad.http.Response.ResponseDataNoveltiesInspections;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseSuccessUpdateMeleage;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModelMenu implements InterfacesMenu.InterfacesModelMenu {

    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;
    private ApiAdapterClient apiAdapterClient;
    private Context context;
    JSONObject jsonBody = null;

    public ModelMenu(InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu, Context context) {
        this.interfacesPresenterMenu = interfacesPresenterMenu;
        this.context = context;
        this.apiAdapterClient = new ApiAdapterClient(context);
        this.jsonBody = new JSONObject();
    }

    @Override
    public void getDataPlacaKilometraje(String plate, int mileage) {
        Call<ResponseValidatePlateMileage> call = apiAdapterClient.getDataPlateMileage(plate, mileage);
        call.enqueue(new Callback<ResponseValidatePlateMileage>() {
            @Override
            public void onResponse(Call<ResponseValidatePlateMileage> call, Response<ResponseValidatePlateMileage> response) {
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {
                    interfacesPresenterMenu.responseSuccessGetDataPlacaKilometraje(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterMenu.responseSuccessGetDataPlacaKilometraje(response.body());
                } else {

                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorGetDataPlacaKilometraje(responseError1);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataPlacaKilometraje(responseError);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataPlacaKilometraje(responseError);
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseValidatePlateMileage> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage("Mala coneexion con el servidor, valida la conexion a internet y vuelve a intentar.");

                interfacesPresenterMenu.responseErrorGetDataPlacaKilometraje(responseError);
            }
        });
    }

    @Override
    public void putDataProfile(int idUser
            , String name
            , String lastName
            , String email
            , int phone
            , String passwordOld
            , String passwordNew
            , int idUserSession
            , int idHistoryLog) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("lastName", lastName);
            jsonObject.put("email", email);
            jsonObject.put("phone", phone);
            jsonObject.put("passwordOld", passwordOld);
            jsonObject.put("passwordNew", passwordNew);
            jsonObject.put("idUserSession", idUserSession);
            jsonObject.put("idHistoryLog", idHistoryLog);

            //Declaramos un Parseo para quitar simbolos y espacios innecesarios
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());

            // creamos un Objeto Call que sea del Modelo EntradasResponse y le asignamos el metodo getEntrada de
            // apiAdapter
            Call<ResponseLoginUser> call = apiAdapterClient.putDataProfile(idUser, gsonObject);
            call.enqueue(new ResponseCallback());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ResponseCallback implements Callback<ResponseLoginUser> {
        @Override
        public void onResponse(Call<ResponseLoginUser> call, Response<ResponseLoginUser> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            if (response.code() == 200) {
                interfacesPresenterMenu.responseSuccesPutDataProfile(response.body());
            } else if (response.code() == 201) {
                interfacesPresenterMenu.responseSuccesPutDataProfile(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterMenu.responseErrorPutDataProfile(responseError1);
                    return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPutDataProfile(responseError);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPutDataProfile(responseError);
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseLoginUser> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Mala coneexion con el servidor, valida la conexion a internet y vuelve a intentar.");
            interfacesPresenterMenu.responseErrorPutDataProfile(responseError);
        }
    }

    @Override
    public void putDataSignOut(int idUser, int idUserSession, int idHistoryLog) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("idUserSession", idUserSession);
            jsonObject.put("idHistoryLog", idHistoryLog);

            //Declaramos un Parseo para quitar simbolos y espacios innecesarios
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());

            // creamos un Objeto Call que sea del Modelo EntradasResponse y le asignamos el metodo getEntrada de
            // apiAdapter
            Call<ResponseJson> call = apiAdapterClient.putDataSignOut(idUser, gsonObject);
            call.enqueue(new ResponseCallbackPutDataSignOut());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class ResponseCallbackPutDataSignOut implements Callback<ResponseJson> {
        @Override
        public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;

            if (response.code() == 200) {
                interfacesPresenterMenu.responseSuccessPutSignOut(response.body());
            } else if (response.code() == 201) {
                interfacesPresenterMenu.responseSuccessPutSignOut(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterMenu.responseErrorPutDataProfile(responseError1);
                    return;*/

                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPutSignOut(responseError);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPutSignOut(responseError);
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseJson> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Mala coneexion con el servidor, valida la conexion a internet y vuelve a intentar.");
            interfacesPresenterMenu.responseErrorPutSignOut(responseError);
        }
    }

    @Override
    public void getDataNotificationsByVehicle(int idVehicle) {
        Call<ResponseDataNotificationByVehicle> call = apiAdapterClient.getDataNotificationsByVehicle(idVehicle);
        call.enqueue(new Callback<ResponseDataNotificationByVehicle>() {
            @Override
            public void onResponse(Call<ResponseDataNotificationByVehicle> call, Response<ResponseDataNotificationByVehicle> response) {
                Log.v("response", response.toString());
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {

                    interfacesPresenterMenu.responseSuccessGetDataNotificationByVehicle(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterMenu.responseSuccessGetDataNotificationByVehicle(response.body());
                } else {

                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorGetDataNotificationByVehicle(responseError1);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataNotificationByVehicle(responseError);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataNotificationByVehicle(responseError);
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseDataNotificationByVehicle> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage("Mala conexion con el servidor, valida la conexion a internet y vuelve a intentar.");
                interfacesPresenterMenu.responseErrorGetDataNotificationByVehicle(responseError);
            }
        });
    }

    @Override
    public void getDataNoveltiesInspections(int idVehicle) {
        Call<ResponseDataNoveltiesInspections> call = apiAdapterClient.getDataNoveltiesInspections(idVehicle);
        call.enqueue(new Callback<ResponseDataNoveltiesInspections>() {
            @Override
            public void onResponse(Call<ResponseDataNoveltiesInspections> call, Response<ResponseDataNoveltiesInspections> response) {
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {

                    interfacesPresenterMenu.responseSuccessGetDataNoveltiesInspection(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterMenu.responseSuccessGetDataNoveltiesInspection(response.body());
                } else {

                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorGetDataNoveltiesInspection(responseError1);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataNoveltiesInspection(responseError);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataNoveltiesInspection(responseError);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDataNoveltiesInspections> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage("Mala conexion con el servidor, valida la conexion a internet y vuelve a intentar.");
                interfacesPresenterMenu.responseErrorGetDataNoveltiesInspection(responseError);
            }
        });
    }


    @Override
    public void getDataTypesVehicularInspection() {
        Call<ResponseDataTypesVehicularInspectios> call = apiAdapterClient.getDataTypesVehicularInspection();
        call.enqueue(new Callback<ResponseDataTypesVehicularInspectios>() {
            @Override
            public void onResponse(Call<ResponseDataTypesVehicularInspectios> call, Response<ResponseDataTypesVehicularInspectios> response) {
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {
                    interfacesPresenterMenu.responseSuccesGetDataTypesVehicularInspections(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterMenu.responseSuccesGetDataTypesVehicularInspections(response.body());
                } else {
                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorGetDataTypesVehicularInspections(responseError1);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataTypesVehicularInspections(responseError);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetDataTypesVehicularInspections(responseError);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDataTypesVehicularInspectios> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage("Mala conexion con el servidor, valida la conexion a internet y vuelve a intentar.");
                interfacesPresenterMenu.responseErrorGetDataTypesVehicularInspections(responseError);
            }
        });
    }

    @Override
    public void getValidateInspectionEnabled(String plateVehicle) {
        Call<ResponseValideteInspectionEnabled> call = apiAdapterClient.getValidateInspectionEnabled(plateVehicle);
        call.enqueue(new Callback<ResponseValideteInspectionEnabled>() {
            @Override
            public void onResponse(Call<ResponseValideteInspectionEnabled> call, Response<ResponseValideteInspectionEnabled> response) {
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {
                    interfacesPresenterMenu.responseSuccessGetValidateInspectionEnabled(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterMenu.responseSuccessGetValidateInspectionEnabled(response.body());
                } else {
                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorGetDataTypesVehicularInspections(responseError1);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetValidateInspectionEnabled(responseError);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorGetValidateInspectionEnabled(responseError);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseValideteInspectionEnabled> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage("Mala conexion con el servidor, valida la conexion a internet y vuelve a intentar.");
                interfacesPresenterMenu.responseErrorGetValidateInspectionEnabled(responseError);
            }
        });
    }

    @Override
    public void postDataOffline(int idRegister, String jsonObject, String nameModule) {

        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject);

        switch (nameModule) {
            case ConstantsStrings
                    .MODULE_INSPECCION_PREOPERACIONAL_CARGA_FURGONES:
                Call<ResponseVehicleInspecion> call = apiAdapterClient.postDataVehicleInspection(gsonObject);
                call.enqueue(new ResponseCallbackPostDataOffline(idRegister));
                break;
            case ConstantsStrings
                    .MODULE_INSPECCION_PREOPERACIONAL_NOTIFICATION_ALERT:
                Call<ResponseJson> call1 = apiAdapterClient.postDataAlertByItemBadToggle(gsonObject);
                call1.enqueue(new ResponseCallbackPostDataOfflineAlertByitemBadToggle(idRegister));
                break;
            case ConstantsStrings
                    .MODULE_INSPECCION_PREOPERACIONAL_NOVEDAD_INSPECTION:
                Call<ResponseJson> call2 = apiAdapterClient.postDataNoveltiesInspection(gsonObject);
                call2.enqueue(new ResponsePostDataOfflineNoveltiesInspection(idRegister));
                break;
            case ConstantsStrings
                    .MODULE_UPDATE_MILEAGE:
                DBSivad dbSivad = new DBSivad(context);
                ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
                Call<ResponseValidatePlateMileage> call3 = apiAdapterClient.updateMeleage(responseValidatePlateMileage.getData().getIdVehicle(), gsonObject);
                call3.enqueue(new ResponseUpdateMileageOffline(idRegister));
                break;
        }
    }

    private class ResponseCallbackPostDataOffline implements Callback<ResponseVehicleInspecion> {
        DBSivad dbSivad;
        int idRegister = 0;

        public ResponseCallbackPostDataOffline(int id) {
            dbSivad = new DBSivad(context);
            this.idRegister = id;
        }

        @Override
        public void onResponse(Call<ResponseVehicleInspecion> call, Response<ResponseVehicleInspecion> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            Log.v("idRegister", String.valueOf(idRegister));

            if (response.code() == 200) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineVehicleInspection(response.body());
            } else if (response.code() == 201) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineVehicleInspection(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError1);
                    return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseVehicleInspecion> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Sin conexión al servidor o a internet.");
            interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
        }
    }


    private class ResponsePostDataOfflineNoveltiesInspection implements Callback<ResponseJson> {

        DBSivad dbSivad;
        int idRegister = 0;

        public ResponsePostDataOfflineNoveltiesInspection(int idRegister) {
            dbSivad = new DBSivad(context);
            this.idRegister = idRegister;
        }

        @Override
        public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            Log.v("idRegister", String.valueOf(idRegister));
            if (response.code() == 200) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineNoveltiesInspection(response.body());
            } else if (response.code() == 201) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineNoveltiesInspection(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError1);
                    return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseJson> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Sin conexión al servidor o a internet.");
            interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
        }
    }

    private class ResponseUpdateMileageOffline implements Callback<ResponseValidatePlateMileage> {

        DBSivad dbSivad;
        int idRegister = 0;

        public ResponseUpdateMileageOffline(int idRegister) {
            dbSivad = new DBSivad(context);
            this.idRegister = idRegister;
        }

        @Override
        public void onResponse(Call<ResponseValidatePlateMileage> call, Response<ResponseValidatePlateMileage> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            Log.v("idRegister", String.valueOf(idRegister));
            if (response.code() == 200) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineUMileageUpdate(response.body());
            } else if (response.code() == 201) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineUMileageUpdate(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorPutDataMeleage(responseError1);
                        return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseValidatePlateMileage> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Sin conexión al servidor o a internet.");
            interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
        }
    }


    private class ResponseCallbackPostDataOfflineAlertByitemBadToggle implements Callback<ResponseJson> {

        DBSivad dbSivad;
        int idRegister = 0;

        public ResponseCallbackPostDataOfflineAlertByitemBadToggle(int idRegister) {
            dbSivad = new DBSivad(context);
            this.idRegister = idRegister;
        }

        @Override
        public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            Log.v("idRegister", String.valueOf(idRegister));
            if (response.code() == 200) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineNoveltiesInspection(response.body());
            } else if (response.code() == 201) {
                int rowDelete = dbSivad.deleteDataOfflineById(idRegister);
                Log.v("rowDelete", String.valueOf(rowDelete));
                interfacesPresenterMenu.responseSuccessPostDataOfflineNoveltiesInspection(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterInspectionPreoperational.responseErrorPostDataAlertByItemBadToggle(responseError1, jsonBody);
                    return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
                }

            }
        }

        @Override
        public void onFailure(Call<ResponseJson> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Sin conexión al servidor o a internet.");
            interfacesPresenterMenu.responseErrorPostDataOffline(responseError);
        }
    }


    @Override
    public void putDataMeleage(String dateRegister, int idUser, int idVehicle, int mileage) {


        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = null;

        try {
            jsonBody.put("idUser", idUser);
            jsonBody.put("dateRegister", dateRegister);
            jsonBody.put("mileage", mileage);

            //Declaramos un Parseo para quitar simbolos y espacios innecesarios
            gsonObject = (JsonObject) jsonParser.parse(jsonBody.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<ResponseValidatePlateMileage> call = apiAdapterClient.updateMeleage(idVehicle, gsonObject);
        call.enqueue(new Callback<ResponseValidatePlateMileage>() {
            @Override
            public void onResponse(Call<ResponseValidatePlateMileage> call, Response<ResponseValidatePlateMileage> response) {
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {
                    interfacesPresenterMenu.responseSuccessPutDataMeleade(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterMenu.responseSuccessPutDataMeleade(response.body());
                } else {

                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterMenu.responseErrorPutDataMeleage(responseError1);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorPutDataMeleage(responseError, jsonBody);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterMenu.responseErrorPutDataMeleage(responseError, jsonBody);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseValidatePlateMileage> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage(ConstantsStrings.NO_CONEXCION_INTERNET);
                interfacesPresenterMenu.responseErrorPutDataMeleage(responseError, jsonBody);
            }
        });
    }

}

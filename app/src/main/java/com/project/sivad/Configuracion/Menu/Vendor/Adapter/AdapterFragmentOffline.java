package com.project.sivad.Configuracion.Menu.Vendor.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataOffline;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFragmentOffline extends RecyclerView.Adapter<AdapterFragmentOffline.ViewHolder> {
    List<PojoDataOffline> pojoDataOfflineList = new ArrayList<>();

    InterfacesMenu.InterfacesSendDataOfflineById interfacesSendDataOfflineById;

    public AdapterFragmentOffline(List<PojoDataOffline> pojoDataOfflineList, InterfacesMenu.InterfacesSendDataOfflineById interfacesSendDataOfflineById) {
        this.pojoDataOfflineList = pojoDataOfflineList;
        this.interfacesSendDataOfflineById = interfacesSendDataOfflineById;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_offline, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int id = 0;
        String dateRegister = "Vacio";
        String module = "Vacio";

        if (pojoDataOfflineList.get(position).getIdRegister() != 0) {
            id = pojoDataOfflineList.get(position).getIdRegister();
        }

        if (pojoDataOfflineList.get(position).getDateRegister() != null) {
            dateRegister = pojoDataOfflineList.get(position).getDateRegister();
        }

        if (pojoDataOfflineList.get(position).getTitleModule() != null) {
            module = pojoDataOfflineList.get(position).getTitleModule();
        }

        holder.idAppCompatTextviewIdOffline.setText(String.valueOf(id));
        holder.idAppCompatTextviewDateRegister.setText(dateRegister);
        holder.idAppCompatTextviewModule.setText(module);

        if (module.equals(ConstantsStrings.MODULE_UPDATE_MILEAGE)) {
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(pojoDataOfflineList.get(position).getData());

            holder.idLinearLayoutValue.setVisibility(View.VISIBLE);
            holder.idAppCompatTextviewValue.setText(gsonObject.get("mileage").getAsString());
        } else {
            holder.idLinearLayoutValue.setVisibility(View.GONE);
            holder.idAppCompatTextviewValue.setText("");
        }

        holder.idMaterialButtonSincronizarData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pojoDataOfflineList.get(position).getTitleModule().equals(ConstantsStrings.MODULE_UPDATE_MILEAGE)) {
                    JsonParser jsonParser = new JsonParser();
                    JsonObject gsonObject = (JsonObject) jsonParser.parse(pojoDataOfflineList.get(position).getData());
                    int mileage = gsonObject.get("mileage").getAsInt();

                    boolean isMileage = false;

                    for (int i = 0; i < pojoDataOfflineList.size(); i++) {
                        JsonParser jsonParser2 = new JsonParser();
                        JsonObject gsonObject2 = (JsonObject) jsonParser2.parse(pojoDataOfflineList.get(i).getData());

                        if (pojoDataOfflineList.get(i).getTitleModule().equals(ConstantsStrings.MODULE_UPDATE_MILEAGE)) {
                            int mileageList = gsonObject2.get("mileage").getAsInt();
                            Log.v("mileageList", String.valueOf(mileageList));
                            if (mileage > mileageList) {
                                isMileage = true;
                                //return;
                            }
                        }

                    }

                    Log.v("isMileage", String.valueOf(isMileage));

                    if (!isMileage) {
                        interfacesSendDataOfflineById.sendDataOfflineById(position);
                    } else {
                        //snackBarPersonalized.showSnackbarWithAction(4, message);
                        AlertDialog.Builder builder = new AlertDialog.Builder(holder.context);
                        builder.setMessage("Este item es mayor a un kilometraje de la lista.\n" +
                                "Por favor revise cada item para validar que kilometraje es menor y poder enviarlo!").setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.setTitle("Kilometraje Mayor");
                        dialog.show();
                    }


                } else {
                    interfacesSendDataOfflineById.sendDataOfflineById(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return pojoDataOfflineList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.idAppCompatImageViewIconNovelties)
        AppCompatImageView idAppCompatImageViewIconNovelties;
        @BindView(R.id.idAppCompatTextviewIdOffline)
        AppCompatTextView idAppCompatTextviewIdOffline;
        @BindView(R.id.idAppCompatTextviewDateRegister)
        AppCompatTextView idAppCompatTextviewDateRegister;
        @BindView(R.id.idAppCompatTextviewModule)
        AppCompatTextView idAppCompatTextviewModule;
        @BindView(R.id.idLinearLayoutCompatExpandle)
        LinearLayoutCompat idLinearLayoutCompatExpandle;
        @BindView(R.id.idMaterialButtonSincronizarData)
        MaterialButton idMaterialButtonSincronizarData;
        @BindView(R.id.idAppCompatTextviewValue)
        AppCompatTextView idAppCompatTextviewValue;
        @BindView(R.id.idLinearLayoutValue)
        LinearLayoutCompat idLinearLayoutValue;
        Context context;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

}

package com.project.sivad.Configuracion.Seguridad.Pojo;

public class ResponseLogin {
    String nameUser;
    String user;
    int iduser;
    int idCliente;
    int idPerfil;

    public ResponseLogin(String nameUser, String user, int iduser, int idCliente, int idPerfil) {
        this.nameUser = nameUser;
        this.user = user;
        this.iduser = iduser;
        this.idCliente = idCliente;
        this.idPerfil = idPerfil;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }
}

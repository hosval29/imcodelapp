package com.project.sivad.Configuracion.Menu.Vendor.Fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.project.sivad.Configuracion.Menu.Interfaces.InterfacesMenu;
import com.project.sivad.Configuracion.Menu.Presenter.PresenterMenu;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataOffline;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.Components.FormatDates;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentUpdateMeleage extends DialogFragment implements InterfacesMenu.InterfaceFragmenUpdateMeleage {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.idAppCompatTextviewLastMeleage)
    AppCompatTextView idAppCompatTextviewLastMeleage;
    @BindView(R.id.idAppCompatTextviewDateLastUpdate)
    AppCompatTextView idAppCompatTextviewDateLastUpdate;
    @BindView(R.id.idTextInputLayoutMeleage)
    TextInputLayout idTextInputLayoutMeleage;
    @BindView(R.id.linearLayoutCompat2)
    LinearLayoutCompat linearLayoutCompat2;
    @BindView(R.id.idAppCompatTextviewError)
    AppCompatTextView idAppCompatTextviewError;
    @BindView(R.id.idLinearLayoutError)
    LinearLayoutCompat idLinearLayoutError;
    @BindView(R.id.idBtnCancelar)
    MaterialButton idBtnCancelar;
    @BindView(R.id.idMaterialButtonAceptar)
    MaterialButton idMaterialButtonAceptar;
    @BindView(R.id.idLinearLayoutButtonsAceptarAndCancelar)
    LinearLayoutCompat idLinearLayoutButtonsAceptarAndCancelar;
    @BindView(R.id.idMaterialButtonOk)
    MaterialButton idMaterialButtonOk;
    @BindView(R.id.idLinearLayoutButtonOk)
    LinearLayoutCompat idLinearLayoutButtonOk;

    //Declaramos las variables String y int
    private int networkState;
    private String dataOffline = "";
    private int codeStatus = 0;
    private String message = "";
    private int mileageCurrent = 0;

    //Declaramos las variables boolean
    public static boolean isConnect = false;

    //Utils Objects
    private Context context;
    private ProgressDialog progressDialog;
    private FormatDates formatDates;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Declaramos la variable list
    private List<PojoDataOffline> pojoDataOfflineList = new ArrayList<>();

    //Instanciamos las interface
    InterfaceFragmentUpdateMeleage interfecesFragmmentUpdateMeleage;
    private InterfacesMenu.InterfacesPresenterMenu interfacesPresenterMenu;

    public interface InterfaceFragmentUpdateMeleage {
        void responsePutDataMeleage(int codeStatus, String message);
    }

    public FragmentUpdateMeleage() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentUpdateMeleage newInstance(String param1, String param2) {
        FragmentUpdateMeleage fragment = new FragmentUpdateMeleage();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        if (context instanceof InterfaceFragmentUpdateMeleage) {
            interfecesFragmmentUpdateMeleage = (InterfaceFragmentUpdateMeleage) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        if (getArguments() != null) {
        }

        context = getContext();
        formatDates = new FormatDates();
        progressDialog = new ProgressDialog(context);
        dbSivad = new DBSivad(context);
        interfacesPresenterMenu = new PresenterMenu(null
                , null
                , null
                , null
                , null
                , null
                , null
                , this
                , context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_meleage, container, false);
        ButterKnife.bind(this, view);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view1 -> {
            validateRegisterNoveltiesInspectionToggleBad();
        });

        toolbar.setTitle("Actualizar Kilometraje");

        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        if (responseValidatePlateMileage != null) {
            String dateLastUpdate = "";
            String mileage = "";
            if (responseValidatePlateMileage.getData().getLastDateUpdate() != null) {
                dateLastUpdate = responseValidatePlateMileage.getData().getLastDateUpdate();
            }

            if (responseValidatePlateMileage.getData().getMileageCurrent() != 0) {
                mileage = String.valueOf(responseValidatePlateMileage.getData().getMileageCurrent());
            }

            idAppCompatTextviewDateLastUpdate.setText(dateLastUpdate);
            idAppCompatTextviewLastMeleage.setText(mileage);
        }

        idTextInputLayoutMeleage.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (idLinearLayoutError.getVisibility() == View.VISIBLE) {
                    idAppCompatTextviewError.setText("");
                    idLinearLayoutError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        idMaterialButtonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateInputs()) {

                    if (validateDataOfflineMileageUpdate()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("Existen registros de actualización de kilometrajes almacenados offline.\n" +
                                "Diríjase al módulo offline y sincronice primero los kilometrajes registrados.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        dismiss();
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.setTitle("Bloqueo Actualizión Kilometraje");
                        dialog.show();

                    }else{
                        int mileage = Integer.parseInt(idTextInputLayoutMeleage.getEditText().getText().toString().trim());
                        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
                        int idVehicle = responseValidatePlateMileage.getData().getIdVehicle();
                        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                        int idUser = responseLoginUser.getData().getIdUser();
                        String dateRegister = formatDates.getDateRegisterFormatDateWithTime();

                        if (mileage > responseValidatePlateMileage.getData().getMileageCurrent()) {
                            progressDialog.setMessage(ConstantsStrings.LOAD);
                            progressDialog.show();
                            idLinearLayoutError.setVisibility(View.GONE);
                            idAppCompatTextviewError.setText("");

                            interfacesPresenterMenu.putDataMeleage(dateRegister
                                    , idUser
                                    , idVehicle
                                    , mileage);

                        } else {
                            idLinearLayoutError.setVisibility(View.VISIBLE);
                            idAppCompatTextviewError.setText(ConstantsStrings.ERROR_MILEAGE);
                        }
                    }
                }
            }
        });

        idBtnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        idMaterialButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String registrationDate = formatDates.getDateRegisterFormatDateWithTime();
                ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                int idUser = responseLoginUser.getData().getIdUser();

                ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
                responseValidatePlateMileage.getData().setLastDateUpdate(formatDates.getDateRegisterFormatDateWithTime());
                responseValidatePlateMileage.getData().setMileageCurrent(mileageCurrent);
                Gson gson = new Gson();
                String json = gson.toJson(responseValidatePlateMileage);
                int idDelete = dbSivad.deleteDataVehicle();
                Long idAdd = dbSivad.addDataVehicle(registrationDate, json);

                dbSivad.addDataOffline(registrationDate, idUser, ConstantsStrings.MODULE_UPDATE_MILEAGE, dataOffline);
                interfecesFragmmentUpdateMeleage.responsePutDataMeleage(200, "Almacenamiento offline con éxito.");
                dismiss();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDataOffline();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }
        }
    };

    private void getDataOffline() {
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        pojoDataOfflineList.clear();
        pojoDataOfflineList.addAll(dbSivad.getDataOffline(responseLoginUser.getData().getIdUser()));
    }

    private boolean validateDataOfflineMileageUpdate(){
        boolean error = false;
        if (isConnect) {
            if (pojoDataOfflineList != null){
                Log.v("pojoDataOfflineList", pojoDataOfflineList.toString());
            }

            if (pojoDataOfflineList.size() > 0) {
                boolean isDataOfflineMileageUpdate = false;
                for (int i = 0; i < pojoDataOfflineList.size(); i++) {
                    if (pojoDataOfflineList.get(i).getTitleModule().equals(ConstantsStrings.MODULE_UPDATE_MILEAGE)) {
                        isDataOfflineMileageUpdate = true;
                    }else{
                        isDataOfflineMileageUpdate = false;
                    }
                }

                Log.v("isDataOfflineMileageUp", String.valueOf(isDataOfflineMileageUpdate));

                if (isDataOfflineMileageUpdate){
                    error = true;
                }else {
                    error = false;
                }

            }else {
                error = false;
            }
        }else {
            error = false;
        }

        return error;
    }

    private boolean validateInputs() {
        boolean error = false;

        if (idTextInputLayoutMeleage.getEditText().getText().toString().matches("")) {
            idTextInputLayoutMeleage.setError("Campo Kilometraje obligatorio");
            error = true;
        }

        return error;
    }


    private void validateRegisterNoveltiesInspectionToggleBad() {

        String message = "¿Está seguro que no desea ir atras?";

        //snackBarPersonalized.showSnackbarWithAction(4, message);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Cancelar Actualización Kilometraje");
        dialog.show();
    }

    @Override
    public void responseErrorPutDataMeleage(ResponseError responseError, JSONObject jsonObject) {
        progressDialog.dismiss();
        dataOffline = jsonObject.toString();
        codeStatus = responseError.getErrorcode();
        message = responseError.getMessage();
        try {
            mileageCurrent = jsonObject.getInt("mileage");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        idAppCompatTextviewError.setText(responseError.getMessage() + "\n"
                + "O presione (Ok) para almacenar offline.");
        idLinearLayoutError.setVisibility(View.VISIBLE);
        idLinearLayoutButtonOk.setVisibility(View.VISIBLE);
        idLinearLayoutButtonsAceptarAndCancelar.setVisibility(View.GONE);
    }

    @Override
    public void responseSuccessPutDataMeleade(ResponseValidatePlateMileage responseValidatePlateMileage) {
        progressDialog.dismiss();
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreated = ft.format(date).toString();
        Gson gson = new Gson();
        String json = gson.toJson(responseValidatePlateMileage);
        int idDelete = dbSivad.deleteDataVehicle();
        Long idAdd = dbSivad.addDataVehicle(dateCreated, json);
        interfecesFragmmentUpdateMeleage.responsePutDataMeleage(responseValidatePlateMileage.getCodeStatus(), responseValidatePlateMileage.getMessage());
        dismiss();
    }

}

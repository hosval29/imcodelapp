package com.project.sivad.Vendor.Splahs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.project.sivad.Configuracion.Seguridad.View.ViewLogin;
import com.project.sivad.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewSplash extends AppCompatActivity {

    @BindView(R.id.idAppCompatImageViewLogoSivad)
    AppCompatImageView idAppCompatImageViewLogoSivad;
    @BindView(R.id.idAppCompatTextViewSivadSignificado)
    AppCompatTextView idAppCompatTextViewSivadSignificado;
    @BindView(R.id.idAppCompatTextViewSivadDisenoDesarrollo)
    AppCompatTextView idAppCompatTextViewSivadDisenoDesarrollo;
    @BindView(R.id.idAppCompatTextViewSivadLogoIncuba)
    AppCompatImageView idAppCompatTextViewSivadLogoIncuba;
    @BindView(R.id.idConstraintLayoutSplah)
    ConstraintLayout idConstraintLayoutSplah;


    Animation animationScale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_splash);
        ButterKnife.bind(this);

        animationScale = AnimationUtils.loadAnimation(this, R.anim.anim_splash);

        idConstraintLayoutSplah.startAnimation(animationScale);

        idAppCompatImageViewLogoSivad.setTranslationX(400);
        idAppCompatTextViewSivadSignificado.setTranslationX(400);
        idAppCompatTextViewSivadDisenoDesarrollo.setTranslationX(400);
        idAppCompatTextViewSivadLogoIncuba.setTranslationX(400);

        idAppCompatImageViewLogoSivad.setAlpha((float) 0.0);
        idAppCompatTextViewSivadSignificado.setAlpha(0);
        idAppCompatTextViewSivadDisenoDesarrollo.setAlpha(0);
        idAppCompatTextViewSivadLogoIncuba.setAlpha((float) 0.0);

        idAppCompatImageViewLogoSivad.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(400).start();
        idAppCompatTextViewSivadSignificado.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(600).start();
        idAppCompatTextViewSivadDisenoDesarrollo.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(1000).start();
        idAppCompatTextViewSivadLogoIncuba.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(1200).start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ViewSplash.this, ViewLogin.class);
                startActivity(intent);
                finish();
            }
        }, 4000);

    }

}

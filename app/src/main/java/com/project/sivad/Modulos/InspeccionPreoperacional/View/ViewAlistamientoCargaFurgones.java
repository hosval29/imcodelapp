package com.project.sivad.Modulos.InspeccionPreoperacional.View;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.pd.chocobar.ChocoBar;
import com.project.sivad.Configuracion.Menu.View.ViewMenu;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces.InterfacesInspeccionPreoperation;
import com.project.sivad.Modulos.InspeccionPreoperacional.Pojo.PojoNoveltieInspection;
import com.project.sivad.Modulos.InspeccionPreoperacional.Presenter.PresenterInspectionPreoperational;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters.PagesAdapterFragmentsAlistamientoPasajeroFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsAlistamientoCargaFurgones.FragmentCargaFurgonesAspectosElectricos;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsAlistamientoCargaFurgones.FragmentCargaFurgonesAspectosGenereales;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsAlistamientoCargaFurgones.FragmentCargaFurgonesAspectosMecanicos;
import com.project.sivad.R;
import com.project.sivad.Util.Components.FormatDates;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ValidateConncet;
import com.project.sivad.Util.ViewPagerTB;
import com.project.sivad.http.Response.DataVehicleResponse;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewAlistamientoCargaFurgones extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        FragmentCargaFurgonesAspectosMecanicos.InterfacesFragmentCFAM,
        FragmentCargaFurgonesAspectosElectricos.InterfacesFragmentCFAE,
        FragmentCargaFurgonesAspectosGenereales.InterfacesFragmentCFAG,
        SnackBarPersonalized.InterfacesResponseSnackbarWithAction,
        InterfacesInspeccionPreoperation.InterfacesViewInspeccionPreoperational,
        ViewNovedades.InterfaceViewNovelties {

    //Variables XML
    @BindView(R.id.toolbarViewInspeccionPreoperacional)
    Toolbar toolbarViewInspeccionPreoperacional;
    @BindView(R.id.appBarLayouViewInspeccionPreoperacional)
    AppBarLayout appBarLayouViewInspeccionPreoperacional;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.bottomNavigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.idViewPager)
    ViewPagerTB idViewPager;
    @BindView(R.id.idCoordinatorLayoutViewAlistamientoCargaFurgones)
    CoordinatorLayout idCoordinatorLayoutViewAlistamientoCargaFurgones;

    //Instanciamos el PagesAdapter
    private PagesAdapterFragmentsAlistamientoPasajeroFurgones pagesAdapterFrgament;

    //Intanciamos los Fragments
    private FragmentCargaFurgonesAspectosElectricos fragmentCargaFurgonesAspectosElectricos;
    private FragmentCargaFurgonesAspectosGenereales fragmentCargaFurgonesAspectosGenereales;
    private FragmentCargaFurgonesAspectosMecanicos fragmentCargaFurgonesAspectosMecanicos;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Utilidades
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;
    private FormatDates formatDates;

    //Variables String y Int
    private int networkState;
    private String dataOffline = "";
    private String nameModule = "";
    private int idInspectionGeneral = 0;

    //Variables de validacion
    private boolean isConnect = false;

    //Instanciamos la Interfaces
    private InterfacesInspeccionPreoperation.InterfacesPresenterInspectionPreoperational interfacesPresenterInspectionPreoperational;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_alistamiento_carga_furgones);
        ButterKnife.bind(this);

        setupToolbar(toolbarViewInspeccionPreoperacional);

        //Inicializamos las variables tipo Object
        context = this;
        dbSivad = new DBSivad(context);
        progressDialog = new ProgressDialog(context);
        interfacesPresenterInspectionPreoperational = new PresenterInspectionPreoperational(this, null, null, context);

        fragmentCargaFurgonesAspectosMecanicos = new FragmentCargaFurgonesAspectosMecanicos();
        fragmentCargaFurgonesAspectosElectricos = new FragmentCargaFurgonesAspectosElectricos();
        fragmentCargaFurgonesAspectosGenereales = new FragmentCargaFurgonesAspectosGenereales();

        idViewPager.setOffscreenPageLimit(3);
        idViewPager.setCurrentItem(0);
        idViewPager.addOnPageChangeListener(new PageFragmentChange());
        idViewPager.setCanScroll(false);
        setupViewPager(getSupportFragmentManager(), idViewPager);

        //((App) getApplication()).getComponents().injectViewInspeccion(this);

        bottomNavigation.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFrag = new ViewNovedades();
                dialogFrag.setCancelable(false);
                dialogFrag.show(ft, "dialog");
            }
        });

        //modoInicial();
    }

    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("estoy aqui", "onResume");

        setupToolbar(toolbarViewInspeccionPreoperacional);

        //Inicializamos las variables tipo Object
        context = this;
        dbSivad = new DBSivad(context);
        formatDates = new FormatDates();
        interfacesPresenterInspectionPreoperational = new PresenterInspectionPreoperational(this, null, null, context);

        fragmentCargaFurgonesAspectosMecanicos = new FragmentCargaFurgonesAspectosMecanicos();
        fragmentCargaFurgonesAspectosElectricos = new FragmentCargaFurgonesAspectosElectricos();
        fragmentCargaFurgonesAspectosGenereales = new FragmentCargaFurgonesAspectosGenereales();

        idViewPager.setOffscreenPageLimit(3);
        idViewPager.setCurrentItem(0);
        idViewPager.addOnPageChangeListener(new PageFragmentChange());
        idViewPager.setCanScroll(false);
        setupViewPager(getSupportFragmentManager(), idViewPager);

        snackBarPersonalized = new SnackBarPersonalized(ViewAlistamientoCargaFurgones.this);
        registerReceiver(broadcastReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        modoInicial();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("estoy aqui", "onPause");
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Log.v("estoy aqui", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Log.v("estoy aqui", "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //dbSivad.deleteDataInspection();
        dbSivad.deleteDataNoveltiesInspection();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            networkState = ValidateConncet.getConnectionStatus(context);
            String message = "";
            switch (networkState) {
                case ValidateConncet.WIFI:
                    message = "Wifi activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.MOBILE:
                    message = "Datos Moviles activado, en linea...";
                    isConnect = true;
                    break;
                case ValidateConncet.NOT_CONNECTED:
                    message = "En estos momentos no cuentas con conexión a intenert.";
                    isConnect = false;
                    break;
            }

            //snackBarPersonalized.showSnackBarIsConncect(isConnect, message);
        }
    };

    private void setupViewPager(FragmentManager fragmentManager, ViewPager viewPagerSetup) {
        pagesAdapterFrgament = new PagesAdapterFragmentsAlistamientoPasajeroFurgones(fragmentManager);

        //Add All Fragment To List
        pagesAdapterFrgament.addFragment(fragmentCargaFurgonesAspectosMecanicos, getResources().getString(R.string.tab_label_aspectos_mecanicos));
        pagesAdapterFrgament.addFragment(fragmentCargaFurgonesAspectosElectricos, getResources().getString(R.string.tab_label_aspectos_electricos));
        pagesAdapterFrgament.addFragment(fragmentCargaFurgonesAspectosGenereales, getResources().getString(R.string.tab_label_aspectos_generales));

        viewPagerSetup.setAdapter(pagesAdapterFrgament);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.itemBottomNavigationAspectosMecanico:
                idViewPager.setCurrentItem(0);
                return true;
            case R.id.itemBottomNavigationAspectosElectricos:
                idViewPager.setCurrentItem(1);
                return true;
            case R.id.itemBottomNavigationAspectosGenerales:
                idViewPager.setCurrentItem(2);

                return true;
        }
        return false;
    }

    @Override
    public void responsePostDataNoveltiesInspecion(int code, String message) {

    }

    public class PageFragmentChange implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosMecanico);
                    break;
                case 1:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosElectricos);
                    bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosElectricos).setEnabled(true);
                    break;
                case 2:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosGenerales);
                    bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosGenerales).setEnabled(true);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    private void modoInicial() {
        bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosElectricos).setEnabled(false);
        bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosGenerales).setEnabled(false);
    }

    public void btnContinue(View view) {
        fragmentCargaFurgonesAspectosMecanicos.btnContinue(view);
    }

    public void btnContinuarElectrico(View view) {
        fragmentCargaFurgonesAspectosElectricos.btnContinuarElectrico(view);
    }

    public void btnFinalizeInspection(View view) {
        fragmentCargaFurgonesAspectosGenereales.btnFinalizeInspection(view);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void sendDataAspectosMecanicos(JSONArray jsonArrayDataAspectosMecanicos) {

        if (jsonArrayDataAspectosMecanicos != null) {
            ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
            if (responseLoginUser != null) {
                Date date = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
                String dateCreatedAt = ft.format(date).toString();
                Long rowAffected = dbSivad.addDataInspection(dateCreatedAt
                        , String.valueOf(responseLoginUser.getData().getIdUser())
                        , jsonArrayDataAspectosMecanicos.toString()
                        , null
                        , null);

                if (rowAffected > 0) {
                    idViewPager.setCurrentItem(1);
                    bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosElectricos).setEnabled(true);
                } else {
                    snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos de aspectos mécanicos");
                }

            } else {
                snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos aspectos mécanicos. No existen datos del usuario");
            }
        } else {
            snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos aspectos mécanicos. Datos de aspectos mécanicos nulos.");
        }
    }

    @Override
    public void registerNoveltiesInspectionAspectosMecanicos(boolean isToggleBad, String nameToggle, String nameFragment) {
        if (isToggleBad) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            DialogFragment dialogFrag = new ViewNovedades();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isToggleBad", isToggleBad);
            bundle.putString("nameToggle", nameToggle);
            bundle.putString("nameFragment", nameFragment);
            dialogFrag.setArguments(bundle);
            dialogFrag.setCancelable(false);
            dialogFrag.show(ft, "dialog");
            //finish();
        }
    }

    @Override
    public void sendDataAspectosElectricos(JSONArray jsonArrayDataAspectosElectricos, int idInpection) {
        idInspectionGeneral = idInpection;
        if (jsonArrayDataAspectosElectricos != null) {
            ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
            if (responseLoginUser != null) {
                Long rowAffected = Long.valueOf(dbSivad.updateDataInspectionAspectosElectricos(jsonArrayDataAspectosElectricos.toString(), idInpection, responseLoginUser.getData().getIdUser()));
                Log.v("rowAffected AE", String.valueOf(rowAffected));
                if (rowAffected > 0) {
                    idViewPager.setCurrentItem(2);
                    bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosGenerales).setEnabled(true);
                } else {
                    snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos de aspectos eléctricos");
                }

            } else {
                snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos aspectos eléctricos. No existen datos del usuario");
            }
        } else {
            snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos aspectos eléctricos. Datos de aspectos eléctricos nulos.");
        }

    }

    @Override
    public void registerNoveltiesInspectionAspectosElectricos(boolean isToggleBad, String nameToggle, String nameFragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFrag = new ViewNovedades();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isToggleBad", isToggleBad);
        bundle.putString("nameToggle", nameToggle);
        bundle.putString("nameFragment", nameFragment);
        dialogFrag.setArguments(bundle);
        dialogFrag.setCancelable(false);
        dialogFrag.show(ft, "dialog");
    }

    @Override
    public void sendDataAspectosGenerales(JSONArray jsonArrayDataAspectosGenerales, int idInspection, int valueCheckBox) {

        if (jsonArrayDataAspectosGenerales != null) {
            ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
            if (responseLoginUser != null) {
                Long rowAffected = Long.valueOf(dbSivad.updateDataInspectionAspectosGenerales(jsonArrayDataAspectosGenerales.toString(), idInspection, responseLoginUser.getData().getIdUser()));

                if (rowAffected > 0) {
                    ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();

                    if (responseValidatePlateMileage != null) {
                        sendDataInspection(idInspection
                                , responseLoginUser.getData().getIdUser()
                                , responseValidatePlateMileage.getData().getIdVehicle()
                                , valueCheckBox);
                    } else {
                        snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos de aspectos generales. No existen datos del vehículo.");
                    }

                } else {
                    snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos de aspectos eléctricos.");
                }

            } else {
                snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos aspectos eléctricos. No existen datos del usuario.");
            }
        } else {
            snackBarPersonalized.showSnackBarErrorCualquiera("Error en almacenar los datos aspectos eléctricos. Datos de aspectos eléctricos nulos.");
        }

    }

    @Override
    public void registerNoveltiesInspectionAspectosGenerales(boolean isToggleBad, String nameToggle, String nameFragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFrag = new ViewNovedades();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isToggleBad", isToggleBad);
        bundle.putString("nameToggle", nameToggle);
        bundle.putString("nameFragment", nameFragment);
        dialogFrag.setArguments(bundle);
        dialogFrag.setCancelable(false);
        dialogFrag.show(ft, "dialog");
    }

    @Override
    public void sendAlertToggleBad(boolean isToggleBad, String nameToggle, String nameFragment) {
        if (isToggleBad) {

            ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
            if (responseLoginUser != null) {

                Date date = new Date();
                Locale spanish = new Locale("es", "ES");
                SimpleDateFormat ft = new SimpleDateFormat("EEEE d' de 'MMMM' de 'yyyy", spanish);
                SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm a");
                String dateRegister = ft.format(date).toString();
                String timeRegister = formatTime.format(date).toString();

                SimpleDateFormat ftTwo = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String dateRegisterAlertInspectionItemBad = ftTwo.format(date).toString();


                ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
                if (responseValidatePlateMileage != null) {
                    interfacesPresenterInspectionPreoperational.postDataAlertByItemBadToggle(responseLoginUser.getData().getIdUser()
                            , responseLoginUser.getData().getName()
                            , responseLoginUser.getData().getLastName()
                            , responseValidatePlateMileage.getData().getPlate()
                            , responseValidatePlateMileage.getData().getIdVehicle()
                            , dateRegister
                            , timeRegister
                            , nameToggle
                            , nameFragment
                            , dateRegisterAlertInspectionItemBad);
                    progressDialog.setMessage("Cargando...\n" +
                            "Espera mientras generamos la alerta.");
                    progressDialog.show();
                } else {
                    snackBarPersonalized.showSnackBarErrorCualquiera("Error al enviar la Alerta. No existen datos del Vehículo.");
                }

            } else {
                snackBarPersonalized.showSnackBarErrorCualquiera("Error al enviar la Alerta. No existen datos del Usuario.");
            }
        }
    }

    @Override
    public void responseSuccesPostDataAlertByItemBadToggle(ResponseJson responseJson) {
        progressDialog.dismiss();

        //Almaceno la data alerta

        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
        String dateCreatedAt = ft.format(date).toString();

        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();

        Gson gson = new Gson();
        String jsonString = gson.toJson(responseJson);

        //saveValuePreference(context, true);
        Long rowAffected = dbSivad.addDataResponseAlertNotificationEmail(responseLoginUser.getData().getIdUser(), dateCreatedAt, jsonString);
        dbSivad.deleteDataInspectionById(idInspectionGeneral);
        if (rowAffected != 0 && rowAffected > 0) {
            ChocoBar.builder().setActivity(ViewAlistamientoCargaFurgones.this)
                    .setActionText("OK")
                    .setActionClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ViewAlistamientoCargaFurgones.this, ViewMenu.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setText(responseJson.getMessage())
                    .setDuration(ChocoBar.LENGTH_INDEFINITE)
                    .green()
                    .show();
        } else {
            snackBarPersonalized.showSnackBarErrorCualquiera("Error al almacenar los datos de la Alerta Notificación");
        }
    }

    @Override
    public void responseErrorPostDataAlertByItemBadToggle(ResponseError responseError, JSONObject jsonObject) {
        progressDialog.dismiss();
        dataOffline = jsonObject.toString();
        nameModule = ConstantsStrings.MODULE_INSPECCION_PREOPERACIONAL_NOTIFICATION_ALERT;
        Log.v("nameModule", nameModule);
        Log.v("nameModule", dataOffline);
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        dbSivad.deleteDataInspectionById(idInspectionGeneral);
        dbSivad.addDataOffline(formatDates.getDateRegisterFormatDateWithTime(), responseLoginUser.getData().getIdUser(), ConstantsStrings.MODULE_INSPECCION_PREOPERACIONAL_NOTIFICATION_ALERT, dataOffline);
        snackBarPersonalized.showSnackbarWithAction(4, responseError.getMessage());
    }

    private void sendDataInspection(int idInspection, int idUser, int idVehicle, int valueCheckBox) {
        //if (isConnect) {

            //Obtengo el numero de inspecciones realizadas
            int inspectionEnabled = 1;
            ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
            ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
            int numInspectionsVehicle = responseValidatePlateMileage.getData().getNumberInspections();
            int numberInspectionsPerformed = dbSivad.getNumRowsOfInspections(responseLoginUser.getData().getIdUser());

            if(numberInspectionsPerformed == numInspectionsVehicle || numberInspectionsPerformed > numInspectionsVehicle){
                inspectionEnabled = 0;
            }

            JSONArray jsonArrayDataInspection = dbSivad.getAllDataInspection(idUser, idInspection);

            List<PojoNoveltieInspection> dataNoveltiesInspection = dbSivad.getDataNoveltiesInspection(idUser);
            JSONArray jsonArrayDataNovelties = new JSONArray();
            JSONObject jsonObjectDataNovelties = new JSONObject();
            if (dataNoveltiesInspection.size() > 0) {
                for (int i = 0; i < dataNoveltiesInspection.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("dateNoveltie", dataNoveltiesInspection.get(i).getDateNoveltie());
                        jsonObject.put("timeNoveltie", dataNoveltiesInspection.get(i).getTimeNoveltie());
                        jsonObject.put("descriptionNoveltie", dataNoveltiesInspection.get(i).getDescriptionNoveltie());
                        jsonObject.put("observationNoveltie", dataNoveltiesInspection.get(i).getObservation());
                        jsonArrayDataNovelties.put(jsonObject);
                        jsonObjectDataNovelties.put("data", jsonArrayDataNovelties);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            interfacesPresenterInspectionPreoperational.postDataInspectionPreoperational(formatDates.getDateRegisterFormatDateWithTime()
                    , idUser
                    , idVehicle
                    , jsonArrayDataInspection
                    , jsonObjectDataNovelties
                    , valueCheckBox
                    , inspectionEnabled);

            progressDialog.setMessage(ConstantsStrings.LOAD_NOTIFICATION_ALERT);
            progressDialog.show();

        /*} else {

            snackBarPersonalized.showSnackbarWithAction(4, "Sin Conexión a intenert. Pulsa (ok) para almacenar offline.");
        }*/
    }

    @Override
    public void interfacesViewSuccessResponse(ResponseVehicleInspecion responseVehicleInspecion) {
        progressDialog.dismiss();
        Gson gson = new Gson();
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        String registrationDate = formatDates.getDateRegisterFormatDateWithTime();

        //Actualizo el numero de inspecciones por dia
        /*ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        responseValidatePlateMileage.getData().setNumberInspections(responseValidatePlateMileage.getData().getNumberInspections() - 1);
        Gson gson = new Gson();
        String json = gson.toJson(responseValidatePlateMileage);
        int idDelete = dbSivad.deleteDataVehicle();
        Long idAdd = dbSivad.addDataVehicle(formatDates.getDateRegisterFormatDateWithTime(), json);*/
        String jsonStringDataVehicleInspection = gson.toJson(responseVehicleInspecion);

        //saveValuePreference(context, true);
        Long rowAffected = dbSivad.addDataVehicleInspection(registrationDate
                , responseLoginUser.getData().getIdUser()
                , jsonStringDataVehicleInspection);

        if (rowAffected > 0) {

            ChocoBar.builder().setActivity(ViewAlistamientoCargaFurgones.this)
                    .setActionText("OK")
                    .setActionClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //dbSivad.deleteDataNoveltiesInspection();
                            Intent intent = new Intent(ViewAlistamientoCargaFurgones.this, ViewMenu.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setText(responseVehicleInspecion.getMessage())
                    .setDuration(ChocoBar.LENGTH_INDEFINITE)
                    .green()
                    .show();
        } else {
            snackBarPersonalized.showSnackBarErrorCualquiera("Error al almacenar la respuesa de inspección de forma local");
        }
    }

    @Override
    public void interfacesViewErrorResponse(ResponseError responseError, JSONObject jsonObject) {
        progressDialog.dismiss();
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
        String registrationDate = formatDates.getDateRegisterFormatDateWithTime();
        dbSivad.addDataOffline(registrationDate, responseLoginUser.getData().getIdUser(), ConstantsStrings.MODULE_INSPECCION_PREOPERACIONAL_CARGA_FURGONES, jsonObject.toString());
        int idRegister = dbSivad.getIdLastInspectionPreoperational(responseLoginUser.getData().getIdUser());
        Log.v("idRegister", String.valueOf(idRegister));
        DataVehicleResponse dataVehicleResponse = new DataVehicleResponse();
        dataVehicleResponse.setIdInspection(idRegister);
        dataVehicleResponse.setDateRegister(registrationDate);

        ResponseVehicleInspecion responseVehicleInspecion = new ResponseVehicleInspecion();
        responseVehicleInspecion.setData(dataVehicleResponse);

        Gson gson = new Gson();
        String jsonStringDataVehicleInspection = gson.toJson(responseVehicleInspecion);

        dbSivad.addDataVehicleInspection(registrationDate, responseLoginUser.getData().getIdUser(), jsonStringDataVehicleInspection);
        snackBarPersonalized.showSnackbarWithAction(4, responseError.getMessage());
    }

    @Override
    public void responseSnackbarWithAction() {
        Intent intent = new Intent(ViewAlistamientoCargaFurgones.this, ViewMenu.class);
        startActivity(intent);
        finish();
    }
}

package com.project.sivad.Modulos.InspeccionPreoperacional.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces.InterfacesInspeccionPreoperation;
import com.project.sivad.Modulos.InspeccionPreoperacional.Pojo.PojoNoveltieInspection;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterNovelties extends RecyclerView.Adapter<AdapterNovelties.ViewHolder> {

    List<PojoNoveltieInspection> pojoNoveltieInspectionList = new ArrayList<>();
    Context context;
    InterfacesInspeccionPreoperation.InterfacesAdapterNoveltie interfacesAdapterNoveltie;

    public AdapterNovelties(List<PojoNoveltieInspection> pojoNoveltieInspectionList, InterfacesInspeccionPreoperation.InterfacesAdapterNoveltie interfacesAdapterNoveltie) {
        this.pojoNoveltieInspectionList = pojoNoveltieInspectionList;
        this.interfacesAdapterNoveltie = interfacesAdapterNoveltie;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_noveltie_inspection, parent, false);
        ViewHolder pvh = new ViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        int idNoveltieInspection = position + 1;
        holder.idAppCompatTextviewIdNoveltie.setText(String.valueOf(idNoveltieInspection));
        holder.idAppCompatTextviewDateNoveltie.setText(pojoNoveltieInspectionList.get(position).getDateNoveltie());
        holder.idAppCompatTextviewTimeNoveltie.setText(pojoNoveltieInspectionList.get(position).getTimeNoveltie());
        holder.idAppCompatTextviewDescriptionNoveltie.setText(pojoNoveltieInspectionList.get(position).getDescriptionNoveltie());
        holder.idAppCompatTextviewObservationNoveltie.setText(pojoNoveltieInspectionList.get(position).getObservation());
        holder.idAppCompatTextviewTypeNoveltie.setText(pojoNoveltieInspectionList.get(position).getTypeNoveltie());

        holder.idMaterialButtonDeleteNoveltie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pojoNoveltieInspectionList.get(position).getTypeNoveltie().equals(ConstantsStrings.TYPENOVELTIESFREE)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(holder.context);
                    builder.setMessage("¿Esta seguro que desea eliminar esta Novedad?").setCancelable(false)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    interfacesAdapterNoveltie.removeItemNoveltie(position);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setTitle("Eliminar Novedad Inspección Libre");
                    dialog.show();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(holder.context);
                    builder.setMessage("Esta novedad corresponde a " + pojoNoveltieInspectionList.get(position).getNameFragment()
                                    + " del Item " + pojoNoveltieInspectionList.get(position).getNameToggle()
                                    + ", que usted seleccionó como Malo (M).\n" +
                            "Si la elimina esta novedad, asegúrese de cambiar la opción que seleccinó.\n\n" +
                            "¿Está seguro que desea eliminar esta Novedad?").setCancelable(false)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    interfacesAdapterNoveltie.removeItemNoveltie(position);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setTitle("Eliminar Novedad Selección Item Malo (M)");
                    dialog.show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return pojoNoveltieInspectionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.idAppCompatImageViewIconNovelties)
        AppCompatImageView idAppCompatImageViewIconNovelties;
        @BindView(R.id.idAppCompatTextviewIdNoveltie)
        AppCompatTextView idAppCompatTextviewIdNoveltie;
        @BindView(R.id.idAppCompatTextviewDateNoveltie)
        AppCompatTextView idAppCompatTextviewDateNoveltie;
        @BindView(R.id.idAppCompatTextviewTimeNoveltie)
        AppCompatTextView idAppCompatTextviewTimeNoveltie;
        @BindView(R.id.idAppCompatTextviewDescriptionNoveltie)
        AppCompatTextView idAppCompatTextviewDescriptionNoveltie;
        @BindView(R.id.idAppCompatTextviewObservationNoveltie)
        AppCompatTextView idAppCompatTextviewObservationNoveltie;
        @BindView(R.id.idLinearLayoutCompatExpandleDetail)
        LinearLayoutCompat idLinearLayoutCompatExpandleDetail;
        @BindView(R.id.idLinearLayoutCompatExpandle)
        LinearLayoutCompat idLinearLayoutCompatExpandle;
        @BindView(R.id.idMaterialButtonDeleteNoveltie)
        MaterialButton idMaterialButtonDeleteNoveltie;
        @BindView(R.id.idAppCompatTextviewTypeNoveltie)
        AppCompatTextView idAppCompatTextviewTypeNoveltie;

        private Context context;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            context = itemView.getContext();
        }
    }
}

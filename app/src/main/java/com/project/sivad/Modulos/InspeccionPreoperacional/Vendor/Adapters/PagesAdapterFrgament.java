package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular.FragmentPreInspeccionVehicularAspectosElectricos;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentAspectosGenerales;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular.FragmentPreInspeccionVehicularAspectosMecanicos;


public class PagesAdapterFrgament extends FragmentPagerAdapter {
    private static int numItemFragments = 3;

    public PagesAdapterFrgament(FragmentManager fm ) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                FragmentAspectosGenerales fragmentAspectosGenerales = new FragmentAspectosGenerales();
                return fragmentAspectosGenerales;
            case 1:
                FragmentPreInspeccionVehicularAspectosMecanicos fragmentPreInspeccionVehicularAspectosMecanicos = new FragmentPreInspeccionVehicularAspectosMecanicos();
                return fragmentPreInspeccionVehicularAspectosMecanicos;
            case 2:
                FragmentPreInspeccionVehicularAspectosElectricos fragmentPreInspeccionVehicularAspectosElectricos = new FragmentPreInspeccionVehicularAspectosElectricos();
                return fragmentPreInspeccionVehicularAspectosElectricos;
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return numItemFragments;
    }

}

package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentPreInspeccionVehicularAspectosElectricos extends Fragment {

    @BindView(R.id.toggleSwitchLucesAltas)
    ToggleSwitch toggleSwitchLucesAltas;
    @BindView(R.id.toggleSwitchLucesBajas)
    ToggleSwitch toggleSwitchLucesBajas;
    @BindView(R.id.toggleSwitchLucesStop)
    ToggleSwitch toggleSwitchLucesStop;
    @BindView(R.id.toggleSwitchDireccionalesDelanteras)
    ToggleSwitch toggleSwitchDireccionalesDelanteras;
    @BindView(R.id.toggleSwitchDireccionalesTraseras)
    ToggleSwitch toggleSwitchDireccionalesTraseras;
    @BindView(R.id.toggleSwitchLimpaBrisasIzqAtr)
    ToggleSwitch toggleSwitchLimpaBrisasIzqAtr;
    @BindView(R.id.toggleSwitchLimpiaBrisasNivelesAgua)
    ToggleSwitch toggleSwitchLimpiaBrisasNivelesAgua;
    @BindView(R.id.toggleSwitchEspejosLateraleIzq)
    ToggleSwitch toggleSwitchEspejosLateraleIzq;
    @BindView(R.id.toggleSwitchEspejosRetrovisores)
    ToggleSwitch toggleSwitchEspejosRetrovisores;
    @BindView(R.id.appCompatCheckBoxFirma)
    AppCompatCheckBox appCompatCheckBoxFirma;
    @BindView(R.id.btnFinalizarInspeccion)
    AppCompatButton btnFinalizarInspeccion;
    @BindView(R.id.nestedScrollViewAspectosElectricos)
    NestedScrollView nestedScrollViewAspectosElectricos;

    public FragmentPreInspeccionVehicularAspectosElectricos() {
        // Required empty public constructor
    }


    public static FragmentPreInspeccionVehicularAspectosElectricos newInstance(String param1, String param2) {
        FragmentPreInspeccionVehicularAspectosElectricos fragment = new FragmentPreInspeccionVehicularAspectosElectricos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preinspeccionvehicular_aspectos_electricos, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}

package com.project.sivad.Modulos.InspeccionPreoperacional.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters.PagesAdapterFragmentsInspeccionPre;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular.FragmentPreInspeccionVehicularAspectosElectricos;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular.FragmentPreInspeccionVehicularAspectosGenerales;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular.FragmentPreInspeccionVehicularAspectosMecanicos;
import com.project.sivad.R;
import com.project.sivad.Util.ViewPagerTB;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewInspecionPreoperacional extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    //Declaramos las variables XML
    @BindView(R.id.idToolbarViewInspeccionPreoperacional)
    Toolbar toolbarViewInspeccionPreoperacional;
    @BindView(R.id.idAppBarLayouViewInspeccionPreoperacional)
    AppBarLayout appBarLayouViewInspeccionPreoperacional;

    @BindView(R.id.idBottomNavigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.idFloatingActionButton)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.idViewPagerViewInspeccionPreoperacional)
    ViewPagerTB idViewPagerViewInspeccionPreoperacional;

    private PagesAdapterFragmentsInspeccionPre pagesAdapterFrgament;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_preinspecion_vehicular);
        ButterKnife.bind(this);

        //Inicializamos las variables tipo Object
        context = this;

        setupToolbar(toolbarViewInspeccionPreoperacional);

        idViewPagerViewInspeccionPreoperacional.setOffscreenPageLimit(3);
        setupViewPager(getSupportFragmentManager(), idViewPagerViewInspeccionPreoperacional);
        idViewPagerViewInspeccionPreoperacional.setCurrentItem(0);
        idViewPagerViewInspeccionPreoperacional.setOnPageChangeListener(new PageFragmentChange());
        idViewPagerViewInspeccionPreoperacional.setCanScroll(false);

        bottomNavigation.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewInspecionPreoperacional.this, ViewNovedades.class);
                startActivity(intent);
                //finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("estoy aqui", "onStart");
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.v("estoy aqui", "onResume");

        modoInicial();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("estoy aqui", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Log.v("estoy aqui", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Log.v("estoy aqui", "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Log.v("estoy aqui", "onDestroy");
    }


    private void setupToolbar(Toolbar toolbarSetup) {
        setSupportActionBar(toolbarSetup);
    }

    private void setupViewPager(FragmentManager fragmentManager, ViewPager viewPagerSetup) {
        pagesAdapterFrgament = new PagesAdapterFragmentsInspeccionPre(fragmentManager);

        //Add All Fragment To List
        pagesAdapterFrgament.addFragment(new FragmentPreInspeccionVehicularAspectosGenerales(), getResources().getString(R.string.tab_label_aspectos_generales));
        pagesAdapterFrgament.addFragment(new FragmentPreInspeccionVehicularAspectosMecanicos(), getResources().getString(R.string.tab_label_aspectos_mecanicos));
        pagesAdapterFrgament.addFragment(new FragmentPreInspeccionVehicularAspectosElectricos(), getResources().getString(R.string.tab_label_aspectos_electricos));

        viewPagerSetup.setAdapter(pagesAdapterFrgament);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Log.v("menuItem", String.valueOf(menuItem));
        switch (menuItem.getItemId()) {
            case R.id.itemBottomNavigationAspectosGenerales:
                Log.v("estoy aqui", "0");
                idViewPagerViewInspeccionPreoperacional.setCurrentItem(0);
                return true;
            case R.id.itemBottomNavigationAspectosMecanico:
                Log.v("estoy aqui", "1");
                idViewPagerViewInspeccionPreoperacional.setCurrentItem(1);
                return false;
            case R.id.itemBottomNavigationAspectosElectricos:
                Log.v("estoy aqui", "2");
                idViewPagerViewInspeccionPreoperacional.setCurrentItem(2);
                return false;
        }
        return false;
    }

    public class PageFragmentChange implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    Log.v("estoy aqui", "0");
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosGenerales);
                    break;
                case 1:
                    Log.v("estoy aqui", "1");
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosMecanico);
                    break;
                case 2:
                    Log.v("estoy aqui", "2");
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosElectricos);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            Log.v("estoy aqui", "2");
        }
    }

    private void modoInicial() {
        Log.v("modoInicial","modoInicial");
        bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosMecanico).setEnabled(true);
        bottomNavigation.getMenu().findItem(R.id.itemBottomNavigationAspectosElectricos).setEnabled(true);
    }
}

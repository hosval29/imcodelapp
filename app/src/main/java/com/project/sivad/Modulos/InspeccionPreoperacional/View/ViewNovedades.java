package com.project.sivad.Modulos.InspeccionPreoperacional.View;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Modulos.InspeccionPreoperacional.Adapter.AdapterNovelties;
import com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces.InterfacesInspeccionPreoperation;
import com.project.sivad.Modulos.InspeccionPreoperacional.Pojo.PojoNoveltieInspection;
import com.project.sivad.Modulos.InspeccionPreoperacional.Presenter.PresenterInspectionPreoperational;
import com.project.sivad.R;
import com.project.sivad.Util.Components.FormatDates;
import com.project.sivad.Util.Components.SnackBarPersonalized;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewNovedades extends DialogFragment implements View.OnClickListener
        , InterfacesInspeccionPreoperation.InterfacesAdapterNoveltie
        , SnackBarPersonalized.InterfacesResponseSnackbarWithAction
        , InterfacesInspeccionPreoperation.InterfacesFragmentNoveltie {

    //Variables XML
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.idTextInputLayoutDescribeNovedad)
    TextInputLayout idTextInputLayoutDescribeNovedad;
    @BindView(R.id.idTextInputLayoutObservaciones)
    TextInputLayout idTextInputLayoutObservaciones;
    @BindView(R.id.idTextInputLayoutDateNoveltie)
    TextInputLayout idTextInputLayoutDateNoveltie;
    @BindView(R.id.idAppCompatButtonAddDateNoveltie)
    AppCompatImageButton idAppCompatButtonAddDateNoveltie;
    @BindView(R.id.idTextInputLayoutTimeNoveltie)
    TextInputLayout idTextInputLayoutTimeNoveltie;
    @BindView(R.id.idAppCompatButtonAddTimeNoveltie)
    AppCompatImageButton idAppCompatButtonAddTimeNoveltie;
    @BindView(R.id.idAppCompactButtonAddNovelties)
    AppCompatButton idAppCompactButtonAddNovelties;
    @BindView(R.id.idLinearLayoutNoNovedad)
    LinearLayoutCompat idLinearLayoutNoNovedad;
    @BindView(R.id.idRecyclerViewNovedades)
    RecyclerView idRecyclerViewNovedades;
    @BindView(R.id.idLinearLayoutButtonAddNoveltie)
    LinearLayoutCompat idLinearLayoutButtonAddNoveltie;
    @BindView(R.id.idLinearLayoutButtonsAceptarAndCancelar)
    LinearLayoutCompat idLinearLayoutButtonsAceptarAndCancelar;

    //Variables Utilidades
    public final Calendar c = Calendar.getInstance();
    @BindView(R.id.idAppCompatTextviewError)
    AppCompatTextView idAppCompatTextviewError;
    @BindView(R.id.idLinearLayoutError)
    LinearLayoutCompat idLinearLayoutError;
    @BindView(R.id.idLinearLayoutRecyclerviewNovelties)
    LinearLayoutCompat idLinearLayoutRecyclerviewNovelties;
    @BindView(R.id.idMaterialButtonCancelarNovelties)
    MaterialButton idMaterialButtonCancelarNovelties;
    @BindView(R.id.idMaterialButtonAceptarNovelties)
    MaterialButton idMaterialButtonAceptarNovelties;
    @BindView(R.id.idMaterialButtonOk)
    MaterialButton idMaterialButtonOk;
    @BindView(R.id.idLinearLayoutButtonOk)
    LinearLayoutCompat idLinearLayoutButtonOk;

    //Declaramos los Objects
    private Context context;
    private SnackBarPersonalized snackBarPersonalized;
    private ProgressDialog progressDialog;
    private FormatDates formatDates;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Variables String y Int
    private static final String CERO = "0";
    private static final String BARRA = "/";
    private static final String DOS_PUNTOS = ":";
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);
    private String nameToggle = "";
    private String nameFragment = "";
    public static String TAG = "FullScreenDialog";
    private int idInspection = 0;
    private String dataOffline = "";

    //Declaramo las variables de validacion
    private boolean isToggleBad = false;
    private boolean isCancelRegisterNoveltieInspection = false;
    private boolean isNoveltiesInspectionEventual = false;

    //Variables Recyclerview
    private AdapterNovelties adapterNovelties;
    private List<PojoNoveltieInspection> pojoNoveltieInspectionList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;

    //Instanciamos la interfaces
    private InterfacesInspeccionPreoperation.InterfacesPresenterInspectionPreoperational interfacesPresenterInspectionPreoperational;
    InterfaceViewNovelties interfcesViewNovelties;

    public interface InterfaceViewNovelties {
        void responsePostDataNoveltiesInspecion(int code, String message);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                nameToggle = bundle.getString("nameToggle") != null && !bundle.getString("nameToggle").isEmpty() ? bundle.getString("nameToggle") : "";
                isToggleBad = bundle.getBoolean("isToggleBad") ? bundle.getBoolean("isToggleBad") : false;
                nameFragment = bundle.get("nameFragment") != null && !bundle.getString("nameFragment").isEmpty() ? bundle.getString("nameFragment") : "";
                isNoveltiesInspectionEventual = bundle.getBoolean("isNoveltiesInspectionEventual") ? bundle.getBoolean("isNoveltiesInspectionEventual") : false;
                idInspection = bundle.getInt("idInspection") != 0 ? bundle.getInt("idInspection") : 0;
            }

        }

        /*Log.v("nameToggle", nameToggle);
        Log.v("nameFragment", nameFragment);
        Log.v("isToggleBad", String.valueOf(isToggleBad));*/


        //Inicializamos los Object
        context = getContext();
        dbSivad = new DBSivad(context);
        progressDialog = new ProgressDialog(context);
        formatDates = new FormatDates();
        snackBarPersonalized = new SnackBarPersonalized(getActivity());
        interfacesPresenterInspectionPreoperational = new PresenterInspectionPreoperational(null
                , this
                , null
                , context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_novedades, container, false);
        ButterKnife.bind(this, view);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view1 -> {
            validateRegisterNoveltiesInspectionToggleBad();
        });

        toolbar.setTitle("Añadir Novedad");

        idAppCompatButtonAddDateNoveltie.setOnClickListener(this);
        idAppCompatButtonAddTimeNoveltie.setOnClickListener(this);
        idAppCompactButtonAddNovelties.setOnClickListener(this);

        idTextInputLayoutTimeNoveltie.getEditText().setEnabled(false);
        idTextInputLayoutDateNoveltie.getEditText().setEnabled(false);

        Log.v("isNoveltiesInspectio", String.valueOf(isNoveltiesInspectionEventual));
        Log.v("idInspection", String.valueOf(idInspection));
        if (isNoveltiesInspectionEventual && idInspection != 0) {
            idLinearLayoutButtonsAceptarAndCancelar.setVisibility(View.VISIBLE);
            idLinearLayoutRecyclerviewNovelties.setVisibility(View.GONE);
        } else {
            idLinearLayoutButtonsAceptarAndCancelar.setVisibility(View.GONE);
            idLinearLayoutRecyclerviewNovelties.setVisibility(View.VISIBLE);
        }

        if (isNoveltiesInspectionEventual) {
            idLinearLayoutButtonAddNoveltie.setVisibility(View.GONE);
            idLinearLayoutButtonsAceptarAndCancelar.setVisibility(View.VISIBLE);
        } else {
            idLinearLayoutButtonAddNoveltie.setVisibility(View.VISIBLE);
            idLinearLayoutButtonsAceptarAndCancelar.setVisibility(View.GONE);
        }

        idTextInputLayoutDescribeNovedad.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutDescribeNovedad.isErrorEnabled()) {
                    idTextInputLayoutDescribeNovedad.setErrorEnabled(false);
                }

                if (idTextInputLayoutDateNoveltie.isErrorEnabled()) {
                    idTextInputLayoutDateNoveltie.setErrorEnabled(false);
                }

                if (idTextInputLayoutTimeNoveltie.isErrorEnabled()) {
                    idTextInputLayoutTimeNoveltie.setErrorEnabled(false);
                }

                if (idTextInputLayoutObservaciones.isErrorEnabled()) {
                    idTextInputLayoutObservaciones.setErrorEnabled(false);
                }

                idLinearLayoutError.setVisibility(View.GONE);
                idAppCompatTextviewError.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idTextInputLayoutObservaciones.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (idTextInputLayoutDescribeNovedad.isErrorEnabled()) {
                    idTextInputLayoutDescribeNovedad.setErrorEnabled(false);
                }

                if (idTextInputLayoutDateNoveltie.isErrorEnabled()) {
                    idTextInputLayoutDateNoveltie.setErrorEnabled(false);
                }

                if (idTextInputLayoutTimeNoveltie.isErrorEnabled()) {
                    idTextInputLayoutTimeNoveltie.setErrorEnabled(false);
                }

                if (idTextInputLayoutObservaciones.isErrorEnabled()) {
                    idTextInputLayoutObservaciones.setErrorEnabled(false);
                }

                idLinearLayoutError.setVisibility(View.GONE);
                idAppCompatTextviewError.setText("");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        setupRecyclerViewNovelties();
        setListNoveltiesDataBase();

        idMaterialButtonAceptarNovelties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateInputs()) {
                    progressDialog.setMessage(ConstantsStrings.LOAD);
                    progressDialog.show();
                    postDataNoveltiesInspection();
                }
            }
        });

        idMaterialButtonCancelarNovelties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        idMaterialButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String registrationDate = formatDates.getDateRegisterFormatDateWithTime();
                ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();
                int idUser = responseLoginUser.getData().getIdUser();
                dbSivad.addDataOffline(registrationDate, idUser, ConstantsStrings.MODULE_INSPECCION_PREOPERACIONAL_NOVEDAD_INSPECTION, dataOffline);
                interfcesViewNovelties.responsePostDataNoveltiesInspecion(200, "Almacenamiento offline con éxito.");
                dismiss();
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isToggleBad = !isToggleBad;
        nameFragment = "";
        nameToggle = "";
        if (pojoNoveltieInspectionList.size() > 0) {
            saveDataNoveltiesInspection();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        /*if (getActivity() instanceof interfcesViewNovelties) {
            interfcesViewNovelties = (InterfaceViewNovelties) getActivity();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/

        try {
            interfcesViewNovelties = (InterfaceViewNovelties) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnSaveDialogListener");
        }
    }


    private void setupRecyclerViewNovelties() {
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        idRecyclerViewNovedades.setHasFixedSize(true);
        idRecyclerViewNovedades.setLayoutManager(linearLayoutManager);
        adapterNovelties = new AdapterNovelties(pojoNoveltieInspectionList, this::removeItemNoveltie);
        idRecyclerViewNovedades.setAdapter(adapterNovelties);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.idAppCompatButtonAddDateNoveltie:
                obtenerFecha();
                break;
            case R.id.idAppCompatButtonAddTimeNoveltie:
                obtenerHora();
                break;
            case R.id.idAppCompactButtonAddNovelties:
                addNoveltie();
                break;
        }
    }

    private void obtenerFecha() {
        DatePickerDialog recogerFecha = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10) ? CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10) ? CERO + String.valueOf(mesActual) : String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                idTextInputLayoutDateNoveltie.getEditText().setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
                if (idTextInputLayoutDateNoveltie.isErrorEnabled()) {
                    idTextInputLayoutDateNoveltie.setErrorEnabled(false);
                }

            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        }, anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();

    }

    private void obtenerHora() {
        TimePickerDialog recogerHora = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada = (hourOfDay < 10) ? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10) ? String.valueOf(CERO + minute) : String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if (hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                idTextInputLayoutTimeNoveltie.getEditText().setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
                if (idTextInputLayoutTimeNoveltie.isErrorEnabled()) {
                    idTextInputLayoutTimeNoveltie.setErrorEnabled(false);
                }
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    private void setListNoveltiesDataBase() {
        ResponseLoginUser responseLoginUser = new ResponseLoginUser();
        responseLoginUser = dbSivad.getDataLoginUser();
        if (responseLoginUser != null) {
            if (dbSivad.getDataNoveltiesInspection(responseLoginUser.getData().getIdUser()) != null) {
                pojoNoveltieInspectionList.addAll(dbSivad.getDataNoveltiesInspection(responseLoginUser.getData().getIdUser()));
                adapterNovelties.notifyDataSetChanged();
                if (pojoNoveltieInspectionList.size() > 0) {
                    idLinearLayoutNoNovedad.setVisibility(View.GONE);
                }
            }
        }
    }

    private void addNoveltie() {
        if (!validateInputs()) {
            String descriptionNoveltie = idTextInputLayoutDescribeNovedad.getEditText().getText().toString();
            String observation = idTextInputLayoutObservaciones.getEditText().getText().toString();
            String dateNoveltie = idTextInputLayoutDateNoveltie.getEditText().getText().toString().trim();
            String timeNoveltie = idTextInputLayoutTimeNoveltie.getEditText().getText().toString();
            String typeNoveltie = isToggleBad ? ConstantsStrings.TYPENOVELTIESTOGGLEBAD : ConstantsStrings.TYPENOVELTIESFREE;

            PojoNoveltieInspection pojoNoveltieInspection = new PojoNoveltieInspection(dateNoveltie, descriptionNoveltie, observation, timeNoveltie, typeNoveltie, nameFragment, nameToggle);
            pojoNoveltieInspectionList.add(pojoNoveltieInspection);
            adapterNovelties.notifyDataSetChanged();

            idTextInputLayoutDescribeNovedad.getEditText().setText("");
            idTextInputLayoutObservaciones.getEditText().setText("");
            idTextInputLayoutDateNoveltie.getEditText().setText("");
            idTextInputLayoutTimeNoveltie.getEditText().setText("");

            if (pojoNoveltieInspectionList.size() > 0) {
                idLinearLayoutNoNovedad.setVisibility(View.GONE);
            }

            if (isToggleBad) {
                isToggleBad = false;
            }
        }
    }

    private boolean validateInputs() {
        boolean error = false;
        if (idTextInputLayoutDescribeNovedad.getEditText().getText().toString().matches("")) {
            idTextInputLayoutDescribeNovedad.setError("Campo obligatorio.");
            error = true;
        }

        if (idTextInputLayoutObservaciones.getEditText().getText().toString().matches("")) {
            idTextInputLayoutObservaciones.setError("Campo obligatorio.");
            error = true;
        }

        if (idTextInputLayoutDateNoveltie.getEditText().getText().toString().trim().matches("")) {
            idTextInputLayoutDateNoveltie.setError("Campo obligatorio.");
            error = true;
        }

        if (idTextInputLayoutTimeNoveltie.getEditText().getText().toString().matches("")) {
            idTextInputLayoutTimeNoveltie.setError("Campo obligatorio.");
            error = true;
        }

        return error;
    }

    private void validateRegisterNoveltiesInspectionToggleBad() {

        String message = "";
        if (!isToggleBad) {
            message = "¿Está seguro que no desea ir atras?";

        } else {
            message = "Usted selecciono a " + nameToggle + " de " + nameFragment + " como Malo(M). ¿Está seguro que no desea resgistrar la Novedad?";
        }

        //snackBarPersonalized.showSnackbarWithAction(4, message);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Cancelar Registro Novedad");
        dialog.show();
    }

    private void saveDataNoveltiesInspection() {

        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreatedAt = ft.format(date).toString();
        ResponseLoginUser responseLoginUser = dbSivad.getDataLoginUser();

        JSONArray jsonArray = new JSONArray();
        if (pojoNoveltieInspectionList.size() > 0) {

            for (int i = 0; i < pojoNoveltieInspectionList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("typeNoveltie", pojoNoveltieInspectionList.get(i).getTypeNoveltie());
                    jsonObject.put("nameToggle", pojoNoveltieInspectionList.get(i).getNameToggle());
                    jsonObject.put("nameFragment", pojoNoveltieInspectionList.get(i).getNameFragment());
                    jsonObject.put("dateNoveltie", pojoNoveltieInspectionList.get(i).getDateNoveltie());
                    jsonObject.put("descriptionNoveltie", pojoNoveltieInspectionList.get(i).getDescriptionNoveltie());
                    jsonObject.put("observationNoveltie", pojoNoveltieInspectionList.get(i).getObservation());
                    jsonObject.put("timeNoveltie", pojoNoveltieInspectionList.get(i).getTimeNoveltie());
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if (responseLoginUser != null) {
            dbSivad.deleteDataNoveltiesInspection();
            dbSivad.addDataNoveltiesInspection(dateCreatedAt, responseLoginUser.getData().getIdUser(), jsonArray.toString());
        }
    }

    @Override
    public void removeItemNoveltie(int index) {
        pojoNoveltieInspectionList.remove(index);
        adapterNovelties.notifyDataSetChanged();
        if (pojoNoveltieInspectionList.size() <= 0) {
            pojoNoveltieInspectionList.clear();
            dbSivad.deleteDataNoveltiesInspection();
            idLinearLayoutNoNovedad.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void responseSnackbarWithAction() {

    }

    private void postDataNoveltiesInspection() {


        String dateNovelties = idTextInputLayoutDateNoveltie.getEditText().getText().toString();
        String timeNovelties = idTextInputLayoutTimeNoveltie.getEditText().getText().toString();
        String descriptionNovelties = idTextInputLayoutDescribeNovedad.getEditText().getText().toString();
        String observationNovelties = idTextInputLayoutObservaciones.getEditText().getText().toString();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("idInspection", idInspection);
            jsonObject.put("dateNovelties", dateNovelties);
            jsonObject.put("timeNovelties", timeNovelties);
            jsonObject.put("descriptionNovelties", descriptionNovelties);
            jsonObject.put("observationNovelties", observationNovelties);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        interfacesPresenterInspectionPreoperational.postDataNoveltiesInspection(jsonObject);

    }

    @Override
    public void responseErrorPostDataNoveltiesInspection(ResponseError responseError, JSONObject jsonObject) {
        idLinearLayoutError.setVisibility(View.VISIBLE);
        idAppCompatTextviewError.setText(responseError.getMessage());
        progressDialog.dismiss();
        dataOffline = jsonObject.toString();
        idAppCompatTextviewError.setText(responseError.getMessage());
        idLinearLayoutButtonOk.setVisibility(View.VISIBLE);
        idLinearLayoutButtonsAceptarAndCancelar.setVisibility(View.GONE);
        idLinearLayoutButtonAddNoveltie.setVisibility(View.GONE);
    }

    @Override
    public void responseSuccesPostDataNoveltiesInspection(ResponseJson responseJson) {
        interfcesViewNovelties.responsePostDataNoveltiesInspecion(responseJson.getCodeStatus(), responseJson.getMessage());
        dismiss();
    }
}

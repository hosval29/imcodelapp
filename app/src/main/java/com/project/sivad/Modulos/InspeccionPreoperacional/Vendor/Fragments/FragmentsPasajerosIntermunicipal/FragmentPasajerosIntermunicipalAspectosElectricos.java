package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosIntermunicipal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPasajerosIntermunicipalAspectosElectricos extends Fragment {


    @BindView(R.id.toggleSwitchABSSRP)
    ToggleSwitch toggleSwitchABSSRP;
    @BindView(R.id.toggleSwitchDelanterasTraseras)
    ToggleSwitch toggleSwitchDelanterasTraseras;
    @BindView(R.id.toggleSwitchDerIzqAtrasPlumillas)
    ToggleSwitch toggleSwitchDerIzqAtrasPlumillas;
    @BindView(R.id.toggleSwitchNivelAgua)
    ToggleSwitch toggleSwitchNivelAgua;
    @BindView(R.id.toggleSwitchLateralesDerIzqRetrovior)
    ToggleSwitch toggleSwitchLateralesDerIzqRetrovior;
    @BindView(R.id.btnContinuarElectrico)
    AppCompatButton btnContinuarElectrico;

    public static FragmentPasajerosIntermunicipalAspectosElectricos newInstance(String param1, String param2) {
        FragmentPasajerosIntermunicipalAspectosElectricos fragment = new FragmentPasajerosIntermunicipalAspectosElectricos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pasajeros_intermunicipal_aspectos_electrico, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

}

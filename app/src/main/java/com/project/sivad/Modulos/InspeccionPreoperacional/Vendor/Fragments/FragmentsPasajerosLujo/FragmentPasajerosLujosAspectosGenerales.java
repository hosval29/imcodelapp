package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosLujo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPasajerosLujosAspectosGenerales extends Fragment {

    @BindView(R.id.toggleSwitchLSRTRRTPT)
    ToggleSwitch toggleSwitchLSRTRRTPT;
    @BindView(R.id.toggleSwitchCambioAceite)
    ToggleSwitch toggleSwitchCambioAceite;
    @BindView(R.id.toggleSwitchSincronizacion)
    ToggleSwitch toggleSwitchSincronizacion;
    @BindView(R.id.toggleSwitchAlineacionBalanceo)
    ToggleSwitch toggleSwitchAlineacionBalanceo;
    @BindView(R.id.toggleSwitchCambioLlantas)
    ToggleSwitch toggleSwitchCambioLlantas;
    @BindView(R.id.toggleSwitchPito)
    ToggleSwitch toggleSwitchPito;
    @BindView(R.id.toggleSwitchCinturonesSeguridad)
    ToggleSwitch toggleSwitchCinturonesSeguridad;
    @BindView(R.id.toggleSwitchCargaExtintores)
    ToggleSwitch toggleSwitchCargaExtintores;
    @BindView(R.id.toggleSwitchBotiquin)
    ToggleSwitch toggleSwitchBotiquin;
    @BindView(R.id.toggleSwitchVidrioPanoramico)
    ToggleSwitch toggleSwitchVidrioPanoramico;
    @BindView(R.id.toggleSwitchIdentidadCorporativa)
    ToggleSwitch toggleSwitchIdentidadCorporativa;
    @BindView(R.id.toggleSwitchAromaInteriorAseoGeneralVehiculo)
    ToggleSwitch toggleSwitchAromaInteriorAseoGeneralVehiculo;
    @BindView(R.id.toggleSwitchAvisoComoConduzo)
    ToggleSwitch toggleSwitchAvisoComoConduzo;
    @BindView(R.id.toggleSwitchCantidadesMontasCojines)
    ToggleSwitch toggleSwitchCantidadesMontasCojines;
    @BindView(R.id.toggleSwitchFuncionamientoWifi)
    ToggleSwitch toggleSwitchFuncionamientoWifi;
    @BindView(R.id.toggleSwitchRefrigerios)
    ToggleSwitch toggleSwitchRefrigerios;
    @BindView(R.id.toggleSwitchFuncionamientoTablets)
    ToggleSwitch toggleSwitchFuncionamientoTablets;
    @BindView(R.id.toggleSwitchCamaraFuncionamiento)
    ToggleSwitch toggleSwitchCamaraFuncionamiento;
    @BindView(R.id.toggleSwitchCamaraPosicion)
    ToggleSwitch toggleSwitchCamaraPosicion;
    @BindView(R.id.toggleSwitchTomaCorriente110v)
    ToggleSwitch toggleSwitchTomaCorriente110v;
    @BindView(R.id.toggleSwitchJaulaMascotasCandado)
    ToggleSwitch toggleSwitchJaulaMascotasCandado;
    @BindView(R.id.toggleSwitchSombrilla)
    ToggleSwitch toggleSwitchSombrilla;
    @BindView(R.id.toggleSwitchHCGTLC)
    ToggleSwitch toggleSwitchHCGTLC;
    @BindView(R.id.toggleSwitchNotaOperarVehiculo)
    ToggleSwitch toggleSwitchNotaOperarVehiculo;
    @BindView(R.id.appCompatCheckBoxFirma)
    AppCompatCheckBox appCompatCheckBoxFirma;
    @BindView(R.id.btnFinalizarInspeccion)
    AppCompatButton btnFinalizarInspeccion;

    public FragmentPasajerosLujosAspectosGenerales() {
        // Required empty public constructor
    }

    public static FragmentPasajerosLujosAspectosGenerales newInstance(String param1, String param2) {
        FragmentPasajerosLujosAspectosGenerales fragment = new FragmentPasajerosLujosAspectosGenerales();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pasajeros_lujos_aspectos_generales, container, false);
        ButterKnife.bind(this, view);
        toggleSwitchLSRTRRTPT.setCheckedPosition(0);
        toggleSwitchCambioAceite.setCheckedPosition(0);
        toggleSwitchSincronizacion.setCheckedPosition(0);
        toggleSwitchAlineacionBalanceo.setCheckedPosition(0);
        toggleSwitchCambioLlantas.setCheckedPosition(0);
        return view;
    }

}

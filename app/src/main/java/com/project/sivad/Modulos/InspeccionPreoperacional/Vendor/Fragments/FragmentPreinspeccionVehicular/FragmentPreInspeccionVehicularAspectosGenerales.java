package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;

import androidx.appcompat.widget.AppCompatButton;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentPreInspeccionVehicularAspectosGenerales extends Fragment {


    @BindView(R.id.toggleSwitchLicencia)
    ToggleSwitch toggleSwitchLicencia;
    @BindView(R.id.toggleSwitchCambioAceite)
    ToggleSwitch toggleSwitchCambioAceite;
    @BindView(R.id.toggleSwitchSincronizacion)
    ToggleSwitch toggleSwitchSincronizacion;
    @BindView(R.id.toggleSwitchAlineacionBalance)
    ToggleSwitch toggleSwitchAlineacionBalance;
    @BindView(R.id.toggleSwitchCambioLlantas)
    ToggleSwitch toggleSwitchCambioLlantas;
    @BindView(R.id.toggleSwitchDispositivoVelocidad)
    ToggleSwitch toggleSwitchDispositivoVelocidad;
    @BindView(R.id.toggleSwitchPito)
    ToggleSwitch toggleSwitchPito;
    @BindView(R.id.toggleSwitchCinturonesSeguridad)
    ToggleSwitch toggleSwitchCinturonesSeguridad;
    @BindView(R.id.toggleSwitchExtintoresMantenimiento)
    ToggleSwitch toggleSwitchExtintoresMantenimiento;
    @BindView(R.id.toggleSwitchBotiquin)
    ToggleSwitch toggleSwitchBotiquin;
    @BindView(R.id.toggleSwitchAvisoConduzco)
    ToggleSwitch toggleSwitchAvisoConduzco;
    @BindView(R.id.toggleSwitchDelanterosTraseros)
    ToggleSwitch toggleSwitchDelanterosTraseros;
    @BindView(R.id.toggleSwitchHCGTLC)
    ToggleSwitch toggleSwitchHCGTLC;
    @BindView(R.id.btnContinuarGenerales)
    AppCompatButton btnContinuarGenerales;
    @BindView(R.id.NestedScrollViewAspectosGenerales)
    NestedScrollView NestedScrollViewAspectosGenerales;

    public FragmentPreInspeccionVehicularAspectosGenerales() {
        // Required empty public constructor
    }


    public static FragmentPreInspeccionVehicularAspectosGenerales newInstance(String param1, String param2) {
        FragmentPreInspeccionVehicularAspectosGenerales fragment = new FragmentPreInspeccionVehicularAspectosGenerales();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preinspeccionvehicular_aspectos_generales, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}

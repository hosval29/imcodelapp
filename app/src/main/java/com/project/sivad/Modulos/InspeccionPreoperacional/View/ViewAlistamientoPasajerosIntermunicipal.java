package com.project.sivad.Modulos.InspeccionPreoperacional.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters.PagesAdapterFragmentsAlistamientoPasajeroFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosIntermunicipal.FragmentPasajerosIntermunicipalAspectosElectricos;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosIntermunicipal.FragmentPasajerosIntermunicipalAspectosGenerales;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosIntermunicipal.FragmentPasajerosIntermunicipalAspectosMecanicos;
import com.project.sivad.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ViewAlistamientoPasajerosIntermunicipal extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbarViewInspeccionPreoperacional)
    androidx.appcompat.widget.Toolbar toolbarViewInspeccionPreoperacional;
    @BindView(R.id.appBarLayouViewInspeccionPreoperacional)
    com.google.android.material.appbar.AppBarLayout appBarLayouViewInspeccionPreoperacional;
    @BindView(R.id.viewPagerViewInspeccionPreoperacional)
    androidx.viewpager.widget.ViewPager viewPagerViewInspeccionPreoperacional;
    @BindView(R.id.floatingActionButton)
    com.google.android.material.floatingactionbutton.FloatingActionButton floatingActionButton;
    @BindView(R.id.bottomNavigation)
    com.google.android.material.bottomnavigation.BottomNavigationView bottomNavigation;

    private PagesAdapterFragmentsAlistamientoPasajeroFurgones pagesAdapterFrgament;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_alistamiento_pasajeros_intermunicipal);
        ButterKnife.bind(this);

        //Inicializamos las variables tipo Object
        context = this;

        setupToolbar(toolbarViewInspeccionPreoperacional);

        setupViewPager(getSupportFragmentManager(), viewPagerViewInspeccionPreoperacional);
        viewPagerViewInspeccionPreoperacional.setCurrentItem(0);
        viewPagerViewInspeccionPreoperacional.setOnPageChangeListener(new PageFragmentChange());

        bottomNavigation.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewAlistamientoPasajerosIntermunicipal.this, ViewNovedades.class);
                startActivity(intent);
                //finish();
            }
        });
    }

    private void setupToolbar(Toolbar toolbarSetup) {
        setSupportActionBar(toolbarSetup);
    }

    private void setupViewPager(FragmentManager fragmentManager, ViewPager viewPagerSetup) {
        pagesAdapterFrgament = new PagesAdapterFragmentsAlistamientoPasajeroFurgones(fragmentManager);

        //Add All Fragment To List
        pagesAdapterFrgament.addFragment(new FragmentPasajerosIntermunicipalAspectosMecanicos(), getResources().getString(R.string.tab_label_aspectos_mecanicos));
        pagesAdapterFrgament.addFragment(new FragmentPasajerosIntermunicipalAspectosElectricos(), getResources().getString(R.string.tab_label_aspectos_electricos));
        pagesAdapterFrgament.addFragment(new FragmentPasajerosIntermunicipalAspectosGenerales(), getResources().getString(R.string.tab_label_aspectos_generales));

        viewPagerSetup.setAdapter(pagesAdapterFrgament);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.itemBottomNavigationAspectosMecanico:
                viewPagerViewInspeccionPreoperacional.setCurrentItem(0);
                return true;
            case R.id.itemBottomNavigationAspectosElectricos:
                viewPagerViewInspeccionPreoperacional.setCurrentItem(1);
                return true;
            case R.id.itemBottomNavigationAspectosGenerales:
                viewPagerViewInspeccionPreoperacional.setCurrentItem(2);
                return true;
        }
        return false;
    }

    public class PageFragmentChange implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }
        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosMecanico);
                    break;
                case 1:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosElectricos);
                    break;
                case 2:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosGenerales);
                    break;
            }
        }
        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }
}

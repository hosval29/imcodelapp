package com.project.sivad.Modulos.InspeccionPreoperacional.Model;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces.InterfacesInspeccionPreoperation;
import com.project.sivad.http.ApiAdapter;
import com.project.sivad.http.ApiAdapterClient;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModelInspectionPreoperational implements InterfacesInspeccionPreoperation.InterfacesModelInspectionPreoperational {

    private InterfacesInspeccionPreoperation.InterfacesPresenterInspectionPreoperational interfacesPresenterInspectionPreoperational;
    private JSONObject jsonBody = null;
    private ApiAdapterClient apiAdapter;

    public ModelInspectionPreoperational(InterfacesInspeccionPreoperation.InterfacesPresenterInspectionPreoperational interfacesPresenterInspectionPreoperational, Context context) {
        this.interfacesPresenterInspectionPreoperational = interfacesPresenterInspectionPreoperational;
        this.jsonBody = new JSONObject();
        this.apiAdapter = new ApiAdapterClient(context);
    }

    @Override
    public void postDataInspectionPreoperational(String dateRegister
            , int idUser
            , int idVehicle
            , JSONArray inspection
            , JSONObject jsonObjectDataNoveltiesInspection
            , int valueCheckBox
            , int inspectionEnabled) {

        try {

            jsonBody.put("dateRegister", dateRegister);
            jsonBody.put("idUser", idUser);
            jsonBody.put("idVehicle", idVehicle);
            jsonBody.put("inspection", inspection);
            jsonBody.put("novelties", jsonObjectDataNoveltiesInspection);
            jsonBody.put("valueCheckBox", valueCheckBox);
            jsonBody.put("inspectionEnabled", inspectionEnabled);

            //Declaramos un Parseo para quitar simbolos y espacios innecesarios
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonBody.toString());

            // creamos un Objeto Call que sea del Modelo EntradasResponse y le asignamos el metodo getEntrada de
            // apiAdapter
            Call<ResponseVehicleInspecion> call = apiAdapter.postDataVehicleInspection(gsonObject);
            call.enqueue(new ResponseCallback());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ResponseCallback implements Callback<ResponseVehicleInspecion> {

        @Override
        public void onResponse(Call<ResponseVehicleInspecion> call, Response<ResponseVehicleInspecion> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            if (response.code() == 200) {
                interfacesPresenterInspectionPreoperational.interfacesViewSuccessResponse(response.body());
            } else if (response.code() == 201) {
                interfacesPresenterInspectionPreoperational.interfacesViewSuccessResponse(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterInspectionPreoperational.interfacesViewErrorResponse(responseError1, jsonBody);
                    return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterInspectionPreoperational.interfacesViewErrorResponse(responseError, jsonBody);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterInspectionPreoperational.interfacesViewErrorResponse(responseError, jsonBody);
                }

            }
        }

        @Override
        public void onFailure(Call<ResponseVehicleInspecion> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Sin conexión al servidor o a internet, Pulsa (ok) para almacenar offline.");
            interfacesPresenterInspectionPreoperational.interfacesViewErrorResponse(responseError, jsonBody);
        }
    }

    @Override
    public void postDataAlertByItemBadToggle(int idUser
            , String nameUser
            , String lastNameUser
            , String plateVehicle
            , int idVehicle
            , String dateAlert
            , String timeRegister
            , String nameToggle
            , String nameFragment
            , String dateRegisterAlertInspectionItemBad) {

        try {
            jsonBody.put("idUser", idUser);
            jsonBody.put("nameUser", nameUser);
            jsonBody.put("lastNameUser", lastNameUser);
            jsonBody.put("plateVehicle", plateVehicle);
            jsonBody.put("idVehicle", idVehicle);
            jsonBody.put("dateAlert", dateAlert);
            jsonBody.put("timeRegister", timeRegister);
            jsonBody.put("nameToggle", nameToggle);
            jsonBody.put("nameFragment", nameFragment);
            jsonBody.put("dateRegister", dateRegisterAlertInspectionItemBad);

            //Declaramos un Parseo para quitar simbolos y espacios innecesarios
            JsonParser jsonParser = new JsonParser();
            JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonBody.toString());

            // creamos un Objeto Call que sea del Modelo EntradasResponse y le asignamos el metodo getEntrada de
            // apiAdapter
            Call<ResponseJson> call = apiAdapter.postDataAlertByItemBadToggle(gsonObject);
            call.enqueue(new ResponseCallbackPostDataAlertByitemBadToggle());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private class ResponseCallbackPostDataAlertByitemBadToggle implements Callback<ResponseJson> {
        @Override
        public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {
            Gson gson = new GsonBuilder().create();
            ResponseError responseError = null;
            if (response.code() == 200) {
                interfacesPresenterInspectionPreoperational.responseSuccesPostDataAlertByItemBadToggle(response.body());
            } else if (response.code() == 201) {
                interfacesPresenterInspectionPreoperational.responseSuccesPostDataAlertByItemBadToggle(response.body());
            } else {

                if (response.code() == 404 && response.message().equals("Not Found")) {
                    /*ResponseError responseError1 = new ResponseError();
                    responseError1.setErrorcode(404);
                    responseError1.setMessage("Problemas en la conexión con el servidor");
                    interfacesPresenterInspectionPreoperational.responseErrorPostDataAlertByItemBadToggle(responseError1, jsonBody);
                    return;*/
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterInspectionPreoperational.responseErrorPostDataAlertByItemBadToggle(responseError, jsonBody);
                } else {
                    try {
                        responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    interfacesPresenterInspectionPreoperational.responseErrorPostDataAlertByItemBadToggle(responseError, jsonBody);
                }

            }
        }

        @Override
        public void onFailure(Call<ResponseJson> call, Throwable t) {
            ResponseError responseError = new ResponseError();
            responseError.setErrorcode(500);
            responseError.setMessage("Sin conexión al servidor o a internet, Pulsa (ok) para almacenar offline.");
            interfacesPresenterInspectionPreoperational.responseErrorPostDataAlertByItemBadToggle(responseError, jsonBody);
        }
    }

    @Override
    public void postDataNoveltiesInspection(JSONObject jsonObjectDataNoveltiesInspection) {

        //Declaramos un Parseo para quitar simbolos y espacios innecesarios
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObjectDataNoveltiesInspection.toString());

        // creamos un Objeto Call que sea del Modelo EntradasResponse y le asignamos el metodo getEntrada de
        // apiAdapter
        Call<ResponseJson> call = apiAdapter.postDataNoveltiesInspection(gsonObject);
        call.enqueue(new Callback<ResponseJson>() {
            @Override
            public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {
                Gson gson = new GsonBuilder().create();
                ResponseError responseError = null;
                if (response.code() == 200) {
                    interfacesPresenterInspectionPreoperational.responseSuccesPostDataNoveltiesInspection(response.body());
                } else if (response.code() == 201) {
                    interfacesPresenterInspectionPreoperational.responseSuccesPostDataNoveltiesInspection(response.body());
                } else {

                    if (response.code() == 404 && response.message().equals("Not Found")) {
                        /*ResponseError responseError1 = new ResponseError();
                        responseError1.setErrorcode(404);
                        responseError1.setMessage("Problemas en la conexión con el servidor");
                        interfacesPresenterInspectionPreoperational.responseErrorPostDataNoveltiesInspection(responseError1, jsonObjectDataNoveltiesInspection);
                        return;*/
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterInspectionPreoperational.responseErrorPostDataNoveltiesInspection(responseError, jsonObjectDataNoveltiesInspection);
                    } else {
                        try {
                            responseError = gson.fromJson(response.errorBody().string(), ResponseError.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        interfacesPresenterInspectionPreoperational.responseErrorPostDataNoveltiesInspection(responseError, jsonObjectDataNoveltiesInspection);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseJson> call, Throwable t) {
                ResponseError responseError = new ResponseError();
                responseError.setErrorcode(500);
                responseError.setMessage("Sin conexión al servidor o a internet, Pulsa (ok) para almacenar offline.");
                interfacesPresenterInspectionPreoperational.responseErrorPostDataNoveltiesInspection(responseError, jsonObjectDataNoveltiesInspection);
            }
        });
    }

}

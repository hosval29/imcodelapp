package com.project.sivad.Modulos.InspeccionPreoperacional.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters.PagesAdapterFragmentsAlistamientoPasajeroFurgones;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosLujo.FragmentPasajerosLujoAspectosElectricos;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosLujo.FragmentPasajerosLujoAspectosMecanicos;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosLujo.FragmentPasajerosLujosAspectosGenerales;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.MenuItem;
import android.view.View;

import com.project.sivad.R;

public class ViewAlistamientosPasajerosLujo extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbarViewInspeccionPreoperacional)
    androidx.appcompat.widget.Toolbar toolbarViewInspeccionPreoperacional;
    @BindView(R.id.appBarLayouViewInspeccionPreoperacional)
    com.google.android.material.appbar.AppBarLayout appBarLayouViewInspeccionPreoperacional;
    @BindView(R.id.viewPagerViewInspeccionPreoperacional)
    androidx.viewpager.widget.ViewPager viewPagerViewInspeccionPreoperacional;
    @BindView(R.id.floatingActionButton)
    com.google.android.material.floatingactionbutton.FloatingActionButton floatingActionButton;
    @BindView(R.id.bottomNavigation)
    com.google.android.material.bottomnavigation.BottomNavigationView bottomNavigation;

    private PagesAdapterFragmentsAlistamientoPasajeroFurgones pagesAdapterFrgament;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_alistamientos_pasajeros_lujo);
        ButterKnife.bind(this);

        //Inicializamos las variables tipo Object
        context = this;

        setupToolbar(toolbarViewInspeccionPreoperacional);

        setupViewPager(getSupportFragmentManager(), viewPagerViewInspeccionPreoperacional);
        viewPagerViewInspeccionPreoperacional.setCurrentItem(0);
        viewPagerViewInspeccionPreoperacional.setOnPageChangeListener(new PageFragmentChange());

        bottomNavigation.setOnNavigationItemSelectedListener(this::onNavigationItemSelected);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewAlistamientosPasajerosLujo.this, ViewNovedades.class);
                startActivity(intent);
                //finish();
            }
        });
    }

    private void setupToolbar(Toolbar toolbarSetup) {
        setSupportActionBar(toolbarSetup);
    }

    private void setupViewPager(FragmentManager fragmentManager, ViewPager viewPagerSetup) {
        pagesAdapterFrgament = new PagesAdapterFragmentsAlistamientoPasajeroFurgones(fragmentManager);

        //Add All Fragment To List
        pagesAdapterFrgament.addFragment(new FragmentPasajerosLujoAspectosMecanicos(), getResources().getString(R.string.tab_label_aspectos_mecanicos));
        pagesAdapterFrgament.addFragment(new FragmentPasajerosLujoAspectosElectricos(), getResources().getString(R.string.tab_label_aspectos_electricos));
        pagesAdapterFrgament.addFragment(new FragmentPasajerosLujosAspectosGenerales(), getResources().getString(R.string.tab_label_aspectos_generales));

        viewPagerSetup.setAdapter(pagesAdapterFrgament);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.itemBottomNavigationAspectosMecanico:
                viewPagerViewInspeccionPreoperacional.setCurrentItem(0);
                return true;
            case R.id.itemBottomNavigationAspectosElectricos:
                viewPagerViewInspeccionPreoperacional.setCurrentItem(1);
                return true;
            case R.id.itemBottomNavigationAspectosGenerales:
                viewPagerViewInspeccionPreoperacional.setCurrentItem(2);
                return true;
        }
        return false;
    }

    public class PageFragmentChange implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }
        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosMecanico);
                    break;
                case 1:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosElectricos);
                    break;
                case 2:
                    bottomNavigation.setSelectedItemId(R.id.itemBottomNavigationAspectosGenerales);
                    break;
            }
        }
        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

}

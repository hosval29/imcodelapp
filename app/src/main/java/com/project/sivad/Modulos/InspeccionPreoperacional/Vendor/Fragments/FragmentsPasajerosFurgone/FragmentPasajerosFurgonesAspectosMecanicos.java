package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosFurgone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentPasajerosFurgonesAspectosMecanicos extends Fragment {

    @BindView(R.id.toggleSwitchFugas)
    ToggleSwitch toggleSwitchFugas;
    @BindView(R.id.toggleSwitchNivelesAceite)
    ToggleSwitch toggleSwitchNivelesAceite;
    @BindView(R.id.toggleSwitchTensionCorreas)
    ToggleSwitch toggleSwitchTensionCorreas;
    @BindView(R.id.toggleSwitchFiltroAceite)
    ToggleSwitch toggleSwitchFiltroAceite;
    @BindView(R.id.toggleSwitchReupuestoFiltroCombustible)
    ToggleSwitch toggleSwitchReupuestoFiltroCombustible;
    @BindView(R.id.toggleSwitchTapasFluido)
    ToggleSwitch toggleSwitchTapasFluido;
    @BindView(R.id.toggleSwitchCapacidadAjusteBornes)
    ToggleSwitch toggleSwitchCapacidadAjusteBornes;
    @BindView(R.id.toggleSwitchJuegoLibreFuncionamiento)
    ToggleSwitch toggleSwitchJuegoLibreFuncionamiento;
    @BindView(R.id.toggleSwitchLiquidoFrenosEmbrague)
    ToggleSwitch toggleSwitchLiquidoFrenosEmbrague;
    @BindView(R.id.toggleSwitchPresionAireDesgasteMinimo)
    ToggleSwitch toggleSwitchPresionAireDesgasteMinimo;
    @BindView(R.id.toggleSwitchDelanterasTraserasRepuesto)
    ToggleSwitch toggleSwitchDelanterasTraserasRepuesto;
    @BindView(R.id.toggleSwitchFugasRetenedoresRetenedorCupling)
    ToggleSwitch toggleSwitchFugasRetenedoresRetenedorCupling;
    @BindView(R.id.toggleSwitchAbtraccionFisuras)
    ToggleSwitch toggleSwitchAbtraccionFisuras;
    @BindView(R.id.toggleSwitchNivelRefrigerante)
    ToggleSwitch toggleSwitchNivelRefrigerante;
    @BindView(R.id.toggleSwitchDireccionJuegoLibreFuncionamiento)
    ToggleSwitch toggleSwitchDireccionJuegoLibreFuncionamiento;
    @BindView(R.id.toggleSwitchFugasDireccionEmbrague)
    ToggleSwitch toggleSwitchFugasDireccionEmbrague;
    @BindView(R.id.btnContinuarMecanico)
    AppCompatButton btnContinuarMecanico;

    public FragmentPasajerosFurgonesAspectosMecanicos() {
        // Required empty public constructor
    }

    public static FragmentPasajerosFurgonesAspectosMecanicos newInstance(String param1, String param2) {
        FragmentPasajerosFurgonesAspectosMecanicos fragment = new FragmentPasajerosFurgonesAspectosMecanicos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pasajeros_furgones_aspectos_mecanicos, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


}

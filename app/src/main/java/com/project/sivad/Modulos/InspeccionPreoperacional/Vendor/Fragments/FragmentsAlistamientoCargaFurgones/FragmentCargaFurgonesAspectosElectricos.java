package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsAlistamientoCargaFurgones;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentCargaFurgonesAspectosElectricos extends Fragment {

    @BindView(R.id.toggleSwitchABSSRP)
    ToggleSwitch toggleSwitchABSSRP;//1
    @BindView(R.id.toggleSwitchDelanterasTraseras)
    ToggleSwitch toggleSwitchDelanterasTraseras;//2
    @BindView(R.id.toggleSwitchDerIzqAtrasPlumillas)
    ToggleSwitch toggleSwitchDerIzqAtrasPlumillas;//3
    @BindView(R.id.toggleSwitchAspersores)
    ToggleSwitch toggleSwitchAspersores;//4
    @BindView(R.id.toggleSwitchNivelAgua)
    ToggleSwitch toggleSwitchNivelAgua;//5
    @BindView(R.id.toggleSwitchLateralesDerIzqRetrovior)
    ToggleSwitch toggleSwitchLateralesDerIzqRetrovior;//6
    @BindView(R.id.toggleSwitchPito)
    ToggleSwitch toggleSwitchPito;//7
    @BindView(R.id.toggleSwitchIndicadoresEInstrumentos)
    ToggleSwitch toggleSwitchIndicadoresEInstrumentos;//8
    @BindView(R.id.toggleSwitchInspeccionCableadoElectrico)
    ToggleSwitch toggleSwitchInspeccionCableadoElectrico;//9
    @BindView(R.id.btnContinuarElectrico)
    AppCompatButton btnContinuarElectrico;
    @BindView(R.id.idAppCompatTextviewErrorABSSRP)
    AppCompatTextView idAppCompatTextviewErrorABSSRP;
    @BindView(R.id.idAppCompatTextviewErrorDelanterasTraseras)
    AppCompatTextView idAppCompatTextviewErrorDelanterasTraseras;
    @BindView(R.id.idAppCompatTextviewErrorDerIzqAtrasPlumillas)
    AppCompatTextView idAppCompatTextviewErrorDerIzqAtrasPlumillas;
    @BindView(R.id.idAppCompatTextviewErrorAspersores)
    AppCompatTextView idAppCompatTextviewErrorAspersores;
    @BindView(R.id.idAppCompatTextviewErrorNivelAgua)
    AppCompatTextView idAppCompatTextviewErrorNivelAgua;
    @BindView(R.id.idAppCompatTextviewErrorLateralesDerIzqRetrovior)
    AppCompatTextView idAppCompatTextviewErrorLateralesDerIzqRetrovior;
    @BindView(R.id.idAppCompatTextviewErrorPito)
    AppCompatTextView idAppCompatTextviewErrorPito;
    @BindView(R.id.idAppCompatTextviewErrorIndicadoresEInstrumentos)
    AppCompatTextView idAppCompatTextviewErrorIndicadoresEInstrumentos;
    @BindView(R.id.idAppCompatTextviewErrorInspeccionCableadoElectrico)
    AppCompatTextView idAppCompatTextviewErrorInspeccionCableadoElectrico;

    @BindView(R.id.idAppCompatTextviewTitleABSSRP)
    AppCompatTextView idAppCompatTextviewTitleABSSRP;//1
    @BindView(R.id.idAppCompatTextviewTitleDelanterasTraseras)
    AppCompatTextView idAppCompatTextviewTitleDelanterasTraseras;//2
    @BindView(R.id.idAppCompatTextviewTitleDerIzqAtrasPlumillas)
    AppCompatTextView idAppCompatTextviewTitleDerIzqAtrasPlumillas;//3
    @BindView(R.id.idAppCompatTextviewTitleAspersores)
    AppCompatTextView idAppCompatTextviewTitleAspersores;//4
    @BindView(R.id.idAppCompatTextviewTitleNivelAgua)
    AppCompatTextView idAppCompatTextviewTitleNivelAgua;//5
    @BindView(R.id.idAppCompatTextviewTitleLateralesDerIzqRetrovior)
    AppCompatTextView idAppCompatTextviewTitleLateralesDerIzqRetrovior;//6
    @BindView(R.id.idAppCompatTextviewTitlePito)
    AppCompatTextView idAppCompatTextviewTitlePito;//7
    @BindView(R.id.idAppCompatTextviewTitleIndicadoresEInstrumentos)
    AppCompatTextView idAppCompatTextviewTitleIndicadoresEInstrumentos;//8
    @BindView(R.id.idAppCompatTextviewTitleInspeccionCableadoElectrico)
    AppCompatTextView idAppCompatTextviewTitleInspeccionCableadoElectrico;//9

    //variables de validacion
    private boolean isABSSRP = false;
    private boolean isDelanterasTraseras = false;
    private boolean isDerIzqAtrasPlumillas = false;
    private boolean isAspersores = false;
    private boolean isNivelAgua = false;
    private boolean isLateralesDerIzqRetrovior = false;
    private boolean isPito = false;
    private boolean isIndicadoresEInstrumentos = false;
    private boolean isInspeccionCableadoElectrico = false;
    private boolean isToggleBad = false;

    //Variables valores de los toggle (2 - Malo || 1 - Regular || 0- Bueno)
    private String valueABSSRP = "";
    private String valueDelanterasTraseras = "";
    private String valueDerIzqAtrasPlumillas = "";
    private String valueAspersores = "";
    private String valueNivelAgua = "";
    private String valueLateralesDerIzqRetrovior = "";
    private String valuePito = "";
    private String valueIndicadoresEInstrumentos = "";
    private String valueInspeccionCableadoElectrico = "";
    private final String nameFragment = ConstantsStrings.NAME_FRAGMENT_ASPECTOS_ELECTRICOS;

    //Interfaces
    InterfacesFragmentCFAE interfacesFragmentCFAE;

    public interface InterfacesFragmentCFAE {
        void sendDataAspectosElectricos(JSONArray jsonArray, int idInspection);

        void registerNoveltiesInspectionAspectosElectricos(boolean isToggleBad, String nameToggle, String nameFragment);
    }

    //Variables de Utilidades
    private Context context;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Variables Arguments
    private int idInspection = 0;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            interfacesFragmentCFAE = (InterfacesFragmentCFAE) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    public FragmentCargaFurgonesAspectosElectricos() {
        // Required empty public constructor
    }

    public static FragmentCargaFurgonesAspectosElectricos newInstance() {
        FragmentCargaFurgonesAspectosElectricos fragment = new FragmentCargaFurgonesAspectosElectricos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        context = getContext();
        dbSivad = new DBSivad(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carga_furgones_aspectos_electricos, container, false);
        ButterKnife.bind(this, view);

        toggleSwitchABSSRP.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isABSSRP = true;
                valueABSSRP = String.valueOf(i);

                if (isABSSRP) {
                    idAppCompatTextviewErrorABSSRP.setVisibility(View.GONE);
                }

                if (i == 2) {

                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleABSSRP.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchDelanterasTraseras.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isDelanterasTraseras = true;
                valueDelanterasTraseras = String.valueOf(i);

                if (isDelanterasTraseras) {
                    idAppCompatTextviewErrorDelanterasTraseras.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleDelanterasTraseras.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchDerIzqAtrasPlumillas.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isDerIzqAtrasPlumillas = true;
                valueDerIzqAtrasPlumillas = String.valueOf(i);

                if (isDerIzqAtrasPlumillas) {
                    idAppCompatTextviewErrorDerIzqAtrasPlumillas.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleDerIzqAtrasPlumillas.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchAspersores.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isAspersores = true;
                valueAspersores = String.valueOf(i);

                if (isAspersores) {
                    idAppCompatTextviewErrorAspersores.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleAspersores.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchNivelAgua.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isNivelAgua = true;
                valueNivelAgua = String.valueOf(i);

                if (isNivelAgua) {
                    idAppCompatTextviewErrorNivelAgua.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleNivelAgua.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchLateralesDerIzqRetrovior.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isLateralesDerIzqRetrovior = true;
                valueLateralesDerIzqRetrovior = String.valueOf(i);

                if (isLateralesDerIzqRetrovior) {
                    idAppCompatTextviewErrorLateralesDerIzqRetrovior.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleLateralesDerIzqRetrovior.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchPito.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isPito = true;
                valuePito = String.valueOf(i);

                if (isPito) {
                    idAppCompatTextviewErrorPito.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitlePito.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchIndicadoresEInstrumentos.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isIndicadoresEInstrumentos = true;
                valueIndicadoresEInstrumentos = String.valueOf(i);

                if (isIndicadoresEInstrumentos) {
                    idAppCompatTextviewErrorIndicadoresEInstrumentos.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleIndicadoresEInstrumentos.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchInspeccionCableadoElectrico.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isInspeccionCableadoElectrico = true;
                valueIndicadoresEInstrumentos = String.valueOf(i);

                if (isInspeccionCableadoElectrico) {
                    idAppCompatTextviewErrorInspeccionCableadoElectrico.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleInspeccionCableadoElectrico.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        return view;

    }

    private void showDialogSelectionToggleBad(boolean isToggleBad, String nameToggle, String nameFragment) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Desea registrar la novedad de la opcion que seleccionó?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        interfacesFragmentCFAE.registerNoveltiesInspectionAspectosElectricos(isToggleBad, nameToggle, nameFragment);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Registrar Novedad.");
        dialog.show();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.v("Estoy aqui", "onActivityCreated");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.v("Estoy aqui", "onViewStateRestored");
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.v("Estoy aqui", "onResume");
    }

    public void btnContinuarElectrico(View view) {
        if (!validatesInputs()) {
            JSONArray jsonArrayDataAspectosElectricos = new JSONArray();

            JSONObject jsonObjectLuces = new JSONObject();
            JSONObject jsonObjectDirrecionales = new JSONObject();
            JSONObject jsonObjectLimpiaBrisas = new JSONObject();
            JSONObject jsonObjectEspejos = new JSONObject();
            JSONObject jsonObjectAccesoriosImportantes = new JSONObject();

            JSONObject jsonObjectDataLuces = new JSONObject();
            JSONObject jsonObjectDataDirrecionales = new JSONObject();
            JSONObject jsonObjectDataLimpiaBrisas = new JSONObject();
            JSONObject jsonObjectDataEspejos = new JSONObject();
            JSONObject jsonObjectDataAccesoriosImportantes = new JSONObject();

            try {
                jsonObjectDataLuces.put("ALTAS - BAJAS - CABINA - LUZ FURGON", valueABSSRP);
                jsonObjectLuces.put("LUCES", jsonObjectDataLuces);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataDirrecionales.put("DELANTERAS - TRASERAS - ESTACIONARIAS", valueDelanterasTraseras);
                jsonObjectDirrecionales.put("DIRECCIONALES", jsonObjectDataDirrecionales);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataLimpiaBrisas.put("DER/IZQ/ATRÁS", valueDerIzqAtrasPlumillas);
                jsonObjectDataLimpiaBrisas.put("ASPERSORES", valueAspersores);
                jsonObjectDataLimpiaBrisas.put("NIVEL DE AGUA", valueNivelAgua);
                jsonObjectLimpiaBrisas.put("LIMPIABRISAS", jsonObjectDataLimpiaBrisas);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataEspejos.put("LATERALES DER/IZQ - RETROVISOR", valueLateralesDerIzqRetrovior);
                jsonObjectEspejos.put("ESPEJO", jsonObjectDataEspejos);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataAccesoriosImportantes.put("PITO", valuePito);
                jsonObjectDataAccesoriosImportantes.put("INDICADORES DE TABLERO", valueIndicadoresEInstrumentos);
                jsonObjectDataAccesoriosImportantes.put("INSPECCION CABLEADO ELECTRICO", valueInspeccionCableadoElectrico);
                jsonObjectAccesoriosImportantes.put("ACCESORIOS IMPORTANTES", jsonObjectDataAccesoriosImportantes);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonArrayDataAspectosElectricos.put(jsonObjectLuces);
            jsonArrayDataAspectosElectricos.put(jsonObjectDirrecionales);
            jsonArrayDataAspectosElectricos.put(jsonObjectEspejos);
            jsonArrayDataAspectosElectricos.put(jsonObjectLimpiaBrisas);
            jsonArrayDataAspectosElectricos.put(jsonObjectAccesoriosImportantes);

            idInspection = dbSivad.getLastId();

            interfacesFragmentCFAE.sendDataAspectosElectricos(jsonArrayDataAspectosElectricos, idInspection);
        }
    }

    private boolean validatesInputs() {
        boolean error = false;

        if (!isABSSRP) {
            idAppCompatTextviewErrorABSSRP.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isAspersores) {
            idAppCompatTextviewErrorAspersores.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isDelanterasTraseras) {
            idAppCompatTextviewErrorDelanterasTraseras.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isDerIzqAtrasPlumillas) {
            idAppCompatTextviewErrorDerIzqAtrasPlumillas.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isIndicadoresEInstrumentos) {
            idAppCompatTextviewErrorIndicadoresEInstrumentos.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isInspeccionCableadoElectrico) {
            idAppCompatTextviewErrorInspeccionCableadoElectrico.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isLateralesDerIzqRetrovior) {
            idAppCompatTextviewErrorLateralesDerIzqRetrovior.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isNivelAgua) {
            idAppCompatTextviewErrorNivelAgua.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isPito) {
            idAppCompatTextviewErrorPito.setVisibility(View.VISIBLE);
            error = true;
        }

        return error;
    }
}

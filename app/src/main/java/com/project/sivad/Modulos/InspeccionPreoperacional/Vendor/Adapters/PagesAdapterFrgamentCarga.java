package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters;

import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsViewInspeccionPreoperacionalCarga.FragmentAspectosElectricosCarga;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsViewInspeccionPreoperacionalCarga.FragmentAspectosGeneralesCarga;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsViewInspeccionPreoperacionalCarga.FragmentAspectosMecanicosCarga;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class PagesAdapterFrgamentCarga extends FragmentPagerAdapter {
    private static int numItemFragments = 3;

    public PagesAdapterFrgamentCarga(FragmentManager fm ) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                FragmentAspectosGeneralesCarga fragmentAspectosGenerales = new FragmentAspectosGeneralesCarga();
                return fragmentAspectosGenerales;
            case 1:
                FragmentAspectosMecanicosCarga fragmentAspectosMecanicos = new FragmentAspectosMecanicosCarga();
                return fragmentAspectosMecanicos;
            case 2:
                FragmentAspectosElectricosCarga fragmentAspectosElectricos = new FragmentAspectosElectricosCarga();
                return fragmentAspectosElectricos;
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return numItemFragments;
    }

}

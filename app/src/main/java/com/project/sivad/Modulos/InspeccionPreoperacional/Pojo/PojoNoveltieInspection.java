package com.project.sivad.Modulos.InspeccionPreoperacional.Pojo;

public class PojoNoveltieInspection {
    String descriptionNoveltie;
    String observationNoveltie;
    String dateNoveltie;
    String timeNoveltie;
    String typeNoveltie;
    String nameFragment;
    String nameToggle;

    public PojoNoveltieInspection(){}

    public PojoNoveltieInspection(String dateNoveltie, String descriptionNoveltie, String observationNoveltie, String timeNoveltie, String typeNoveltie, String nameFragment, String nameToggle) {
        this.descriptionNoveltie = descriptionNoveltie;
        this.observationNoveltie = observationNoveltie;
        this.dateNoveltie = dateNoveltie;
        this.timeNoveltie = timeNoveltie;
        this.typeNoveltie = typeNoveltie;
        this.nameFragment = nameFragment;
        this.nameToggle = nameToggle;
    }

    public String getNameFragment() {
        return nameFragment;
    }

    public void setNameFragment(String nameFragment) {
        this.nameFragment = nameFragment;
    }

    public String getNameToggle() {
        return nameToggle;
    }

    public void setNameToggle(String nameToggle) {
        this.nameToggle = nameToggle;
    }

    public String getObservationNoveltie() {
        return observationNoveltie;
    }

    public void setObservationNoveltie(String observationNoveltie) {
        this.observationNoveltie = observationNoveltie;
    }

    public String getTypeNoveltie() {
        return typeNoveltie;
    }

    public void setTypeNoveltie(String typeNoveltie) {
        this.typeNoveltie = typeNoveltie;
    }

    public String getDescriptionNoveltie() {
        return descriptionNoveltie;
    }

    public void setDescriptionNoveltie(String descriptionNoveltie) {
        this.descriptionNoveltie = descriptionNoveltie;
    }

    public String getObservation() {
        return observationNoveltie;
    }

    public void setObservation(String observation) {
        this.observationNoveltie = observation;
    }

    public String getDateNoveltie() {
        return dateNoveltie;
    }

    public void setDateNoveltie(String dateNoveltie) {
        this.dateNoveltie = dateNoveltie;
    }

    public String getTimeNoveltie() {
        return timeNoveltie;
    }

    public void setTimeNoveltie(String timeNoveltie) {
        this.timeNoveltie = timeNoveltie;
    }

    @Override
    public String toString() {
        return "PojoNoveltieInspection{" +
                "descriptionNoveltie='" + descriptionNoveltie + '\'' +
                ", observationNoveltie='" + observationNoveltie + '\'' +
                ", dateNoveltie='" + dateNoveltie + '\'' +
                ", timeNoveltie='" + timeNoveltie + '\'' +
                ", typeNoveltie='" + typeNoveltie + '\'' +
                ", nameFragment='" + nameFragment + '\'' +
                ", nameToggle='" + nameToggle + '\'' +
                '}';
    }
}

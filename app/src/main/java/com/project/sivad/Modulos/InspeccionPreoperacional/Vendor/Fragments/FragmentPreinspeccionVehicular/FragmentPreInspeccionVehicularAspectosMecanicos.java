package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentPreinspeccionVehicular;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPreInspeccionVehicularAspectosMecanicos extends Fragment {


    @BindView(R.id.toggleSwitchFugas)
    ToggleSwitch toggleSwitchFugas;
    @BindView(R.id.toggleSwitchNivelesAceite)
    ToggleSwitch toggleSwitchNivelesAceite;
    @BindView(R.id.toggleSwitchTensionCorreas)
    ToggleSwitch toggleSwitchTensionCorreas;
    @BindView(R.id.toggleSwitchFiltroAceite)
    ToggleSwitch toggleSwitchFiltroAceite;
    @BindView(R.id.toggleSwitchTapasFluido)
    ToggleSwitch toggleSwitchTapasFluido;
    @BindView(R.id.toggleSwitchNivelesElectrolitos)
    ToggleSwitch toggleSwitchNivelesElectrolitos;
    @BindView(R.id.toggleSwitchAjustesBornes)
    ToggleSwitch toggleSwitchAjustesBornes;
    @BindView(R.id.toggleSwitchLicencia)
    ToggleSwitch toggleSwitchLicencia;
    @BindView(R.id.toggleSwitchFrenosPrincipal)
    ToggleSwitch toggleSwitchFrenosPrincipal;
    @BindView(R.id.toggleSwitchEmergencias)
    ToggleSwitch toggleSwitchEmergencias;
    @BindView(R.id.toggleSwitchLimiteFrenos)
    ToggleSwitch toggleSwitchLimiteFrenos;
    @BindView(R.id.toggleSwitchRCC)
    ToggleSwitch toggleSwitchRCC;
    @BindView(R.id.toggleSwitchRCE)
    ToggleSwitch toggleSwitchRCE;
    @BindView(R.id.toggleSwitchLucesDelanteras)
    ToggleSwitch toggleSwitchLucesDelanteras;
    @BindView(R.id.toggleSwitchLucesTraseras)
    ToggleSwitch toggleSwitchLucesTraseras;
    @BindView(R.id.toggleSwitchRepuestos)
    ToggleSwitch toggleSwitchRepuestos;
    @BindView(R.id.toggleSwitchTransmisionFugas)
    ToggleSwitch toggleSwitchTransmisionFugas;
    @BindView(R.id.toggleSwitchRetenedorEjes)
    ToggleSwitch toggleSwitchRetenedorEjes;
    @BindView(R.id.toggleSwitchRetenodorCupling)
    ToggleSwitch toggleSwitchRetenodorCupling;
    @BindView(R.id.toggleSwitchObstrucciones)
    ToggleSwitch toggleSwitchObstrucciones;
    @BindView(R.id.toggleSwitchFisuras)
    ToggleSwitch toggleSwitchFisuras;
    @BindView(R.id.toggleSwitchNovelRefigerante)
    ToggleSwitch toggleSwitchNovelRefigerante;
    @BindView(R.id.btnContinuarMecanico)
    AppCompatButton btnContinuarMecanico;

    public FragmentPreInspeccionVehicularAspectosMecanicos() {
        // Required empty public constructor
    }

    public static FragmentPreInspeccionVehicularAspectosMecanicos newInstance(String param1, String param2) {
        FragmentPreInspeccionVehicularAspectosMecanicos fragment = new FragmentPreInspeccionVehicularAspectosMecanicos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preinspeccionvehicular_aspectos_mecanicos, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}

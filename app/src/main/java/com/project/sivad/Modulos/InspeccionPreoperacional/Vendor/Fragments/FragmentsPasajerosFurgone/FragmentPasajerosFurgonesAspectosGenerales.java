package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosFurgone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPasajerosFurgonesAspectosGenerales extends Fragment {


    @BindView(R.id.toggleSwitchLSRTRRTPT)
    ToggleSwitch toggleSwitchLSRTRRTPT;
    @BindView(R.id.toggleSwitchCambioAceite)
    ToggleSwitch toggleSwitchCambioAceite;
    @BindView(R.id.toggleSwitchSincronizacion)
    ToggleSwitch toggleSwitchSincronizacion;
    @BindView(R.id.toggleSwitchAlineacionBalanceo)
    ToggleSwitch toggleSwitchAlineacionBalanceo;
    @BindView(R.id.toggleSwitchCambioLlantas)
    ToggleSwitch toggleSwitchCambioLlantas;
    @BindView(R.id.toggleSwitchDispositivoVelocidad)
    ToggleSwitch toggleSwitchDispositivoVelocidad;
    @BindView(R.id.toggleSwitchPito)
    ToggleSwitch toggleSwitchPito;
    @BindView(R.id.toggleSwitchCinturonesSeguridad)
    ToggleSwitch toggleSwitchCinturonesSeguridad;
    @BindView(R.id.toggleSwitchCargaExtintores)
    ToggleSwitch toggleSwitchCargaExtintores;
    @BindView(R.id.toggleSwitchBotiquin)
    ToggleSwitch toggleSwitchBotiquin;
    @BindView(R.id.toggleSwitchVidrioPanoramico)
    ToggleSwitch toggleSwitchVidrioPanoramico;
    @BindView(R.id.toggleSwitchIdentidadCorporativa)
    ToggleSwitch toggleSwitchIdentidadCorporativa;
    @BindView(R.id.toggleSwitchAromaInteriorAseoGeneralVehiculo)
    ToggleSwitch toggleSwitchAromaInteriorAseoGeneralVehiculo;
    @BindView(R.id.toggleSwitchAvisoComoConduzo)
    ToggleSwitch toggleSwitchAvisoComoConduzo;
    @BindView(R.id.toggleSwitchKitAntiderrame)
    ToggleSwitch toggleSwitchKitAntiderrame;
    @BindView(R.id.toggleSwitchCierraPuertaFurgon)
    ToggleSwitch toggleSwitchCierraPuertaFurgon;
    @BindView(R.id.toggleSwitchCobijasTablas)
    ToggleSwitch toggleSwitchCobijasTablas;
    @BindView(R.id.toggleSwitchSogas)
    ToggleSwitch toggleSwitchSogas;
    @BindView(R.id.toggleSwitchTechoFurgon)
    ToggleSwitch toggleSwitchTechoFurgon;
    @BindView(R.id.toggleSwitchHCGTLC)
    ToggleSwitch toggleSwitchHCGTLC;
    @BindView(R.id.toggleSwitchNotaOperarVehiculo)
    ToggleSwitch toggleSwitchNotaOperarVehiculo;
    @BindView(R.id.appCompatCheckBoxFirma)
    AppCompatCheckBox appCompatCheckBoxFirma;
    @BindView(R.id.btnFinalizarInspeccion)
    AppCompatButton btnFinalizarInspeccion;

    public FragmentPasajerosFurgonesAspectosGenerales() {
        // Required empty public constructor
    }

    public static FragmentPasajerosFurgonesAspectosGenerales newInstance(String param1, String param2) {
        FragmentPasajerosFurgonesAspectosGenerales fragment = new FragmentPasajerosFurgonesAspectosGenerales();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pasajeros_furgones_aspectos_generales, container, false);
        ButterKnife.bind(this, view);
        toggleSwitchLSRTRRTPT.setCheckedPosition(0);
        toggleSwitchCambioAceite.setCheckedPosition(0);
        toggleSwitchSincronizacion.setCheckedPosition(0);
        toggleSwitchAlineacionBalanceo.setCheckedPosition(0);
        toggleSwitchCambioLlantas.setCheckedPosition(0);
        return view;
    }


}

package com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces;

import com.google.gson.JsonObject;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONObject;

public interface InterfacesInspeccionPreoperation {

    interface InterfacesViewInspeccionPreoperational {
        void interfacesViewSuccessResponse(ResponseVehicleInspecion responseVehicleInspecion);

        void interfacesViewErrorResponse(ResponseError responseError, JSONObject jsonObject);

        void responseErrorPostDataAlertByItemBadToggle(ResponseError responseError, JSONObject jsonObject);

        void responseSuccesPostDataAlertByItemBadToggle(ResponseJson responseJson);
    }

    interface InterfacesFragmentNoveltie {
        void responseErrorPostDataNoveltiesInspection(ResponseError responseError, JSONObject jsonObject);

        void responseSuccesPostDataNoveltiesInspection(ResponseJson responseJson);
    }

    interface InterfacesPresenterInspectionPreoperational {
        void interfacesViewSuccessResponse(ResponseVehicleInspecion responseVehicleInspecion);

        void interfacesViewErrorResponse(ResponseError responseError, JSONObject jsonObject);

        void postDataInspectionPreoperational(String dateRegister
                , int idUser
                , int idVehicle
                , JSONArray inspection
                , JSONObject jsonArrayDataNoveltiesInspection
                , int valueCheckBox
                , int inspectionEnabled);

        void responseErrorPostDataAlertByItemBadToggle(ResponseError responseError, JSONObject jsonObject);

        void responseSuccesPostDataAlertByItemBadToggle(ResponseJson responseJson);

        void postDataAlertByItemBadToggle(int idUser
                , String nameUser
                , String lastNameUser
                , String plateVehicle
                , int idVehicle
                , String dateAlert
                , String timeRegister
                , String nameToggle
                , String nameFragment
                , String dateRegisterAlertInspectionItemBad);

        void postDataNoveltiesInspection(JSONObject jsonObjectDataNoveltiesInspection);

        void responseErrorPostDataNoveltiesInspection(ResponseError responseError, JSONObject jsonObject);

        void responseSuccesPostDataNoveltiesInspection(ResponseJson responseJson);
    }

    interface InterfacesModelInspectionPreoperational {

        void postDataInspectionPreoperational(String dateRegister
                , int idUser
                , int idVehicle
                , JSONArray inspection
                , JSONObject jsonArrayDataNoveltiesInspection
                , int valueCheckBox
                , int inspectionEnabled);

        void postDataAlertByItemBadToggle(int idUser
                , String nameUser
                , String lastNameUser
                , String plateVehicle
                , int idVehicle
                , String dateAlert
                , String timeRegister
                , String nameToggle
                , String nameFragment
                , String dateRegisterAlertInspectionItemBad);

        void postDataNoveltiesInspection(JSONObject jsonObjectDataNoveltiesInspection);
    }

    interface InterfacesAdapterNoveltie {
        void removeItemNoveltie(int index);
    }
}

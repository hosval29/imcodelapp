package com.project.sivad.Modulos.InspeccionPreoperacional.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters.PagesAdapterFrgamentCarga;
import com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentEvualiacionesFisicas;
import com.project.sivad.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

public class ViewAlistamientoCarga extends AppCompatActivity {

    //Declaramos las variables XML
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    private FloatingActionButton btnNovedades;

    private PagesAdapterFrgamentCarga pagesAdapterFrgament;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_alistamiento_carga);

        //Inicializamos las referencias XML
        toolbar = (Toolbar) findViewById(R.id.idToolbarViewAlistamientoCarga);
        viewPager = (ViewPager) findViewById(R.id.idViewPagerViewAlistamiento);
        tabLayout = (TabLayout) findViewById(R.id.idTabLayoutAlistamientoCarga);
        btnNovedades = (FloatingActionButton) findViewById(R.id.idBtnAddNovedades);

        //Inicializamos las variables tipo Object
        context = this;

        setupToolbar(toolbar);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText(R.string.tab_label_aspectos_generales).setIcon(R.drawable.icon_tab_aspectos_generales);
        tabLayout.getTabAt(1).setText(R.string.tab_label_aspectos_mecanicos).setIcon(R.drawable.icon_tab_aspectos_mecanicos);
        tabLayout.getTabAt(2).setText(R.string.tab_label_aspectos_electricos).setIcon(R.drawable.icon_tab_aspectos_electicos);
        tabLayout.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        btnNovedades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewAlistamientoCarga.this, ViewNovedades.class);
                startActivity(intent);
                finish();
            }
        });

        //loadInfoTomaMuestraLibre();
    }

    private void setupToolbar(Toolbar toolbarSetup) {
        setSupportActionBar(toolbarSetup);
    }

    private void setupViewPager(ViewPager viewPagerSetup) {
        pagesAdapterFrgament = new PagesAdapterFrgamentCarga(getSupportFragmentManager());
        viewPagerSetup.setAdapter(pagesAdapterFrgament);
    }

    private void loadInfoTomaMuestraLibre() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFrag;
        dialogFrag = new FragmentEvualiacionesFisicas();
        dialogFrag.setCancelable(false);
        dialogFrag.show(ft, "dialog");
    }
}

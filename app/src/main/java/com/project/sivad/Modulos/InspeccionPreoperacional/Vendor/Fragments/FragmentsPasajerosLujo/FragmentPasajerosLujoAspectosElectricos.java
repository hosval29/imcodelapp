package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsPasajerosLujo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.sivad.R;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentPasajerosLujoAspectosElectricos extends Fragment {

    @BindView(R.id.toggleSwitchABSSRP)
    ToggleSwitch toggleSwitchABSSRP;
    @BindView(R.id.toggleSwitchDelanterasTraseras)
    ToggleSwitch toggleSwitchDelanterasTraseras;
    @BindView(R.id.toggleSwitchDerIzqAtrasPlumillas)
    ToggleSwitch toggleSwitchDerIzqAtrasPlumillas;
    @BindView(R.id.toggleSwitchNivelAgua)
    ToggleSwitch toggleSwitchNivelAgua;
    @BindView(R.id.toggleSwitchLateralesDerIzqRetrovior)
    ToggleSwitch toggleSwitchLateralesDerIzqRetrovior;
    @BindView(R.id.btnContinuarElectrico)
    AppCompatButton btnContinuarElectrico;

    public FragmentPasajerosLujoAspectosElectricos() {
        // Required empty public constructor
    }

    public static FragmentPasajerosLujoAspectosElectricos newInstance(String param1, String param2) {
        FragmentPasajerosLujoAspectosElectricos fragment = new FragmentPasajerosLujoAspectosElectricos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pasajeros_lujo_aspectos_electricos, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}

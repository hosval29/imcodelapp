package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Adapters;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PagesAdapterFragmentsAlistamientoPasajeroFurgones extends FragmentPagerAdapter {
    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> nameList = new ArrayList<>();
    private static int numItemFragments = 3;

    public PagesAdapterFragmentsAlistamientoPasajeroFurgones(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String titleFrgament) {
        fragmentList.add(fragment);
        nameList.add(titleFrgament);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    public CharSequence getPageTitle(int position) {
        return nameList.get(position);
    }

    @Override
    public int getCount() {
        return numItemFragments;
    }
}

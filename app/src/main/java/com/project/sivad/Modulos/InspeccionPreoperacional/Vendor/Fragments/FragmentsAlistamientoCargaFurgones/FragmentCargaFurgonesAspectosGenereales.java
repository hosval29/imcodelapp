package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsAlistamientoCargaFurgones;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;
import com.pd.chocobar.ChocoBar;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentCargaFurgonesAspectosGenereales extends Fragment {


    @BindView(R.id.toggleSwitchLSRTTT)
    ToggleSwitch toggleSwitchLSRTTT;//1
    @BindView(R.id.toggleSwitchCambioAceite)
    ToggleSwitch toggleSwitchCambioAceite;//2
    @BindView(R.id.toggleSwitchSincronizacion)
    ToggleSwitch toggleSwitchSincronizacion;//3
    @BindView(R.id.toggleSwitchAlineacionBalanceo)
    ToggleSwitch toggleSwitchAlineacionBalanceo;//4
    @BindView(R.id.toggleSwitchCambioLlantas)
    ToggleSwitch toggleSwitchCambioLlantas;//5
    @BindView(R.id.toggleSwitchCinturonesSeguridad)
    ToggleSwitch toggleSwitchCinturonesSeguridad;//6
    @BindView(R.id.toggleSwitchCargaExtintores)
    ToggleSwitch toggleSwitchCargaExtintores;//7
    @BindView(R.id.toggleSwitchBotiquin)
    ToggleSwitch toggleSwitchBotiquin;//8
    @BindView(R.id.toggleSwitchVentanasCerraduras)
    ToggleSwitch toggleSwitchVentanasCerraduras;//9
    @BindView(R.id.toggleSwitchKitAntiderrame)
    ToggleSwitch toggleSwitchKitAntiderrame;//10
    @BindView(R.id.toggleSwitchEstadoFurgonCarpa)
    ToggleSwitch toggleSwitchEstadoFurgonCarpa;//11
    @BindView(R.id.toggleSwitchSenalizacionLogosCalcomania)
    ToggleSwitch toggleSwitchSenalizacionLogosCalcomania;//12
    @BindView(R.id.toggleSwitchHCGTLCMC)
    ToggleSwitch toggleSwitchHCGTLCMC;//13

    @BindView(R.id.idAppCompatTextviewErrorCinturonesSeguridad)
    AppCompatTextView idAppCompatTextviewErrorCinturonesSeguridad;
    @BindView(R.id.idAppCompatTextviewErrorCargaExtintores)
    AppCompatTextView idAppCompatTextviewErrorCargaExtintores;
    @BindView(R.id.idAppCompatTextviewErrorBotiquin)
    AppCompatTextView idAppCompatTextviewErrorBotiquin;
    @BindView(R.id.idAppCompatTextviewErrorVentanasCerraduras)
    AppCompatTextView idAppCompatTextviewErrorVentanasCerraduras;
    @BindView(R.id.idAppCompatTextviewErrorKitAntiderrame)
    AppCompatTextView idAppCompatTextviewErrorKitAntiderrame;
    @BindView(R.id.idAppCompatTextviewErrorEstadoFurgonCarpa)
    AppCompatTextView idAppCompatTextviewErrorEstadoFurgonCarpa;
    @BindView(R.id.idAppCompatTextviewErrorSenalizacionLogosCalcomania)
    AppCompatTextView idAppCompatTextviewErrorSenalizacionLogosCalcomania;
    @BindView(R.id.idAppCompatTextviewErrorHCGTLCMC)
    AppCompatTextView idAppCompatTextviewErrorHCGTLCMC;

    @BindView(R.id.appCompatCheckBoxFirma)
    AppCompatCheckBox appCompatCheckBoxFirma;
    @BindView(R.id.btnFinalizarInspeccion)
    AppCompatButton btnFinalizarInspeccion;
    @BindView(R.id.idAppCompatTextviewDateRealizacionCambioAceite)
    AppCompatTextView idAppCompatTextviewDateRealizacionCambioAceite;
    @BindView(R.id.idAppCompatTextviewDateRealizacionSincronizacion)
    AppCompatTextView idAppCompatTextviewDateRealizacionSincronizacion;
    @BindView(R.id.idAppCompatTextviewDateRealizacionAlineacionBalanceo)
    AppCompatTextView idAppCompatTextviewDateRealizacionAlineacionBalanceo;
    @BindView(R.id.idAppCompatTextviewDateRealizacionCambioLlantas)
    AppCompatTextView idAppCompatTextviewDateRealizacionCambioLlantas;

    @BindView(R.id.idAppCompatTextviewTitleLSRTTT)
    AppCompatTextView idAppCompatTextviewTitleLSRTTT;//1
    @BindView(R.id.idAppCompatTextviewTitleCambioAceite)
    AppCompatTextView idAppCompatTextviewTitleCambioAceite;//2
    @BindView(R.id.idAppCompatTextviewTitleSincronizacion)
    AppCompatTextView idAppCompatTextviewTitleSincronizacion;//3
    @BindView(R.id.idAppCompatTextviewTitleAlineacionBalanceo)
    AppCompatTextView idAppCompatTextviewTitleAlineacionBalanceo;//4
    @BindView(R.id.idAppCompatTextviewTitleCambioLlantas)
    AppCompatTextView idAppCompatTextviewTitleCambioLlantas;//5
    @BindView(R.id.idAppCompatTextviewTitleCinturonesSeguridad)
    AppCompatTextView idAppCompatTextviewTitleCinturonesSeguridad;//6
    @BindView(R.id.idAppCompatTextviewTitleCargaExtintores)
    AppCompatTextView idAppCompatTextviewTitleCargaExtintores;//7
    @BindView(R.id.idAppCompatTextviewTitleBotiquin)
    AppCompatTextView idAppCompatTextviewTitleBotiquin;//8
    @BindView(R.id.idAppCompatTextviewTitleVentanasCerraduras)
    AppCompatTextView idAppCompatTextviewTitleVentanasCerraduras;//9
    @BindView(R.id.idAppCompatTextviewTitleKitAntiderrame)
    AppCompatTextView idAppCompatTextviewTitleKitAntiderrame;//10
    @BindView(R.id.idAppCompatTextviewTitleEstadoFurgonCarpa)
    AppCompatTextView idAppCompatTextviewTitleEstadoFurgonCarpa;//11
    @BindView(R.id.idAppCompatTextviewTitleSenalizacionLogosCalcomania)
    AppCompatTextView idAppCompatTextviewTitleSenalizacionLogosCalcomania;//12
    @BindView(R.id.idAppCompatTextviewTitleHCGTLCMC)
    AppCompatTextView idAppCompatTextviewTitleHCGTLCMC;//13

    //Variables de validacion
    private boolean isLSRTTT = false;
    private boolean isCambioAceite = false;
    private boolean isSincronizacion = false;
    private boolean isAlineacionBalanceo = false;
    private boolean isCambioLlantas = false;
    private boolean isCinturonesSeguridad = false;
    private boolean isCargaExtintores = false;
    private boolean isBotiquin = false;
    private boolean isVentanasCerraduras = false;
    private boolean isKitAntiderrame = false;
    private boolean isEstadoFurgonCarpa = false;
    private boolean isSenalizacionLogosCalcomania = false;
    private boolean isHCGTLCMC = false;
    private boolean isCheckBoxFirma = false;
    private boolean isToggleBad = false;

    //Variables valores de los toggle (2 - Malo || 1 - Regular || 0 - Bueno)
    private String valueLSRTTT = "";
    private String valueCambioAceite = "";
    private String valueSincronizacion = "";
    private String valueAlineacionBalanceo = "";
    private String valueCambioLlantas = "";
    private String valueCinturonesSeguridad = "";
    private String valueCargaExtintores = "";
    private String valueBotiquin = "";
    private String valueVentanasCerraduras = "";
    private String valueKitAntiderrame = "";
    private String valueEstadoFurgonCarpa = "";
    private String valueSenalizacionLogosCalcomania = "";
    private String valueHCGTLCMC = "";
    private final String nameFragment = ConstantsStrings.NAME_FRAGMENT_ASPECTOS_GENERALES;

    //Interfaces
    InterfacesFragmentCFAG interfacesFragmentCFAG;

    public interface InterfacesFragmentCFAG {
        void sendDataAspectosGenerales(JSONArray jsonArray, int idInspection, int valueCheckbox);

        void registerNoveltiesInspectionAspectosGenerales(boolean isToggleBad, String nameToggle, String nameFragment);

        void sendAlertToggleBad(boolean isToggleBad, String nameTogle, String nameFragment);
    }

    //Variables de Utilidades
    private Context context;

    //Instanciamos la DB
    private DBSivad dbSivad;

    //Variables Arguments
    private int idInspection = 0;
    private int valueCheckBox = 0;

    public FragmentCargaFurgonesAspectosGenereales() {
        // Required empty public constructor
    }

    public static FragmentCargaFurgonesAspectosGenereales newInstance(String param1, String param2) {
        FragmentCargaFurgonesAspectosGenereales fragment = new FragmentCargaFurgonesAspectosGenereales();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InterfacesFragmentCFAG) {
            interfacesFragmentCFAG = (InterfacesFragmentCFAG) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        context = getContext();
        dbSivad = new DBSivad(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carga_furgones_aspectos_genereales, container, false);
        ButterKnife.bind(this, view);

        toggleSwitchLSRTTT.setCheckedPosition(0);
        toggleSwitchLSRTTT.setEnabled(false);
        valueLSRTTT = "0";
        toggleSwitchCambioAceite.setCheckedPosition(0);
        toggleSwitchCambioAceite.setEnabled(false);
        valueCambioAceite = "0";
        toggleSwitchSincronizacion.setCheckedPosition(0);
        toggleSwitchSincronizacion.setEnabled(false);
        valueSincronizacion = "0";
        toggleSwitchAlineacionBalanceo.setCheckedPosition(0);
        toggleSwitchAlineacionBalanceo.setEnabled(false);
        valueAlineacionBalanceo = "0";
        toggleSwitchCambioLlantas.setCheckedPosition(0);
        toggleSwitchCambioLlantas.setEnabled(false);
        valueCambioLlantas = "0";

        ResponseValidatePlateMileage responseValidatePlateMileage = dbSivad.getDataVehicle();
        if (responseValidatePlateMileage != null) {
            JsonArray jsonArray = responseValidatePlateMileage.getData().getDataAlerts();

            String dateRealizacionSincronizacion = "Vacio";
            String dateRealizacionCambioLlantas = "Vacio";
            String dateRealizacionCambioAceite = "Vacio";
            String dateRealizacionAlineacionBalanceo = "Vacio";

            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                if (jsonObject.has("typeAlert")
                        && jsonObject.get("typeAlert").getAsString().equals("Sincronización")) {
                    dateRealizacionSincronizacion = jsonObject.get("dateRealizacion").getAsString();
                }

                if (jsonObject.has("typeAlert")
                        && jsonObject.get("typeAlert").getAsString().equals("Alineación || Balanceo")) {
                    dateRealizacionAlineacionBalanceo = jsonObject.get("dateRealizacion").getAsString();
                }

                if (jsonObject.has("typeAlert")
                        && jsonObject.get("typeAlert").getAsString().equals("Cambio Aceite")) {
                    dateRealizacionCambioAceite = jsonObject.get("dateRealizacion").getAsString();
                }

                if (jsonObject.has("typeAlert")
                        && jsonObject.get("typeAlert").getAsString().equals("Cambio de Llantas")) {
                    dateRealizacionCambioLlantas = jsonObject.get("dateRealizacion").getAsString();
                }
            }

            idAppCompatTextviewDateRealizacionAlineacionBalanceo.setText(dateRealizacionAlineacionBalanceo);
            idAppCompatTextviewDateRealizacionSincronizacion.setText(dateRealizacionSincronizacion);
            idAppCompatTextviewDateRealizacionCambioAceite.setText(dateRealizacionCambioAceite);
            idAppCompatTextviewDateRealizacionCambioLlantas.setText(dateRealizacionCambioLlantas);
        }

        toggleSwitchLSRTTT.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isLSRTTT = true;
                valueLSRTTT = String.valueOf(i);

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleLSRTTT.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchCambioAceite.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isCambioAceite = true;
                valueCambioAceite = String.valueOf(i);

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleCambioAceite.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchSincronizacion.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isSincronizacion = true;
                valueSincronizacion = String.valueOf(i);

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleSincronizacion.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchAlineacionBalanceo.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isAlineacionBalanceo = true;
                valueAlineacionBalanceo = String.valueOf(i);

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleAlineacionBalanceo.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchCambioLlantas.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isCambioLlantas = true;
                valueCambioLlantas = String.valueOf(i);
                /*if (isCambioLlantas) {
                    idAppCompatTextviewErrorL.setVisibility(View.GONE);
                }*/

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleCambioLlantas.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchCinturonesSeguridad.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isCinturonesSeguridad = true;
                valueCinturonesSeguridad = String.valueOf(i);
                if (isCinturonesSeguridad) {
                    idAppCompatTextviewErrorCinturonesSeguridad.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleCinturonesSeguridad.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchCargaExtintores.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isCargaExtintores = true;
                valueCargaExtintores = String.valueOf(i);
                if (isCargaExtintores) {
                    idAppCompatTextviewErrorCargaExtintores.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleCargaExtintores.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }

        });

        toggleSwitchBotiquin.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isBotiquin = true;
                valueBotiquin = String.valueOf(i);
                if (isBotiquin) {
                    idAppCompatTextviewErrorBotiquin.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleBotiquin.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchVentanasCerraduras.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isVentanasCerraduras = true;
                valueVentanasCerraduras = String.valueOf(i);
                if (isVentanasCerraduras) {
                    idAppCompatTextviewErrorVentanasCerraduras.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleVentanasCerraduras.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchKitAntiderrame.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isKitAntiderrame = true;
                valueKitAntiderrame = String.valueOf(i);

                if (isKitAntiderrame) {
                    idAppCompatTextviewErrorKitAntiderrame.setVisibility(View.GONE);
                }

                if (i != 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Este item de evaluación bloquea el proceso de la inspección preoperacional.\n" +
                            "Automaticamente se enviará un correo electrónico notificando esta alerta.\n" +
                            "Pulse (SI), para generar la alerta o pulse (NO) para cancelar")
                            .setCancelable(false)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    isToggleBad = true;
                                    String nameToggle = idAppCompatTextviewTitleKitAntiderrame.getText().toString();
                                    interfacesFragmentCFAG.sendAlertToggleBad(isToggleBad, nameToggle, nameFragment);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    isKitAntiderrame = true;
                                    valueKitAntiderrame = "0";
                                    Log.v("valueKitAntiderrame", valueKitAntiderrame);
                                    //toggleSwitchKitAntiderrame.setCheckedPosition(null);
                                    toggleSwitchKitAntiderrame.setCheckedPosition(0);
                                    toggleSwitchKitAntiderrame.clearFocus();
                                    toggleSwitchKitAntiderrame.clearDisappearingChildren();
                                    dialog.cancel();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setTitle("Notificación Alerta");
                    dialog.show();
                }
            }
        });

        toggleSwitchEstadoFurgonCarpa.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isEstadoFurgonCarpa = true;
                valueEstadoFurgonCarpa = String.valueOf(i);
                if (isEstadoFurgonCarpa) {
                    idAppCompatTextviewErrorEstadoFurgonCarpa.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleEstadoFurgonCarpa.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchSenalizacionLogosCalcomania.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isSenalizacionLogosCalcomania = true;
                valueSenalizacionLogosCalcomania = String.valueOf(i);
                if (isSenalizacionLogosCalcomania) {
                    idAppCompatTextviewErrorSenalizacionLogosCalcomania.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleSenalizacionLogosCalcomania.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchHCGTLCMC.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isHCGTLCMC = true;
                valueHCGTLCMC = String.valueOf(i);
                if (isHCGTLCMC) {
                    idAppCompatTextviewErrorHCGTLCMC.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleHCGTLCMC.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        appCompatCheckBoxFirma.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.v("b", String.valueOf(b));
                isCheckBoxFirma = b;
            }
        });


        return view;
    }

    private void showDialogSelectionToggleBad(boolean isToggleBad, String nameToggle, String nameFragment) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Desea registrar la novedad de la opcion que seleccionó?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        interfacesFragmentCFAG.registerNoveltiesInspectionAspectosGenerales(isToggleBad, nameToggle, nameFragment);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Registrar Novedad.");
        dialog.show();
    }


    public void btnFinalizeInspection(View view) {
        if (!validateInputs()) {
            JSONArray jsonArrayDataAspectosGenerales = new JSONArray();

            JSONObject jsonObjectDocumentos = new JSONObject();
            JSONObject jsonObjectMantenimiento = new JSONObject();
            JSONObject jsonObjectAccesoriosImportantes = new JSONObject();
            JSONObject jsonObjectAccesoriosDelServicio = new JSONObject();
            JSONObject jsonObjectEquipoCarretera = new JSONObject();

            JSONObject jsonObjectDataDocumentos = new JSONObject();
            JSONObject jsonObjectDataMantenimiento = new JSONObject();
            JSONObject jsonObjectDataAccesoriosImportantes = new JSONObject();
            JSONObject jsonObjectDataAccesoriosDelServicio = new JSONObject();
            JSONObject jsonObjectDataEquipoCarretera = new JSONObject();

            try {
                jsonObjectDataDocumentos.put("LICENCIA DE CONDUCCION - SOAT - REVISION TECNOMECANICA- TARJETA DE PROPIEDAD - TARJETA DE OPERACIÓN", valueLSRTTT);
                jsonObjectDocumentos.put("DOCUMENTOS", jsonObjectDataDocumentos);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataMantenimiento.put("CAMBIO DE ACEITE", valueCambioAceite);
                jsonObjectDataMantenimiento.put("SINCRONIZACION", valueSincronizacion);
                jsonObjectDataMantenimiento.put("ALINEACION - BALANCEO", valueAlineacionBalanceo);
                jsonObjectDataMantenimiento.put("CAMBIO DE LLANTAS", valueCambioLlantas);
                jsonObjectMantenimiento.put("MANTENIMIENTO", jsonObjectDataMantenimiento);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataAccesoriosImportantes.put("CINTURONES DE SEGURIDAD", valueCinturonesSeguridad);
                jsonObjectDataAccesoriosImportantes.put("CARGA DE EXTINTORES Y VENCIMIENTO", valueCargaExtintores);
                jsonObjectDataAccesoriosImportantes.put("BOTIQUIN (COMPLETO Y BUEN ESTADO)", valueBotiquin);
                jsonObjectDataAccesoriosImportantes.put("VENTANAS Y CERRADURAS", valueVentanasCerraduras);
                jsonObjectAccesoriosImportantes.put("ACCESORIOS IMPORTANTES", jsonObjectDataAccesoriosImportantes);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataAccesoriosDelServicio.put("KIT ANTI-DERRAME", valueKitAntiderrame);
                jsonObjectDataAccesoriosDelServicio.put("ESTADO DEL FURGON, O CARPA", valueEstadoFurgonCarpa);
                jsonObjectDataAccesoriosDelServicio.put("CINTAS REFLECTIVAS/ PLACAS LISAS", valueSenalizacionLogosCalcomania);
                jsonObjectAccesoriosDelServicio.put("ACCESORIOS DEL SERVICIO", jsonObjectDataAccesoriosDelServicio);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataEquipoCarretera.put("(HERRAMIENTA,CRUCETA,GATO,TACOS,SEÑALES,CHALECO,CONOS)", valueHCGTLCMC);
                jsonObjectEquipoCarretera.put("EQUIPO DE CARRETERA", jsonObjectDataEquipoCarretera);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonArrayDataAspectosGenerales.put(jsonObjectDocumentos);
            jsonArrayDataAspectosGenerales.put(jsonObjectMantenimiento);
            jsonArrayDataAspectosGenerales.put(jsonObjectAccesoriosImportantes);
            jsonArrayDataAspectosGenerales.put(jsonObjectAccesoriosDelServicio);
            jsonArrayDataAspectosGenerales.put(jsonObjectEquipoCarretera);

            idInspection = dbSivad.getLastId();


            if (isCheckBoxFirma) {
                valueCheckBox = 1;
            }

            interfacesFragmentCFAG.sendDataAspectosGenerales(jsonArrayDataAspectosGenerales, idInspection, valueCheckBox);
        }
    }

    private boolean validateInputs() {
        boolean error = false;
        if (!isCinturonesSeguridad) {
            idAppCompatTextviewErrorCinturonesSeguridad.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isCargaExtintores) {
            idAppCompatTextviewErrorCargaExtintores.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isBotiquin) {
            idAppCompatTextviewErrorBotiquin.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isVentanasCerraduras) {
            idAppCompatTextviewErrorVentanasCerraduras.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isKitAntiderrame) {
            idAppCompatTextviewErrorKitAntiderrame.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isEstadoFurgonCarpa) {
            idAppCompatTextviewErrorEstadoFurgonCarpa.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isSenalizacionLogosCalcomania) {
            idAppCompatTextviewErrorSenalizacionLogosCalcomania.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isHCGTLCMC) {
            idAppCompatTextviewErrorHCGTLCMC.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isCheckBoxFirma) {
            appCompatCheckBoxFirma.setError("Debe aceptar condiciones de firma");
            error = true;
        }

        return error;
    }


    public void response(int codeStatus, String message) {
        ChocoBar.builder().setView(getView())
                .setText("RED")
                .setDuration(ChocoBar.LENGTH_INDEFINITE)
                .setActionText(android.R.string.ok)
                .red()   // in built red ChocoBar
                .show();
    }

}

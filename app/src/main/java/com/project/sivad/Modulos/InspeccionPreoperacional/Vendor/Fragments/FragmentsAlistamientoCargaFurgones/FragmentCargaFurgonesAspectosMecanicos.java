package com.project.sivad.Modulos.InspeccionPreoperacional.Vendor.Fragments.FragmentsAlistamientoCargaFurgones;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.llollox.androidtoggleswitch.widgets.ToggleSwitch;
import com.project.sivad.R;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentCargaFurgonesAspectosMecanicos extends Fragment {


    @BindView(R.id.toggleSwitchNivelAceiteMotor)
    ToggleSwitch toggleSwitchNivelAceiteMotor;//1
    @BindView(R.id.toggleSwitchTensionCorreas)
    ToggleSwitch toggleSwitchTensionCorreas;//2
    @BindView(R.id.toggleSwitchFiltroAceiteFiltroAire)
    ToggleSwitch toggleSwitchFiltroAceiteFiltroAire;//3
    @BindView(R.id.toggleSwitchFugaRefrigerante)
    ToggleSwitch toggleSwitchFugaRefrigerante;//4
    @BindView(R.id.toggleSwitchAguaBateria)
    ToggleSwitch toggleSwitchAguaBateria;//5
    @BindView(R.id.toggleSwitchRevisaNivelHidraulicoDireccion)
    ToggleSwitch toggleSwitchRevisaNivelHidraulicoDireccion;//6
    @BindView(R.id.toggleSwitchNivelLiquidoFrenos)
    ToggleSwitch toggleSwitchNivelLiquidoFrenos;//7
    @BindView(R.id.toggleSwitchFrenosPrincipal)
    ToggleSwitch toggleSwitchFrenosPrincipal;//8
    @BindView(R.id.toggleSwitchFrenoEmergencia)
    ToggleSwitch toggleSwitchFrenoEmergencia;//9
    @BindView(R.id.toggleSwitchpresionLlantas)
    ToggleSwitch toggleSwitchpresionLlantas;//10
    @BindView(R.id.toggleSwitchProfundidadMenor4mm)
    ToggleSwitch toggleSwitchProfundidadMenor4mm;//11
    @BindView(R.id.toggleSwitchEstadoRuedaRinPernos)
    ToggleSwitch toggleSwitchEstadoRuedaRinPernos;//12
    @BindView(R.id.toggleSwitchNeumaticoDelanteroDerIzq)
    ToggleSwitch toggleSwitchNeumaticoDelanteroDerIzq;//13
    @BindView(R.id.toggleSwitchneumaticoTraseroDerIzq)
    ToggleSwitch toggleSwitchneumaticoTraseroDerIzq;//14
    @BindView(R.id.toggleSwitchLlantaRepuesto)
    ToggleSwitch toggleSwitchLlantaRepuesto;//15
    @BindView(R.id.toggleSwitchMuellesBombonasBalancines)
    ToggleSwitch toggleSwitchMuellesBombonasBalancines;//16
    @BindView(R.id.toggleSwitchGraduacionClutch)
    ToggleSwitch toggleSwitchGraduacionClutch;//17

    @BindView(R.id.btnContinuarMecanico)
    AppCompatButton btnContinuarMecanico;
    @BindView(R.id.idAppCompatTextviewErrorNivelAceiteMotor)
    AppCompatTextView idAppCompatTextviewErrorNivelAceiteMotor;
    @BindView(R.id.idAppCompatTextviewErrorTensionCorreas)
    AppCompatTextView idAppCompatTextviewErrorTensionCorreas;
    @BindView(R.id.idAppCompatTextviewErrorFiltroAceiteFiltroAire)
    AppCompatTextView idAppCompatTextviewErrorFiltroAceiteFiltroAire;
    @BindView(R.id.idAppCompatTextviewErrorFugaRefrigerante)
    AppCompatTextView idAppCompatTextviewErrorFugaRefrigerante;
    @BindView(R.id.idAppCompatTextviewErrorAguaBateria)
    AppCompatTextView idAppCompatTextviewErrorAguaBateria;
    @BindView(R.id.idAppCompatTextviewErrorRevisaNivelHidraulicoDireccion)
    AppCompatTextView idAppCompatTextviewErrorRevisaNivelHidraulicoDireccion;
    @BindView(R.id.idAppCompatTextviewErrorNivelLiquidoFrenos)
    AppCompatTextView idAppCompatTextviewErrorNivelLiquidoFrenos;
    @BindView(R.id.idAppCompatTextviewErrorFrenosPrincipal)
    AppCompatTextView idAppCompatTextviewErrorFrenosPrincipal;
    @BindView(R.id.idAppCompatTextviewErrorFrenoEmergencia)
    AppCompatTextView idAppCompatTextviewErrorFrenoEmergencia;
    @BindView(R.id.idAppCompatTextviewErrorPresionLlantas)
    AppCompatTextView idAppCompatTextviewErrorPresionLlantas;
    @BindView(R.id.idAppCompatTextviewErrorProfundidadMenor4mm)
    AppCompatTextView idAppCompatTextviewErrorProfundidadMenor4mm;
    @BindView(R.id.idAppCompatTextviewErrorEstadoRuedaRinPernos)
    AppCompatTextView idAppCompatTextviewErrorEstadoRuedaRinPernos;
    @BindView(R.id.idAppCompatTextviewErrorNeumaticoDelanteroDerIzq)
    AppCompatTextView idAppCompatTextviewErrorNeumaticoDelanteroDerIzq;
    @BindView(R.id.idAppCompatTextviewErrorNeumaticoTraseroDerIzq)
    AppCompatTextView idAppCompatTextviewErrorNeumaticoTraseroDerIzq;
    @BindView(R.id.idAppCompatTextviewErrorLlantaRepuesto)
    AppCompatTextView idAppCompatTextviewErrorLlantaRepuesto;
    @BindView(R.id.idAppCompatTextviewErrorMuellesBombonasBalancines)
    AppCompatTextView idAppCompatTextviewErrorMuellesBombonasBalancines;
    @BindView(R.id.idAppCompatTextviewErrorGraduacionClutch)
    AppCompatTextView idAppCompatTextviewErrorGraduacionClutch;

    @BindView(R.id.idAppCompatTextviewTitleNivelAceiteMotor)
    AppCompatTextView idAppCompatTextviewTitleNivelAceiteMotor;//1
    @BindView(R.id.idAppCompatTextviewTitleTensionCorreas)
    AppCompatTextView idAppCompatTextviewTitleTensionCorreas;//2
    @BindView(R.id.idAppCompatTextviewTitleFiltroAceiteFiltroAire)
    AppCompatTextView idAppCompatTextviewTitleFiltroAceiteFiltroAire;//3
    @BindView(R.id.idAppCompatTextviewTitleFugaRefrigerante)
    AppCompatTextView idAppCompatTextviewTitleFugaRefrigerante;//4
    @BindView(R.id.idAppCompatTextviewTitleAguaBateria)
    AppCompatTextView idAppCompatTextviewTitleAguaBateria;//5
    @BindView(R.id.idAppCompatTextviewTitleRevisaNivelHidraulicoDireccion)
    AppCompatTextView idAppCompatTextviewTitleRevisaNivelHidraulicoDireccion;//6
    @BindView(R.id.idAppCompatTextviewTitleNivelLiquidoFrenos)
    AppCompatTextView idAppCompatTextviewTitleNivelLiquidoFrenos;//7
    @BindView(R.id.idAppCompatTextviewTitleFrenosPrincipal)
    AppCompatTextView idAppCompatTextviewTitleFrenosPrincipal;//8
    @BindView(R.id.idAppCompatTextviewTitleFrenoEmergencia)
    AppCompatTextView idAppCompatTextviewTitleFrenoEmergencia;//9
    @BindView(R.id.idAppCompatTextviewTitlePresionLlantas)
    AppCompatTextView idAppCompatTextviewTitlePresionLlantas;//10
    @BindView(R.id.idAppCompatTextviewTitleProfundidadMenor4mm)
    AppCompatTextView idAppCompatTextviewTitleProfundidadMenor4mm;//11
    @BindView(R.id.idAppCompatTextviewTitleEstadoRuedaRinPernos)
    AppCompatTextView idAppCompatTextviewTitleEstadoRuedaRinPernos;//12
    @BindView(R.id.idAppCompatTextviewTitleNeumaticoDelanteroDerIzq)
    AppCompatTextView idAppCompatTextviewTitleNeumaticoDelanteroDerIzq;//13
    @BindView(R.id.idAppCompatTextviewTitleNeumaticoTraseroDerIzq)
    AppCompatTextView idAppCompatTextviewTitleNeumaticoTraseroDerIzq;//14
    @BindView(R.id.idAppCompatTextviewTitleLlantaRepuesto)
    AppCompatTextView idAppCompatTextviewTitleLlantaRepuesto;//15
    @BindView(R.id.idAppCompatTextviewTitleMuellesBombonasBalancines)
    AppCompatTextView idAppCompatTextviewTitleMuellesBombonasBalancines;//16
    @BindView(R.id.idAppCompatTextviewTitleGraduacionClutch)
    AppCompatTextView idAppCompatTextviewTitleGraduacionClutch;//17

    //variables de validacion
    private boolean isNivelAceiteMotor = false;
    private boolean isTensionCorreas = false;
    private boolean isFiltroAceiteFiltroAire = false;
    private boolean isFugaRegfrigerante = false;
    private boolean isAguaBateria = false;
    private boolean isRevisionNivelHidraulicoDireccion = false;
    private boolean isNivelLiquidoFrenos = false;
    private boolean isFrenoPrincipal = false;
    private boolean isFrenoEmergencia = false;
    private boolean isPresionLlantas = false;
    private boolean isProfundidadMenor4Mm = false;
    private boolean isEstadoRuedaRinPernos = false;
    private boolean isNeumaticoDelanteroIzq = false;
    private boolean isNeumaticoTraseroIzq = false;
    private boolean isLlantaRepuesto = false;
    private boolean isMuellesBombonasBalancines = false;
    private boolean isGraduacionClutch = false;
    private boolean isToggleBad = false;

    //Variables valores de los toggle (0 - Malo || 1 - Regular || 2- Bueno)
    private String valueNivelAceiteMotor = "";
    private String valueTensionCorreas = "";
    private String valueFiltroAceiteFiltroAire = "";
    private String valueFugaRegfrigerante = "";
    private String valueAguaBateria = "";
    private String valueRevisionNivelHidraulicoDireccion = "";
    private String valueNivelLiquidoFrenos = "";
    private String valueFrenoPrincipal = "";
    private String valueFrenoEmergencia = "";
    private String valuePresionLlantas = "";
    private String valueProfundidadMenor4Mm = "";
    private String valueEstadoRuedaRinPernos = "";
    private String valueNeumaticoDelanteroIzq = "";
    private String valueNeumaticoTraseroIzq = "";
    private String valueLlantaRepuesto = "";
    private String valueMuellesBombonasBalancines = "";
    private String valueGraduacionClutch = "";
    private final String nameFragment = ConstantsStrings.NAME_FRAGMENT_ASPECTOS_MECANICOS;

    //Variables de utilidad
    private Context context;

    public FragmentCargaFurgonesAspectosMecanicos() {
        // Required empty public constructor
    }

    InterfacesFragmentCFAM interfacesFragmentCFAM;

    public interface InterfacesFragmentCFAM {
        void sendDataAspectosMecanicos(JSONArray jsonArrayDataAspectosMecanicos);

        void registerNoveltiesInspectionAspectosMecanicos(boolean isToggleBad, String nameToggle, String nameFragment);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            interfacesFragmentCFAM = (InterfacesFragmentCFAM) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    public static FragmentCargaFurgonesAspectosMecanicos newInstance(String param1, String param2) {
        FragmentCargaFurgonesAspectosMecanicos fragment = new FragmentCargaFurgonesAspectosMecanicos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        context = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_carga_furgones_aspectos_mecanicos, container, false);
        ButterKnife.bind(this, view);

        toggleSwitchNivelAceiteMotor.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {

                isNivelAceiteMotor = true;
                valueNivelAceiteMotor = String.valueOf(i);

                if (isNivelAceiteMotor) {
                    idAppCompatTextviewErrorNivelAceiteMotor.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleNivelAceiteMotor.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchTensionCorreas.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isTensionCorreas = true;
                valueTensionCorreas = String.valueOf(i);

                if (isTensionCorreas) {
                    idAppCompatTextviewErrorTensionCorreas.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleTensionCorreas.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchFiltroAceiteFiltroAire.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isFiltroAceiteFiltroAire = true;
                valueFiltroAceiteFiltroAire = String.valueOf(i);

                if (isFiltroAceiteFiltroAire) {
                    idAppCompatTextviewErrorFiltroAceiteFiltroAire.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleFiltroAceiteFiltroAire.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchFugaRefrigerante.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isFugaRegfrigerante = true;
                valueFugaRegfrigerante = String.valueOf(i);

                if (isFugaRegfrigerante) {
                    idAppCompatTextviewErrorFugaRefrigerante.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleFugaRefrigerante.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchAguaBateria.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isAguaBateria = true;
                valueAguaBateria = String.valueOf(i);

                if (isAguaBateria) {
                    idAppCompatTextviewErrorAguaBateria.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleAguaBateria.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchRevisaNivelHidraulicoDireccion.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isRevisionNivelHidraulicoDireccion = true;
                valueRevisionNivelHidraulicoDireccion = String.valueOf(i);

                if (isRevisionNivelHidraulicoDireccion) {
                    idAppCompatTextviewErrorRevisaNivelHidraulicoDireccion.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleRevisaNivelHidraulicoDireccion.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchNivelLiquidoFrenos.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isNivelLiquidoFrenos = true;
                valueNivelLiquidoFrenos = String.valueOf(i);

                if (isNivelLiquidoFrenos) {
                    idAppCompatTextviewErrorNivelLiquidoFrenos.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleNivelLiquidoFrenos.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchFrenosPrincipal.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isFrenoPrincipal = true;
                valueFrenoPrincipal = String.valueOf(i);

                if (isFrenoPrincipal) {
                    idAppCompatTextviewErrorFrenosPrincipal.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleFrenosPrincipal.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchFrenoEmergencia.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isFrenoEmergencia = true;
                valueFrenoEmergencia = String.valueOf(i);

                if (isFrenoEmergencia) {
                    idAppCompatTextviewErrorFrenoEmergencia.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleFrenoEmergencia.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchpresionLlantas.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isPresionLlantas = true;
                valuePresionLlantas = String.valueOf(i);

                if (isPresionLlantas) {
                    idAppCompatTextviewErrorPresionLlantas.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitlePresionLlantas.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchProfundidadMenor4mm.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isProfundidadMenor4Mm = true;
                valueProfundidadMenor4Mm = String.valueOf(i);

                if (isProfundidadMenor4Mm) {
                    idAppCompatTextviewErrorProfundidadMenor4mm.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleProfundidadMenor4mm.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchEstadoRuedaRinPernos.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isEstadoRuedaRinPernos = true;
                valueEstadoRuedaRinPernos = String.valueOf(i);

                if (isEstadoRuedaRinPernos) {
                    idAppCompatTextviewErrorEstadoRuedaRinPernos.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleEstadoRuedaRinPernos.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchNeumaticoDelanteroDerIzq.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isNeumaticoDelanteroIzq = true;
                valueNeumaticoDelanteroIzq = String.valueOf(i);

                if (isNeumaticoDelanteroIzq) {
                    idAppCompatTextviewErrorNeumaticoDelanteroDerIzq.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleNeumaticoDelanteroDerIzq.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchneumaticoTraseroDerIzq.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isNeumaticoTraseroIzq = true;
                valueNeumaticoTraseroIzq = String.valueOf(i);

                if (isNeumaticoTraseroIzq) {
                    idAppCompatTextviewErrorNeumaticoTraseroDerIzq.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleNeumaticoTraseroDerIzq.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchLlantaRepuesto.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isLlantaRepuesto = true;
                valueLlantaRepuesto = String.valueOf(i);

                if (isLlantaRepuesto) {
                    idAppCompatTextviewErrorLlantaRepuesto.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleLlantaRepuesto.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchMuellesBombonasBalancines.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isMuellesBombonasBalancines = true;
                valueMuellesBombonasBalancines = String.valueOf(i);

                if (isMuellesBombonasBalancines) {
                    idAppCompatTextviewErrorMuellesBombonasBalancines.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleMuellesBombonasBalancines.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        toggleSwitchGraduacionClutch.setOnChangeListener(new ToggleSwitch.OnChangeListener() {
            @Override
            public void onToggleSwitchChanged(int i) {
                //Toast.makeText(getContext(), "estoy en " + i, Toast.LENGTH_SHORT).show();
                isGraduacionClutch = true;
                valueGraduacionClutch = String.valueOf(i);

                if (isGraduacionClutch) {
                    idAppCompatTextviewErrorGraduacionClutch.setVisibility(View.GONE);
                }

                if (i == 2) {
                    isToggleBad = true;
                    String nameToggle = idAppCompatTextviewTitleGraduacionClutch.getText().toString();
                    showDialogSelectionToggleBad(isToggleBad, nameToggle, nameFragment);
                }
            }
        });

        return view;
    }

    private void showDialogSelectionToggleBad(boolean isToggleBad, String nameToggle, String nameFragment) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("¿Desea registrar la novedad de la opcion que seleccionó?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        interfacesFragmentCFAM.registerNoveltiesInspectionAspectosMecanicos(isToggleBad, nameToggle, nameFragment);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setTitle("Registrar Novedad.");
        dialog.show();
    }


    public void btnContinue(View view) {


        if (!validateInputs()) {

            JSONArray jsonArrayDataAspectosMecanicos = new JSONArray();

            JSONObject jsonObjectMotor = new JSONObject();
            JSONObject jsonObjectFrenoDireccion = new JSONObject();
            JSONObject jsonObjectLlanta = new JSONObject();
            JSONObject jsonObjectSuspencion = new JSONObject();
            JSONObject jsonObjectClutch = new JSONObject();

            JSONObject jsonObjectDataMotor = new JSONObject();
            JSONObject jsonObjectDataFrenosDireccion = new JSONObject();
            JSONObject jsonObjectDataLlanta = new JSONObject();
            JSONObject jsonObjectDataSuspension = new JSONObject();
            JSONObject jsonObjectDataClutch = new JSONObject();

            try {
                jsonObjectDataMotor.put("NIVEL DE ACEITE DE MOTOR", valueNivelAceiteMotor);
                jsonObjectDataMotor.put("TENSION DE CORREAS", valueTensionCorreas);
                jsonObjectDataMotor.put("FILTRO DE ACEITE - FILTRO DE AIRE", valueFiltroAceiteFiltroAire);
                jsonObjectDataMotor.put("NIVEL REFRIGERANTE", valueFugaRegfrigerante);
                jsonObjectDataMotor.put("AGUA BATERIA", valueAguaBateria);
                jsonObjectMotor.put("MOTOR", jsonObjectDataMotor);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataFrenosDireccion.put("NIVEL HDRAULICO DIRECCION", valueRevisionNivelHidraulicoDireccion);
                jsonObjectDataFrenosDireccion.put("NIVEL LIQUIDO DE FRENOS", valueNivelLiquidoFrenos);
                jsonObjectDataFrenosDireccion.put("FRENO PRINCIPAL", valueFrenoPrincipal);
                jsonObjectDataFrenosDireccion.put("FRENO DE EMERGENCIA", valueFrenoEmergencia);
                jsonObjectFrenoDireccion.put("FRENOS / DIRECCION", jsonObjectDataFrenosDireccion);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataLlanta.put("PRESION LLANTAS", valuePresionLlantas);
                jsonObjectDataLlanta.put("PROFUNDIDAD MENOR A 4MM", valueProfundidadMenor4Mm);
                jsonObjectDataLlanta.put("ESTADO RUEDA/RIN/PERNOS", valueEstadoRuedaRinPernos);
                jsonObjectDataLlanta.put("NEUMATICO DELANTERO DER - IZQ", valueNeumaticoDelanteroIzq);
                jsonObjectDataLlanta.put("NEUMATICO TRASERO DER - IZQ", valueNeumaticoTraseroIzq);
                jsonObjectDataLlanta.put("LLANTA REPUESTO", valueLlantaRepuesto);
                jsonObjectLlanta.put("LLANTAS", jsonObjectDataLlanta);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataSuspension.put("MUELLES / BOMBONAS /BALANCINES", valueMuellesBombonasBalancines);
                jsonObjectSuspencion.put("SUSPENCION", jsonObjectDataSuspension);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObjectDataClutch.put("GRADUACION DEL CLUTCH", valueGraduacionClutch);
                jsonObjectClutch.put("CLUTCH", jsonObjectDataClutch);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonArrayDataAspectosMecanicos.put(jsonObjectMotor);
            jsonArrayDataAspectosMecanicos.put(jsonObjectLlanta);
            jsonArrayDataAspectosMecanicos.put(jsonObjectFrenoDireccion);
            jsonArrayDataAspectosMecanicos.put(jsonObjectSuspencion);
            jsonArrayDataAspectosMecanicos.put(jsonObjectClutch);

            interfacesFragmentCFAM.sendDataAspectosMecanicos(jsonArrayDataAspectosMecanicos);
        }
    }

    private boolean validateInputs() {
        boolean error = false;

        if (!isNivelAceiteMotor) {
            idAppCompatTextviewErrorNivelAceiteMotor.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isTensionCorreas) {
            idAppCompatTextviewErrorTensionCorreas.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isFiltroAceiteFiltroAire) {
            idAppCompatTextviewErrorFiltroAceiteFiltroAire.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isFugaRegfrigerante) {
            idAppCompatTextviewErrorFugaRefrigerante.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isAguaBateria) {
            idAppCompatTextviewErrorAguaBateria.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isRevisionNivelHidraulicoDireccion) {
            idAppCompatTextviewErrorRevisaNivelHidraulicoDireccion.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isNivelLiquidoFrenos) {
            idAppCompatTextviewErrorNivelLiquidoFrenos.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isFrenoPrincipal) {
            idAppCompatTextviewErrorFrenosPrincipal.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isFrenoEmergencia) {
            idAppCompatTextviewErrorFrenoEmergencia.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isPresionLlantas) {
            idAppCompatTextviewErrorPresionLlantas.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isProfundidadMenor4Mm) {
            idAppCompatTextviewErrorProfundidadMenor4mm.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isEstadoRuedaRinPernos) {
            idAppCompatTextviewErrorEstadoRuedaRinPernos.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isNeumaticoDelanteroIzq) {
            idAppCompatTextviewErrorNeumaticoDelanteroDerIzq.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isNeumaticoTraseroIzq) {
            idAppCompatTextviewErrorNeumaticoTraseroDerIzq.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isLlantaRepuesto) {
            idAppCompatTextviewErrorLlantaRepuesto.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isMuellesBombonasBalancines) {
            idAppCompatTextviewErrorMuellesBombonasBalancines.setVisibility(View.VISIBLE);
            error = true;
        }

        if (!isGraduacionClutch) {
            idAppCompatTextviewErrorGraduacionClutch.setVisibility(View.VISIBLE);
            error = true;
        }

        return error;
    }

}

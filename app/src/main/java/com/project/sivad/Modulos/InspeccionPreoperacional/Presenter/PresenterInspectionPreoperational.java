package com.project.sivad.Modulos.InspeccionPreoperacional.Presenter;

import android.content.Context;

import com.project.sivad.Modulos.InspeccionPreoperacional.Interfaces.InterfacesInspeccionPreoperation;
import com.project.sivad.Modulos.InspeccionPreoperacional.Model.ModelInspectionPreoperational;
import com.project.sivad.http.Response.ResponseError;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONObject;

public class PresenterInspectionPreoperational implements InterfacesInspeccionPreoperation.InterfacesPresenterInspectionPreoperational {
    private InterfacesInspeccionPreoperation.InterfacesModelInspectionPreoperational interfacesModelInspectionPreoperational;
    private InterfacesInspeccionPreoperation.InterfacesViewInspeccionPreoperational interfacesViewInspeccionPreoperational;
    private InterfacesInspeccionPreoperation.InterfacesFragmentNoveltie interfacesFragmentNoveltie;

    public PresenterInspectionPreoperational(InterfacesInspeccionPreoperation.InterfacesViewInspeccionPreoperational interfacesViewInspeccionPreoperational
            , InterfacesInspeccionPreoperation.InterfacesFragmentNoveltie interfacesFragmentNoveltie
            , InterfacesInspeccionPreoperation.InterfacesFragmentNoveltie interfacesFragmentNoveltieAlertToggleBad
            , Context context) {
        this.interfacesViewInspeccionPreoperational = interfacesViewInspeccionPreoperational;
        this.interfacesFragmentNoveltie = interfacesFragmentNoveltie;
        this.interfacesModelInspectionPreoperational = new ModelInspectionPreoperational(this, context);
    }

    @Override
    public void interfacesViewSuccessResponse(ResponseVehicleInspecion responseVehicleInspecion) {
        if (interfacesViewInspeccionPreoperational != null) {
            interfacesViewInspeccionPreoperational.interfacesViewSuccessResponse(responseVehicleInspecion);
        }
    }

    @Override
    public void interfacesViewErrorResponse(ResponseError responseError, JSONObject jsonObject) {
        if (interfacesViewInspeccionPreoperational != null) {
            interfacesViewInspeccionPreoperational.interfacesViewErrorResponse(responseError, jsonObject);
        }
    }

    @Override
    public void postDataInspectionPreoperational(String dateRegister, int idUser, int idVehicle, JSONArray inspection, JSONObject jsonArrayDataNoveltiesInspection, int valueCheckBox, int inspectionEnabled) {
        if (interfacesViewInspeccionPreoperational != null) {
            interfacesModelInspectionPreoperational.postDataInspectionPreoperational(dateRegister, idUser, idVehicle, inspection, jsonArrayDataNoveltiesInspection, valueCheckBox, inspectionEnabled);
        }
    }

    @Override
    public void postDataAlertByItemBadToggle(int idUser
            , String nameUser
            , String lastNameUser
            , String plateVehicle
            , int idVehicle
            , String dateAlert
            , String timeRegister
            , String nameToggle
            , String nameFragment, String dateRegisterAlertInspectionItemBad) {
        if (interfacesViewInspeccionPreoperational != null) {
            interfacesModelInspectionPreoperational.postDataAlertByItemBadToggle(idUser, nameUser, lastNameUser, plateVehicle, idVehicle, dateAlert, timeRegister, nameToggle, nameFragment, dateRegisterAlertInspectionItemBad);
        }
    }

    @Override
    public void responseErrorPostDataAlertByItemBadToggle(ResponseError responseError, JSONObject jsonObject) {
        if (interfacesViewInspeccionPreoperational != null) {
            interfacesViewInspeccionPreoperational.responseErrorPostDataAlertByItemBadToggle(responseError, jsonObject);
        }
    }

    @Override
    public void responseSuccesPostDataAlertByItemBadToggle(ResponseJson responseJson) {
        if (interfacesViewInspeccionPreoperational != null) {
            interfacesViewInspeccionPreoperational.responseSuccesPostDataAlertByItemBadToggle(responseJson);
        }
    }

    /**
     * Estos son los metodos para la interfaces Fragament Novelties Inspection
     */
    @Override
    public void responseErrorPostDataNoveltiesInspection(ResponseError responseError, JSONObject jsonObject) {
        if (interfacesFragmentNoveltie != null) {
            interfacesFragmentNoveltie.responseErrorPostDataNoveltiesInspection(responseError, jsonObject);
        }
    }

    @Override
    public void responseSuccesPostDataNoveltiesInspection(ResponseJson responseJson) {
        if (interfacesFragmentNoveltie != null) {
            interfacesFragmentNoveltie.responseSuccesPostDataNoveltiesInspection(responseJson);
        }
    }

    @Override
    public void postDataNoveltiesInspection(JSONObject jsonObjectDataNoveltiesInspection) {
        if (interfacesFragmentNoveltie != null) {
            interfacesModelInspectionPreoperational.postDataNoveltiesInspection(jsonObjectDataNoveltiesInspection);
        }
    }

}

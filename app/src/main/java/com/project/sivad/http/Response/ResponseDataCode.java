package com.project.sivad.http.Response;

public class ResponseDataCode {

    private int codeStatus;
    private String message;
    Data data;


    // Getter Methods

    public String getMessage() {
        return message;
    }

    public int getCodeStatus() {
        return codeStatus;
    }

    public Data getData() {
        return data;
    }

    // Setter Methods

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseDataCode{" +
                "codeStatus=" + codeStatus +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}


package com.project.sivad.http.Response;

public class ResponseSuccessUpdateMeleage {
    int codeStatus;
    String message;
    DataResponseUpdateMeleage data;

    public int getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataResponseUpdateMeleage getData() {
        return data;
    }

    public void setData(DataResponseUpdateMeleage data) {
        this.data = data;
    }
}

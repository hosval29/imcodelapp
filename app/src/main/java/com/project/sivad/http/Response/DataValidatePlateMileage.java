package com.project.sivad.http.Response;

import com.google.gson.JsonArray;

public class DataValidatePlateMileage {
    private String plate;
    private int mileageCurrent;
    private int idVehicle;
    private String typeInspection;
    private String dateLastUpdate;
    private JsonArray dataAlerts;
    private int numberInspections;

    // Getter Methods

    public String getLastDateUpdate() {
        return dateLastUpdate;
    }

    public String getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(String dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    public int getNumberInspections() {
        return numberInspections;
    }

    public void setNumberInspections(int numberInspections) {
        this.numberInspections = numberInspections;
    }

    public void setLastDateUpdate(String lastDateUpdate) {
        this.dateLastUpdate = lastDateUpdate;
    }

    public JsonArray getDataAlerts() {
        return dataAlerts;
    }

    public void setDataAlerts(JsonArray dataAlerts) {
        this.dataAlerts = dataAlerts;
    }

    public int getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(int idVehicle) {
        this.idVehicle = idVehicle;
    }

    public String getPlate() {
        return plate;
    }

    public float getMileageCurrent() {
        return mileageCurrent;
    }

    public String getTypeInspection() {
        return typeInspection;
    }

    // Setter Methods

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public void setMileageCurrent(int mileageCurrent) {
        this.mileageCurrent = mileageCurrent;
    }

    public void setTypeInspection(String typeInspection) {
        this.typeInspection = typeInspection;
    }

    @Override
    public String toString() {
        return "DataValidatePlateMileage{" +
                "plate='" + plate + '\'' +
                ", mileageCurrent=" + mileageCurrent +
                ", idVehicle=" + idVehicle +
                ", typeInspection='" + typeInspection + '\'' +
                ", dateLastUpdate='" + dateLastUpdate + '\'' +
                ", dataAlerts=" + dataAlerts +
                ", numberInspections=" + numberInspections +
                '}';
    }
}

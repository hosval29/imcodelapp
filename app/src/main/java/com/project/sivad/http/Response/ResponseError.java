package com.project.sivad.http.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseError {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("codeStatus")
    @Expose
    private int codeStatus;

    public int getErrorcode() {
        return codeStatus;
    }

    public void setErrorcode(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseError{" +
                "message='" + message + '\'' +
                ", codestatus='" + codeStatus + '\'' +
                '}';
    }
}

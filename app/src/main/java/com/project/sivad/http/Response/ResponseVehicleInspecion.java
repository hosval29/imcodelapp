package com.project.sivad.http.Response;

public class ResponseVehicleInspecion {
    int codeStatus;
    String message;
    DataVehicleResponse data;

    public int getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataVehicleResponse getData() {
        return data;
    }

    public void setData(DataVehicleResponse data) {
        this.data = data;
    }


}

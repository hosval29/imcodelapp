package com.project.sivad.http.Response;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseJson {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("codeStatus")
    @Expose
    private int codeStatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    @Override
    public String toString() {
        return "ResponseJson{" +
                "message='" + message + '\'' +
                ", codeStatus=" + codeStatus +
                '}';
    }
}


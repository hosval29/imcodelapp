package com.project.sivad.http.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class DataLoginUser {
    private String name;
    private String lastName;
    private String profile;
    private int phone;
    private String email;
    private int idUser;
    private int idHistoryLog;
    private int idUserSession;
    private String token;
    private JsonArray modules;

    // Getter Methods


    public JsonArray getModules() {
        return modules;
    }

    public void setModules(JsonArray modules) {
        this.modules = modules;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdUserSession() {
        return idUserSession;
    }

    public void setIdUserSession(int idUserSession) {
        this.idUserSession = idUserSession;
    }

    public int getIdHistoryLog() {
        return idHistoryLog;
    }

    public void setIdHistoryLog(int idHistoryLog) {
        this.idHistoryLog = idHistoryLog;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfile() {
        return profile;
    }

    public int getIdUser() {
        return idUser;
    }

    // Setter Methods

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return "DataLoginUser{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", profile='" + profile + '\'' +
                ", phone=" + phone +
                ", email='" + email + '\'' +
                ", idUser=" + idUser +
                ", idHistoryLog=" + idHistoryLog +
                ", idUserSession=" + idUserSession +
                ", token='" + token + '\'' +
                ", modules=" + modules +
                '}';
    }
}
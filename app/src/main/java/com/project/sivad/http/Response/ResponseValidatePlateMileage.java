package com.project.sivad.http.Response;

public class ResponseValidatePlateMileage {
    private int codeStatus;
    private String message;
    DataValidatePlateMileage data;


    // Getter Methods

    public int getCodeStatus() {
        return codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public DataValidatePlateMileage getData() {
        return data;
    }

    // Setter Methods

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(DataValidatePlateMileage data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseValidatePlateMileage{" +
                "codeStatus=" + codeStatus +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
package com.project.sivad.http.Response;

public class Connections {
    private float id;
    private String code;
    private String url;
    private String urlLogo;
    private float idClient;
    private String createdAt;
    private String updatedAt;


    // Getter Methods

    public float getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlLogo() {
        return urlLogo;
    }

    public float getIdClient() {
        return idClient;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    // Setter Methods

    public void setId(float id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }

    public void setIdClient(float idClient) {
        this.idClient = idClient;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Connections{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", url='" + url + '\'' +
                ", urlLogo='" + urlLogo + '\'' +
                ", idClient=" + idClient +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}

package com.project.sivad.http.Response;

public class DataresponseValidateInspectionEnabled {
    boolean isInspectionEnabled;

    public boolean isEnabled() {
        return isInspectionEnabled;
    }

    public void setEnabled(boolean enabled) {
        isInspectionEnabled = enabled;
    }
}

package com.project.sivad.http.Response;

public class ResponseValideteInspectionEnabled {
    int codeStatus;
    String message;
    DataresponseValidateInspectionEnabled data;

    public int getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataresponseValidateInspectionEnabled getData() {
        return data;
    }

    public void setData(DataresponseValidateInspectionEnabled data) {
        this.data = data;
    }
}

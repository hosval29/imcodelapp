package com.project.sivad.http.Response;

import com.google.gson.JsonArray;

public class ResponseDataTypesVehicularInspectios {
    int codeStatus;
    String message;
    JsonArray data;

    public int getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonArray getData() {
        return data;
    }

    public void setData(JsonArray data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseDataTypesVehicularInspectios{" +
                "codeStatus=" + codeStatus +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}

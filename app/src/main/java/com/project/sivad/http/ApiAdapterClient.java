package com.project.sivad.http;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.annotations.JsonAdapter;
import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseDataNotificationByVehicle;
import com.project.sivad.http.Response.ResponseDataNoveltiesInspections;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseJson;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseSuccessUpdateMeleage;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiAdapterClient {
    private static DBSivad dbSivad;

    public ApiAdapterClient(Context cn) {
        Log.v("cn", cn.getPackageName());
        dbSivad = new DBSivad(cn);
    }

    private static ApiServices API_SERVICES;

    //la variable type define el tipo de peticion de la rura a la cual debe apuntar
    // Clinte o Empresa,
    public static ApiServices apiRestfullMethods() {
        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(10, TimeUnit.SECONDS);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        if (API_SERVICES == null) {


            ResponseDataCode responseDataCode = dbSivad.getDataCodeSetting();
            Log.v("responsecode", responseDataCode.toString());
            if (responseDataCode != null) {
                Log.v("url", responseDataCode.getData().getConnections().getUrl());
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(responseDataCode.getData().getConnections().getUrl())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .client(httpClient.build()) // <-- usamos el log level
                        .build();

                API_SERVICES = retrofit.create(ApiServices.class);
            }

        }

        return API_SERVICES;
    }


    public static Call<ResponseLoginUser> getDataLoginUser(String user, String pass, String imei, String token) {
        return apiRestfullMethods().getDataLoginUser(user, pass, imei, token);
    }

    public static Call<ResponseValidatePlateMileage> getDataPlateMileage(String plate, int mileage) {
        return apiRestfullMethods().getDataPlateMileage(plate, mileage);
    }

    public static Call<ResponseVehicleInspecion> postDataVehicleInspection(JsonObject jsonObjectDataInspection){
        return apiRestfullMethods().postDataVehicleInspection(jsonObjectDataInspection);
    }

    public static Call<ResponseLoginUser> putDataProfile(int idUser, JsonObject jsonObject){
        return apiRestfullMethods().putDataProfile(idUser, jsonObject);
    }

    public Call<ResponseJson> putDataSignOut(int idUser, JsonObject gsonObject) {
        return apiRestfullMethods().putDataSignOut(idUser, gsonObject);
    }

    public Call<ResponseDataNotificationByVehicle> getDataNotificationsByVehicle(int idVehicle) {
        return apiRestfullMethods().getDataNotificationsByVehicle(idVehicle);
    }

    public Call<ResponseDataNoveltiesInspections> getDataNoveltiesInspections(int idVehicle) {
        return apiRestfullMethods().getDataNoveltiesInspections(idVehicle);
    }

    public Call<ResponseJson> postDataAlertByItemBadToggle(JsonObject gsonObject) {
        return apiRestfullMethods().postDataAlertByItemBadToggle(gsonObject);
    }

    public Call<ResponseDataTypesVehicularInspectios> getDataTypesVehicularInspection() {
        return apiRestfullMethods().getDataTypesVehicularInspection();
    }

    public Call<ResponseJson> postDataNoveltiesInspection(JsonObject gsonObject) {
        return apiRestfullMethods().postDataNoveltiesInspection(gsonObject);
    }

    public Call<ResponseValidatePlateMileage> updateMeleage(int idVehicle, JsonObject jsonObject) {
        return apiRestfullMethods().updateMeleage(idVehicle, jsonObject);
    }

    public Call<ResponseValideteInspectionEnabled> getValidateInspectionEnabled(String plateVehicle) {
        return apiRestfullMethods().getValidateInspectionEnabled(plateVehicle);
    }
}

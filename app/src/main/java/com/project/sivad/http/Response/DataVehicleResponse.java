package com.project.sivad.http.Response;

public class DataVehicleResponse {
    int idInspection;
    String dateRegister;

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public int getIdInspection() {
        return idInspection;
    }

    public void setIdInspection(int idInspection) {
        this.idInspection = idInspection;
    }

    @Override
    public String toString() {
        return "DataVehicleResponse{" +
                "idInspection=" + idInspection +
                ", dateRegister='" + dateRegister + '\'' +
                '}';
    }
}

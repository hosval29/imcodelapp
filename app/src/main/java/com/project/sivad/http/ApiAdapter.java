package com.project.sivad.http;

import android.content.Context;
import android.util.Log;

import com.project.sivad.DataBase.DBSivad;
import com.project.sivad.Util.ConstantsSetting.ConstantesRestApi;
import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiAdapter {
    static Context cn;

    public ApiAdapter(Context cn) {
        this.cn = cn;
    }

    private static ApiServices API_SERVICES;

    //la variable type define el tipo de peticion de la rura a la cual debe apuntar
    // Clinte o Empresa,
    public static ApiServices getApiServices() {
        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(10, TimeUnit.SECONDS);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        if (API_SERVICES == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ConstantesRestApi.ROOT_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build()) // <-- usamos el log level
                    .build();

            API_SERVICES = retrofit.create(ApiServices.class);
        }

        return API_SERVICES;
    }

    public static Call<ResponseDataCode> getDataByCodeSetting(String codeSetting) {
        return getApiServices().getDataByCodeSetting(codeSetting);
    }

}

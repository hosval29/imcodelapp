package com.project.sivad.http.Response;

public class ResponseLoginUser {
    private int codeStatus;
    private String message;
    DataLoginUser data;

    // Getter Methods

    public int getCodestatus() {
        return codeStatus;
    }

    public String getMessage() {
        return message;
    }

    public DataLoginUser getData() {
        return data;
    }

    // Setter Methods

    public void setCodeStatus(int codeStatus) {
        this.codeStatus = codeStatus;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(DataLoginUser dataObject) {
        this.data = dataObject;
    }

    @Override
    public String toString() {
        return "ResponseLoginUser{" +
                "codestatus=" + codeStatus +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
package com.project.sivad.http;

import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseDataNotificationByVehicle;
import com.project.sivad.http.Response.ResponseDataNoveltiesInspections;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseJson;
import com.google.gson.JsonObject;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseSuccessUpdateMeleage;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseValideteInspectionEnabled;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiServices {

    @GET("/api/connections")
    Call<ResponseDataCode> getDataByCodeSetting(@Header("codesetting") String codeSetting);

    @GET("/api/security/signin")
    Call<ResponseLoginUser> getDataLoginUser(@Header("user") String user
            , @Header("pass") String pass
            , @Header("imei") String imei
            , @Header("token") String token);

    @GET("/api/vehicles/app/validateplatemileage")
    Call<ResponseValidatePlateMileage> getDataPlateMileage(@Header("plate") String plate
            , @Header("mileage") int mileage);

    @POST("/api/vehiclesinspections")
    Call<ResponseVehicleInspecion> postDataVehicleInspection(@Body JsonObject jsonObject);

    @PUT("/api/users/app/{id}")
    Call<ResponseLoginUser> putDataProfile(@Path("id") int idUser, @Body JsonObject jsonObject);

    @PUT("/api/security/app/signout/{id}")
    Call<ResponseJson> putDataSignOut(@Path("id") int idUser, @Body JsonObject gsonObject);

    @GET("/api/notifications/app/{id}")
    Call<ResponseDataNotificationByVehicle> getDataNotificationsByVehicle(@Path("id") int idVehicle);

    @GET("/api/vehiclesinspections/app/{id}")
    Call<ResponseDataNoveltiesInspections> getDataNoveltiesInspections(@Path("id") int idVehicle);

    @POST("/api/vehiclesinspections/app/alert")
    Call<ResponseJson> postDataAlertByItemBadToggle(@Body JsonObject gsonObject);

    @GET("/api/typesvehicularinspection")
    Call<ResponseDataTypesVehicularInspectios> getDataTypesVehicularInspection();

    @POST("/api/vehiclesinspections/app/novelties")
    Call<ResponseJson> postDataNoveltiesInspection(@Body JsonObject gsonObject);

    @PUT("/api/vehicles/app/update/mileage/{idVehicle}")
    Call<ResponseValidatePlateMileage> updateMeleage(@Path("idVehicle") int idVehicle, @Body JsonObject jsonObject);

    @GET("/api/vehicles/app/validateinspectionenabled/{plateVehicle}")
    Call<ResponseValideteInspectionEnabled> getValidateInspectionEnabled(@Path("plateVehicle") String plateVehicle);
}

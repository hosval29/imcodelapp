package com.project.sivad.http.Response;

public class Data {
    private float id;
    private String client;
    private String nit;
    private String numberPhone;
    private String address;
    private String email;
    private float status;
    private String createdAt;
    private String updatedAt;
    Connections connections;


    // Getter Methods

    public float getId() {
        return id;
    }

    public String getClient() {
        return client;
    }

    public String getNit() {
        return nit;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public float getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Connections getConnections() {
        return connections;
    }

    // Setter Methods

    public void setId(float id) {
        this.id = id;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setStatus(float status) {
        this.status = status;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setConnections(Connections connectionsObject) {
        this.connections = connectionsObject;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", client='" + client + '\'' +
                ", nit='" + nit + '\'' +
                ", numberPhone='" + numberPhone + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", status=" + status +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", ConnectionsObject=" + connections +
                '}';
    }
}

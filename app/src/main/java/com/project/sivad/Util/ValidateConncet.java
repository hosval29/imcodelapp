package com.project.sivad.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.widget.Toolbar;

import com.project.sivad.R;

public class ValidateConncet {
    public static final int NOT_CONNECTED = 0;
    public static final int WIFI = 1;
    public static final int MOBILE = 2;
    //public static Activity ACTIVITY;

    public static int getConnectionStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI && activeNetwork.getState() == NetworkInfo.State.CONNECTED) {
                return WIFI;
            }else if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE && activeNetwork.getState() == NetworkInfo.State.CONNECTED){
                return MOBILE;
            }else{
                return NOT_CONNECTED;
            }
        }
        return NOT_CONNECTED;
    }

}

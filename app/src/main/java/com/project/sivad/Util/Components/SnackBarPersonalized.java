package com.project.sivad.Util.Components;

import android.app.Activity;
import android.view.View;

import com.pd.chocobar.ChocoBar;

public class SnackBarPersonalized {
    private Activity activity;

    InterfacesResponseSnackbarWithAction interfacesShowSnackbarWithAction;

    public interface InterfacesResponseSnackbarWithAction{
        void responseSnackbarWithAction();
    }

    public SnackBarPersonalized(Activity activity){
        super();
        this.activity = activity;
        interfacesShowSnackbarWithAction = (InterfacesResponseSnackbarWithAction) activity;
    }



    public void showSnackBarResponse(int code, String message){
        switch (code){
            case 200:
                ChocoBar.builder().setActivity(activity)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .build()  // in built green ChocoBar
                        .show();
                break;
            case 201:
                ChocoBar.builder().setActivity(activity)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .green()  // in built green ChocoBar
                        .show();
                break;
            case 400:
                ChocoBar.builder().setActivity(activity)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .orange()  // in built green ChocoBar
                        .show();
                break;
            case 404:
                ChocoBar.builder().setActivity(activity)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .orange()  // in built green ChocoBar
                        .show();
                break;
            case 500:
                ChocoBar.builder().setActivity(activity)
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_LONG)
                        .red()  // in built green ChocoBar
                        .show();
                break;
        }

    }

    public void showSnackBarIsConncect(boolean isConnect, String message){

        if (isConnect){
            ChocoBar.builder().setActivity(activity)
                    .setText(message)
                    .setDuration(ChocoBar.LENGTH_LONG)
                    .green()  // in built green ChocoBar
                    .show();

        }else{
            ChocoBar.builder().setActivity(activity)
                    .setText(message)
                    .setDuration(ChocoBar.LENGTH_LONG)
                    .red()  // in built green ChocoBar
                    .show();

        }
    }

    public void showSnackBarErrorCualquiera(String message) {
        ChocoBar.builder().setActivity(activity)
                .setText(message)
                .setTextSize(12)
                .setMaxLines(4)
                .setDuration(ChocoBar.LENGTH_LONG)
                .orange()  // in built green ChocoBar
                .show();
    }


    public void showSnackbarWithAction(int color, String message) {

        switch (color){
            case 0:
                ChocoBar.builder().setActivity(activity)
                        .setActionText("OK")
                        .setActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                interfacesShowSnackbarWithAction.responseSnackbarWithAction();
                            }
                        })
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_INDEFINITE)
                        .build()
                        .show();
                break;
            case 1:
                ChocoBar.builder().setActivity(activity)
                        .setActionText("OK")
                        .setActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                interfacesShowSnackbarWithAction.responseSnackbarWithAction();
                            }
                        })
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_INDEFINITE)
                        .cyan()
                        .show();
                break;
            case 2:
                ChocoBar.builder().setActivity(activity)
                        .setActionText("OK")
                        .setActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                interfacesShowSnackbarWithAction.responseSnackbarWithAction();
                            }
                        })
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_INDEFINITE)
                        .green()
                        .show();
                break;
            case 3:
                ChocoBar.builder().setActivity(activity)
                        .setActionText("OK")
                        .setActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                interfacesShowSnackbarWithAction.responseSnackbarWithAction();
                            }
                        })
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_INDEFINITE)
                        .orange()
                        .show();
                break;
            case 4:
                ChocoBar.builder().setActivity(activity)
                        .setActionText("OK")
                        .setActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                interfacesShowSnackbarWithAction.responseSnackbarWithAction();
                            }
                        })
                        .setText(message)
                        .setDuration(ChocoBar.LENGTH_INDEFINITE)
                        .red()
                        .show();
                break;
        }


    }
}

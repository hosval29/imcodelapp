package com.project.sivad.Util.ConstantsSetting;

public class ConstantsStrings {
    public static final String MODULE_INSPECCION_PREOPERACIONAL_CARGA_FURGONES = "Inspección Carga Furgones";
    public static final String MODULE_INSPECCION_PREOPERACIONAL_NOTIFICATION_ALERT = "Notificación Alerta";
    public static final String MODULE_INSPECCION_PREOPERACIONAL_NOVEDAD_INSPECTION = "Novedad Inspección";
    public static final String TYPENOVELTIESFREE = "Inspección Libre";
    public static final String TYPENOVELTIESTOGGLEBAD = "Novedad Seleecion Malo";
    public static final String NAME_FRAGMENT_ASPECTOS_MECANICOS = "Aspectos Mécanicos";
    public static final String NAME_FRAGMENT_ASPECTOS_ELECTRICOS = "Aspectos Eléctricos";
    public static final String NAME_FRAGMENT_ASPECTOS_GENERALES = "Aspectos Generales";
    public static final String NO_CONEXCION_INTERNET = "Sin conexión a internet. Valide la conexión.";
    public static final String NO_CONEXCION_INTERNET_SERVER = "Sin conexión a internet o con el servidor, valida la conexion a internet y vuelve a intentar.";
    public static final String LOAD = "Cargando...";
    public static final String ERROR_MILEAGE = "El kilometraje no puede ser menor o igual al kilometraje actual!";
    public static final String MODULE_UPDATE_MILEAGE = "Actualización Kilometraje";
    public static final String LOAD_NOTIFICATION_ALERT = "Cargando...\n"  + "Enviando información de la inspección.";
}

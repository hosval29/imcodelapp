package com.project.sivad.Util.ConstantsSetting;

public class ConstantsStringsDB {
    public static final String DATABASE_NAME = "SIVAD";
    public static final int DATABASE_VERSION = 1;

    //Strings para la tabla CodeSetting
    public static final String Table_Prueba = "TablaPrueba";
    public static final String TP_id = "id";
    public static final String TP_dateCreated = "fechaCreacion";
    public static final String TP_data = "data";

    public static final String CREATE_TABLE_Prueba = "CREATE TABLE " + Table_Prueba + "("
            + TP_id + " INTEGER PRIMARY KEY,"//1
            + TP_dateCreated + " TEXT,"//2
            + TP_data + " TEXT)";//3

    //Strings para la tabla CodeSetting
    public static final String Table_Code_Setting = "CodeSetting";
    public static final String TCS_id = "id";
    public static final String TCS_dateCreated = "fechaCreacion";
    public static final String TCS_data = "data";

    public static final String CREATE_TABLE_CODE_SETTING = "CREATE TABLE " + Table_Code_Setting + "("
            + TCS_id + " INTEGER PRIMARY KEY,"//1
            + TCS_dateCreated + " TEXT,"//2
            + TCS_data + " TEXT)";//3

    //Strings para la tabla LoginUser
    public static final String Table_Login_User = "LoginUser";
    public static final String TLU_id = "id";
    public static final String TLU_dateCreated = "dateCreatedAt";
    public static final String TLU_data = "data";

    public static final String CREATE_TABLE_LOGIN_USER = "CREATE TABLE " + Table_Login_User + "("
            + TLU_id + " INTEGER PRIMARY KEY,"//1
            + TLU_dateCreated + " TEXT,"//2
            + TLU_data + " TEXT)";//3

    //Strings para la tabla DataVehicle
    public static final String Table_Data_Vehicle = "DataVehicle";
    public static final String TDV_id = "id";
    public static final String TDV_dateCreated = "dateCreatedAt";
    public static final String TDV_data = "data";

    public static final String CREATE_TABLE_DATA_VEHICLE = "CREATE TABLE " + Table_Data_Vehicle + "("
            + TDV_id + " INTEGER PRIMARY KEY,"//1
            + TDV_dateCreated + " TEXT,"//2
            + TDV_data + " TEXT)";//3

    //Strings para la tabla InspectionPreoperational
    public static final String Table_Inspection_Preoperational = "InspectionPreoperational";
    public static final String TIP_id = "id";
    public static final String TIP_dateCreated = "dateCreatedAt";
    public static final String TIP_idUser = "idUser";
    public static final String TIP_dataAspectosGenerales = "dataAspectosGenerales";
    public static final String TIP_dataAspectosMecanicos = "dataAspectosMecanicos";
    public static final String TIP_dataAspectosElectricos = "dataAspectosElectricos";

    public static final String CREATE_TABLE_INSPECTION_PREOPERATIONAL = "CREATE TABLE " + Table_Inspection_Preoperational + "("
            + TIP_id + " INTEGER PRIMARY KEY,"//1
            + TIP_dateCreated + " TEXT,"//2
            + TIP_idUser + " INTEGER,"//3
            + TIP_dataAspectosGenerales + " TEXT,"//4
            + TIP_dataAspectosMecanicos + " TEXT,"//5
            + TIP_dataAspectosElectricos + " TEXT)";//6


    //Strings para la tabla DataVehicle
    public static final String Table_Offline = "Offline";
    public static final String TOFF_id = "id";
    public static final String TOFF_dateCreated = "dateCreatedAt";
    public static final String TOFF_idUser = "idUser";
    public static final String TOFF_modulo = "modulo";
    public static final String TOFF_data = "data";

    public static final String CREATE_TABLE_OFFLINE = "CREATE TABLE " + Table_Offline + "("
            + TOFF_id + " INTEGER PRIMARY KEY,"//1
            + TOFF_dateCreated + " TEXT,"//2
            + TOFF_idUser + " INTEGER,"//3
            + TOFF_modulo + " TEXT,"//4
            + TOFF_data + " TEXT)";//5

    //tring para la tabla NoveltiesInspection
    public static final String Table_Novelties_Inspection = "NoveltiesInpection";
    public static final String TNI_id = "id";
    public static final String TNI_dateCreated = "dateCreatedAt";
    public static final String TNI_idUser = "idUser";
    public static final String TNI_data = "data";

    public static final String CREATE_TABLE_NOVELTIES_INSPECTION = "CREATE TABLE " + Table_Novelties_Inspection + "("
            + TNI_id + " INTEGER PRIMARY KEY,"//1
            + TNI_dateCreated + " TEXT,"//2
            + TNI_idUser + " INTEGER,"//3
            + TNI_data + " TEXT)";//4

    //tring para la tabla VehiclesInspections
    public static final String Table_Vehicle_Inspections = "VehiclesInspections";
    public static final String TVI_id = "id";
    public static final String TVI_dateCreated = "dateCreatedAt";
    public static final String TVI_idUser = "idUser";
    public static final String TVI_data = "data";

    public static final String CREATE_TABLE_VEHICLE_INSPECTIONS = "CREATE TABLE " + Table_Vehicle_Inspections + "("
            + TVI_id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"//1
            + TVI_dateCreated + " TEXT,"//2
            + TVI_idUser + " INTEGER,"//3
            + TVI_data + " TEXT)";//4

    //tring para la tabla TypesVehicularInspectios
    public static final String Table_Types_Vehicular_Inspectios = "TypesVehicularInspectios";
    public static final String TTVI_id = "id";
    public static final String TTVI_dateCreated = "dateCreatedAt";
    public static final String TTVI_data = "data";

    public static final String CREATE_TABLE_TYPES_VEHICULAR_INSPECTIONS = "CREATE TABLE " + Table_Types_Vehicular_Inspectios + "("
            + TVI_id + " INTEGER PRIMARY KEY,"//1
            + TVI_dateCreated + " TEXT,"//2
            + TVI_data + " TEXT)";//4


    //String para la tabla AlertNotificationEmailResponse
    public static final String Table_Alert_Notification_Email_Response = "AlertNotificationEmailResponse";
    public static final String TANER_id = "id";
    public static final String TANER_idUser = "idUser";
    public static final String TANER_dateCreated = "dateCreatedAt";
    public static final String TANER_data = "data";

    public static final String CREATE_TABLE_ALERT_NOTIFICATION_EMAIL_RESPONSE = "CREATE TABLE " + Table_Alert_Notification_Email_Response + "("
            + TANER_id + " INTEGER PRIMARY KEY,"//1
            + TANER_dateCreated + " TEXT,"//2
            + TANER_idUser + " INTEGER,"//2
            + TANER_data + " TEXT)";//4

}

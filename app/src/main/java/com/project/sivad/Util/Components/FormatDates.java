package com.project.sivad.Util.Components;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDates {
    private Date date;
    private SimpleDateFormat ft;

    public String getDateRegisterFormatDateWithTime(){
        String dateRegister = "";
        date = new Date();
        ft = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        dateRegister = ft.format(date).toString();
        return  dateRegister;
    }

    public String getDataRegisterFotmatDateLLLL(){
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String dateCreated = ft.format(date).toString();
        return  dateCreated;
    }
}

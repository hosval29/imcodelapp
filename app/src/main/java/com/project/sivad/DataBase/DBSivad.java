package com.project.sivad.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.project.sivad.Configuracion.Menu.Vendor.Pojo.PojoDataOffline;
import com.project.sivad.Modulos.InspeccionPreoperacional.Pojo.PojoNoveltieInspection;
import com.project.sivad.Util.ConstantsSetting.ConstantsStrings;
import com.project.sivad.Util.ConstantsSetting.ConstantsStringsDB;
import com.project.sivad.http.Response.ResponseDataCode;
import com.project.sivad.http.Response.ResponseDataTypesVehicularInspectios;
import com.project.sivad.http.Response.ResponseLoginUser;
import com.project.sivad.http.Response.ResponseValidatePlateMileage;
import com.project.sivad.http.Response.ResponseVehicleInspecion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DBSivad extends SQLiteOpenHelper {

    public DBSivad(Context context) {
        super(context, ConstantsStringsDB.DATABASE_NAME, null, ConstantsStringsDB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(ConstantsStringsDB.CREATE_TABLE_CODE_SETTING);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_LOGIN_USER);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_DATA_VEHICLE);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_INSPECTION_PREOPERATIONAL);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_NOVELTIES_INSPECTION);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_OFFLINE);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_VEHICLE_INSPECTIONS);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_TYPES_VEHICULAR_INSPECTIONS);
        db.execSQL(ConstantsStringsDB.CREATE_TABLE_ALERT_NOTIFICATION_EMAIL_RESPONSE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Eiminamos tablas anteriores si existen
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Code_Setting);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Login_User);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Data_Vehicle);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Inspection_Preoperational);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Novelties_Inspection);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Offline);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Vehicle_Inspections);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Types_Vehicular_Inspectios);
        db.execSQL("DROP TABLE IF EXISTS " + ConstantsStringsDB.Table_Alert_Notification_Email_Response);

        // re-iniciamos metodo principal
        onCreate(db);
    }


    public Long addDataCodeSetting(String dateCreatedAt, String data) {
        Long id = 12345678910L;

        SQLiteDatabase db = this.getWritableDatabase();

        //Creamos un Object tipo ContentValues para mapear los valores a insertar
        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TCS_dateCreated, dateCreatedAt);//1
        values.put(ConstantsStringsDB.TCS_data, data);//2

        try {
            //Insertamos datos
            id = db.insert(ConstantsStringsDB.Table_Code_Setting, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public int deleteDataCodeSetting() {
        int id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        //elimanacion de las filas
        id = db.delete(ConstantsStringsDB.Table_Code_Setting, null, null);

        //Cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public ResponseDataCode getDataCodeSetting() {
        ResponseDataCode responseDataCode = null;

        //Abrimos la conexion
        SQLiteDatabase db = this.getReadableDatabase();

        //String que describe la consulta
        String sql = "SELECT " + ConstantsStringsDB.TCS_data + " FROM " + ConstantsStringsDB.Table_Code_Setting;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                Gson gson = new GsonBuilder().create();
                String dataCodeSetting = c.getString(c.getColumnIndex(ConstantsStringsDB.TCS_data));
                if (dataCodeSetting.length() > 0) {
                    responseDataCode = gson.fromJson(dataCodeSetting, ResponseDataCode.class);
                } else {
                    responseDataCode = null;
                }


            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return responseDataCode;
    }

    public Long addDataLoginUser(String dateCreatedAt, String data) {
        Long id = 12345678910L;

        SQLiteDatabase db = this.getWritableDatabase();

        //Creamos un Object tipo ContentValues para mapear los valores a insertar
        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TLU_dateCreated, dateCreatedAt);//1
        values.put(ConstantsStringsDB.TLU_data, data);//2

        try {
            //Insertamos datos
            id = db.insert(ConstantsStringsDB.Table_Login_User, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public int deleteDataLoginUser() {
        int id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        //elimanacion de las filas
        id = db.delete(ConstantsStringsDB.Table_Login_User, "1", null);

        //Cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public ResponseLoginUser getDataLoginUser() {
        ResponseLoginUser responseLoginUser = null;

        //Abrimos la conexion
        SQLiteDatabase db = this.getReadableDatabase();

        //String que describe la consulta
        String sql = "SELECT " + ConstantsStringsDB.TLU_data + " FROM " + ConstantsStringsDB.Table_Login_User;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                Gson gson = new GsonBuilder().create();
                String dataLoginUser = c.getString(c.getColumnIndex(ConstantsStringsDB.TLU_data));
                if (dataLoginUser.length() > 0) {
                    responseLoginUser = gson.fromJson(dataLoginUser, ResponseLoginUser.class);
                } else {
                    responseLoginUser = null;
                }


            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return responseLoginUser;
    }

    /**
     * Estos son los metodos para la tabla DataVehicle
     **/

    public Long addDataVehicle(String dateCreatedAt, String data) {
        Long id = 12345678910L;

        SQLiteDatabase db = this.getWritableDatabase();

        //Creamos un Object tipo ContentValues para mapear los valores a insertar
        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TDV_dateCreated, dateCreatedAt);//1
        values.put(ConstantsStringsDB.TDV_data, data);//2

        try {
            //Insertamos datos
            id = db.insert(ConstantsStringsDB.Table_Data_Vehicle, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public int deleteDataVehicle() {
        int id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        //elimanacion de las filas
        id = db.delete(ConstantsStringsDB.Table_Data_Vehicle, "1", null);

        //Cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public ResponseValidatePlateMileage getDataVehicle() {
        ResponseValidatePlateMileage responseValidatePlateMileage = null;

        //Abrimos la conexion
        SQLiteDatabase db = this.getReadableDatabase();

        //String que describe la consulta
        String sql = "SELECT " + ConstantsStringsDB.TDV_data + " FROM " + ConstantsStringsDB.Table_Data_Vehicle;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                Gson gson = new GsonBuilder().create();
                String dataVehicle = c.getString(c.getColumnIndex(ConstantsStringsDB.TDV_data));
                if (dataVehicle.length() > 0) {
                    responseValidatePlateMileage = gson.fromJson(dataVehicle, ResponseValidatePlateMileage.class);
                } else {
                    responseValidatePlateMileage = null;
                }


            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return responseValidatePlateMileage;
    }

    /**
     *Estos son los metodos de la TablaInspectionPreoperational
     */
    public Long addDataInspection(String dateCreatedAt
            , String idUser
            , String dataAspectosMecanicos
            , String dataAspectosElectricos
            , String dataAspectosGenreales) {

        Long id = 1234567890L;

        SQLiteDatabase db = this.getWritableDatabase();

        //Creamos un Object tipo ContentValues para mapear los valores a insertar
        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TIP_dateCreated, dateCreatedAt);//1
        values.put(ConstantsStringsDB.TIP_idUser, idUser);//2
        values.put(ConstantsStringsDB.TIP_dataAspectosMecanicos, dataAspectosMecanicos);//2
        values.put(ConstantsStringsDB.TIP_dataAspectosElectricos, dataAspectosElectricos);//2
        values.put(ConstantsStringsDB.TIP_dataAspectosGenerales, dataAspectosGenreales);//2

        try {
            //Insertamos datos
            id = db.insert(ConstantsStringsDB.Table_Inspection_Preoperational, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //cerramos conexion a la base de datos
        db.close();

        return id;
    }

    public int deleteDataInspection(int idUser) {
        int id = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        id = db.delete(ConstantsStringsDB.Table_Inspection_Preoperational, ConstantsStringsDB.TIP_idUser + "=" + idUser, null);

        db.close();

        return id;
    }

    public int deleteDataInspectionById(int idInspection) {
        int id = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        id = db.delete(ConstantsStringsDB.Table_Inspection_Preoperational, ConstantsStringsDB.TIP_id + "=" + idInspection, null);

        db.close();

        return id;
    }

    public int getLastId() {
        int _id = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(ConstantsStringsDB.Table_Inspection_Preoperational, new String[]{ConstantsStringsDB.TIP_id}, null, null, null, null, null);
        if (cursor.moveToLast()) {
            _id = cursor.getInt(0);
        }
        cursor.close();
        db.close();
        return _id;
    }

    public int getNumRowsOfInspections(int idUser) {
        int numRows = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * " +
                        " FROM " + ConstantsStringsDB.Table_Inspection_Preoperational +
                        " WHERE " + ConstantsStringsDB.TIP_idUser + "=" + idUser
                , null);

        Log.i("Number of registers", " :: " + c.getCount());
        numRows = c.getCount();

        return numRows;
    }

    public int updateDataInspectionAspectosElectricos(String dataUpdate, int idInspection, int idUser) {
        int id = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TIP_dataAspectosElectricos, dataUpdate);

        id = db.update(ConstantsStringsDB.Table_Inspection_Preoperational
                , values
                , ConstantsStringsDB.TIP_id + "=" + idInspection + " AND " + ConstantsStringsDB.TIP_idUser + "=" + idUser
                , null);

        db.close();

        return id;
    }

    public int updateDataInspectionAspectosGenerales(String dataUpdate, int idInspection, int idUser) {
        int id = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TIP_dataAspectosGenerales, dataUpdate);

        id = db.update(ConstantsStringsDB.Table_Inspection_Preoperational
                , values
                , ConstantsStringsDB.TIP_id + "=" + idInspection + " AND " + ConstantsStringsDB.TIP_idUser + "=" + idUser
                , null);

        db.close();

        return id;
    }

    public String getLastRegistrationDateInspectionPreoperational(int idUser) {
        String dateRegister = "";

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT " + ConstantsStringsDB.TIP_dateCreated
                + " FROM " + ConstantsStringsDB.Table_Inspection_Preoperational
                + " WHERE " + ConstantsStringsDB.TIP_idUser + "=" + idUser
                + " ORDER BY " + ConstantsStringsDB.TIP_id
                + " DESC LIMIT 1";

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                dateRegister = c.getString(c.getColumnIndex(ConstantsStringsDB.TIP_dateCreated));
            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return dateRegister;

    }

    public int getIdLastInspectionPreoperational(int idUser) {
        int id = 0;

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT " + ConstantsStringsDB.TIP_id
                + " FROM " + ConstantsStringsDB.Table_Inspection_Preoperational
                + " WHERE " + ConstantsStringsDB.TIP_idUser + "=" + idUser
                + " ORDER BY " + ConstantsStringsDB.TIP_id
                + " DESC LIMIT 1";

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                id = c.getInt(c.getColumnIndex(ConstantsStringsDB.TIP_id));
            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return id;

    }

    public JSONArray getAllDataInspection(int idUser, int idInspection) {
        JSONArray dataInspection = new JSONArray();

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT * " +
                " FROM " + ConstantsStringsDB.Table_Inspection_Preoperational +
                " WHERE " + ConstantsStringsDB.TIP_idUser + "=" + idUser +
                " AND " + ConstantsStringsDB.TIP_id + "=" + idInspection;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                try {
                    //Log.v("dataAspectosEledtricos", c.getString(c.getColumnIndex(ConstantsStringsDB.TIP_dataAspectosElectricos)));
                    System.out.println("dataAspectosEledtricos" + c.getString(c.getColumnIndex(ConstantsStringsDB.TIP_dataAspectosElectricos)));
                    JSONArray jsonArrayApectosMecanicos = new JSONArray(c.getString(c.getColumnIndex(ConstantsStringsDB.TIP_dataAspectosMecanicos)));
                    JSONArray jsonArrayAspectosElectricos = new JSONArray(c.getString(c.getColumnIndex(ConstantsStringsDB.TIP_dataAspectosElectricos)));
                    JSONArray jsonArrayAspectosGenerales = new JSONArray(c.getString(c.getColumnIndex(ConstantsStringsDB.TIP_dataAspectosGenerales)));

                    JSONObject jsonObjectAspectosMecanicos = new JSONObject();
                    JSONObject jsonObjectAspectosElectricos = new JSONObject();
                    JSONObject jsonObjectAspectosGenerales = new JSONObject();

                    jsonObjectAspectosMecanicos.put("Aspectos Mécanicos", jsonArrayApectosMecanicos);
                    jsonObjectAspectosElectricos.put("Aspectos Eléctricos", jsonArrayAspectosElectricos);
                    jsonObjectAspectosGenerales.put("Aspectos Generales", jsonArrayAspectosGenerales);

                    dataInspection.put(jsonObjectAspectosMecanicos);
                    dataInspection.put(jsonObjectAspectosElectricos);
                    dataInspection.put(jsonObjectAspectosGenerales);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return dataInspection;
    }

    /**
     * Estos son los metodo para la tabla Offline
     */

    public Long addDataOffline(String dateCreatedAt, int idUser, String module, String data) {
        Long id = 1234567890L;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TOFF_dateCreated, dateCreatedAt);
        values.put(ConstantsStringsDB.TOFF_idUser, idUser);
        values.put(ConstantsStringsDB.TOFF_modulo, module);
        values.put(ConstantsStringsDB.TOFF_data, data);

        try {
            id = db.insert(ConstantsStringsDB.Table_Offline, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();

        return id;
    }

    public List<PojoDataOffline> getDataOffline(int idUser) {
        List<PojoDataOffline> pojoDataOfflineList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "SELECT * FROM " + ConstantsStringsDB.Table_Offline + " WHERE " + ConstantsStringsDB.TOFF_idUser + "=" + idUser;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {

                PojoDataOffline pojoDataOffline = new PojoDataOffline(c.getInt(c.getColumnIndex(ConstantsStringsDB.TOFF_id))
                        , c.getString(c.getColumnIndex(ConstantsStringsDB.TOFF_dateCreated))
                        , c.getString(c.getColumnIndex(ConstantsStringsDB.TOFF_modulo))
                        , c.getString(c.getColumnIndex(ConstantsStringsDB.TOFF_data)));
                pojoDataOfflineList.add(pojoDataOffline);
            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();

        return pojoDataOfflineList;
    }


    public int getIdByModule(String module) {
        int id = 0;

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT " + ConstantsStringsDB.TOFF_id +
                " FROM " + ConstantsStringsDB.Table_Offline +
                " WHERE " + ConstantsStringsDB.TOFF_modulo + "='" + module+"'";

        Log.v("sql offline", sql);

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                id = c.getInt(c.getColumnIndex(ConstantsStringsDB.TOFF_id));

            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();

        return id;
    }

    public int deleteDataOfflineById(int idRegister) {
        int rowFilesAffect = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        rowFilesAffect = db.delete(ConstantsStringsDB.Table_Offline, ConstantsStringsDB.TOFF_id + "=" + idRegister, null);

        db.close();

        return rowFilesAffect;
    }


    /**
     * Estos son los metodo para la tabla NoveltiesInspection
     */
    public Long addDataNoveltiesInspection(String dateCreatedAt, int idUser, String data) {

        Long result = 1234567890L;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TNI_dateCreated, dateCreatedAt);
        values.put(ConstantsStringsDB.TNI_idUser, idUser);
        values.put(ConstantsStringsDB.TNI_data, data);

        try {
            result = db.insert(ConstantsStringsDB.Table_Novelties_Inspection, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();

        return result;
    }

    public int deleteDataNoveltiesInspection() {
        int numFilesDeletes = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        numFilesDeletes = db.delete(ConstantsStringsDB.Table_Novelties_Inspection, null, null);

        db.close();

        return numFilesDeletes;
    }

    public List<PojoNoveltieInspection> getDataNoveltiesInspection(int idUser) {
        List<PojoNoveltieInspection> pojoNoveltieInspectionList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT " + ConstantsStringsDB.TNI_data
                + " FROM " + ConstantsStringsDB.Table_Novelties_Inspection
                + " WHERE " + ConstantsStringsDB.TNI_idUser
                + "=" + idUser;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                String dataNoveltiesInspection = c.getString(c.getColumnIndex(ConstantsStringsDB.TNI_data));
                if (dataNoveltiesInspection.length() > 0) {
                    ObjectMapper objectMapper = new ObjectMapper();

                    try {
                        pojoNoveltieInspectionList = Arrays.asList(objectMapper.readValue(dataNoveltiesInspection, PojoNoveltieInspection[].class));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    pojoNoveltieInspectionList = null;
                }


            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return pojoNoveltieInspectionList;
    }

    public String getDataNoveltiesInspectionPost(int idUser) {
        String dataNoveltiesInspection = "";

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT " + ConstantsStringsDB.TNI_data
                + " FROM " + ConstantsStringsDB.Table_Novelties_Inspection
                + " WHERE " + ConstantsStringsDB.TNI_idUser
                + "=" + idUser;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                dataNoveltiesInspection = c.getString(c.getColumnIndex(ConstantsStringsDB.TNI_data));
            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return dataNoveltiesInspection;
    }

    /**
     * Estos son los metodos para la tabla VehicleInspecions
     */

    public Long addDataVehicleInspection(String dateCreatedAt, int idUser, String data) {
        Long rowAffected = 1234567890L;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TVI_dateCreated, dateCreatedAt);
        values.put(ConstantsStringsDB.TVI_idUser, idUser);
        values.put(ConstantsStringsDB.TVI_data, data);

        try {
            rowAffected = db.insert(ConstantsStringsDB.Table_Vehicle_Inspections, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();

        return rowAffected;
    }

    public ResponseVehicleInspecion getDataVehicleInspection(int idUser) {
        ResponseVehicleInspecion responseVehicleInspecion = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT * FROM "
                + ConstantsStringsDB.Table_Vehicle_Inspections
                + " WHERE " + ConstantsStringsDB.TVI_idUser + "="
                + idUser;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                Gson gson = new GsonBuilder().create();
                String dataVehicleInspection = c.getString(c.getColumnIndex(ConstantsStringsDB.TDV_data));
                if (dataVehicleInspection.length() > 0) {
                    responseVehicleInspecion = gson.fromJson(dataVehicleInspection, ResponseVehicleInspecion.class);
                } else {
                    responseVehicleInspecion = null;
                }


            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return responseVehicleInspecion;
    }

    public int deleteDataVehicleInspection(int idUser) {
        int rowFilesAffect = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        rowFilesAffect = db.delete(ConstantsStringsDB.Table_Vehicle_Inspections, ConstantsStringsDB.TVI_idUser + "=" + idUser, null);

        db.close();

        return rowFilesAffect;
    }

    public String getDateRegisterVehicleInspection(int idUser) {
        String dateRegister = "";

        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT " + ConstantsStringsDB.TVI_dateCreated
                + " FROM " + ConstantsStringsDB.Table_Vehicle_Inspections
                + " WHERE " + ConstantsStringsDB.TVI_idUser + "=" + idUser
                + " ORDER BY " + ConstantsStringsDB.TVI_id
                + " DESC LIMIT 1";

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                dateRegister = c.getString(c.getColumnIndex(ConstantsStringsDB.TVI_dateCreated));
            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return dateRegister;

    }


    /**
     * Estos son los metodos para la tabla TypesVehicularInspectios
     */

    public Long addDataTypesVehicularInspectios(String dateCreatedAt, String data) {
        Long rowAffected = 1234567890L;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TTVI_dateCreated, dateCreatedAt);
        values.put(ConstantsStringsDB.TTVI_data, data);

        try {
            rowAffected = db.insert(ConstantsStringsDB.Table_Types_Vehicular_Inspectios, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();

        return rowAffected;
    }

    public ResponseDataTypesVehicularInspectios getDataTypesVehicularInspection() {
        ResponseDataTypesVehicularInspectios responseDataTypesVehicularInspectios = null;

        //Abrimos la conexion
        SQLiteDatabase db = this.getReadableDatabase();

        //String que describe la consulta
        String sql = "SELECT " + ConstantsStringsDB.TTVI_data + " FROM " + ConstantsStringsDB.Table_Types_Vehicular_Inspectios;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                Gson gson = new GsonBuilder().create();
                String dataVehicle = c.getString(c.getColumnIndex(ConstantsStringsDB.TTVI_data));
                if (dataVehicle.length() > 0) {
                    responseDataTypesVehicularInspectios = gson.fromJson(dataVehicle, ResponseDataTypesVehicularInspectios.class);
                } else {
                    responseDataTypesVehicularInspectios = null;
                }


            } while (c.moveToNext());
        }
        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return responseDataTypesVehicularInspectios;
    }


    public int deleteDataTypesVehicularInspection() {
        int rowFilesAffect = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        rowFilesAffect = db.delete(ConstantsStringsDB.Table_Types_Vehicular_Inspectios, null, null);

        db.close();

        return rowFilesAffect;
    }

    /**
     * Estos los metododos para la tabla Table_Alert_Notification_Email_Response
     */

    public Long addDataResponseAlertNotificationEmail(int idUser, String dateCreatedAt, String data) {
        Long rowAffected = 1234567890L;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsStringsDB.TANER_idUser, idUser);
        values.put(ConstantsStringsDB.TANER_dateCreated, dateCreatedAt);
        values.put(ConstantsStringsDB.TANER_data, data);

        try {
            rowAffected = db.insert(ConstantsStringsDB.Table_Alert_Notification_Email_Response, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();

        return rowAffected;
    }

    public String getDateResponseAlertNotificationEmail(int idUser) {
        String date = "";

        SQLiteDatabase db = this.getReadableDatabase();

        //String que describe la consulta
        String sql = "SELECT " + ConstantsStringsDB.TANER_dateCreated
                + " FROM "
                + ConstantsStringsDB.Table_Alert_Notification_Email_Response
                + " WHERE " + ConstantsStringsDB.TANER_idUser + "=" + idUser;

        //Cursor que realiza la consulta
        Cursor c = db.rawQuery(sql, null);

        //Recorremos el resultado del cursor y seteamos el valor a idPozo
        if (c.moveToFirst()) {
            do {
                date = c.getString(c.getColumnIndex(ConstantsStringsDB.TANER_dateCreated));
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();

        //Cerramos la conexion
        db.close();
        //retornamos la idPozo
        return date;
    }

    public int deleteDataResponseAlertNotificationEmail(int idUser) {
        int rowFilesAffect = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        rowFilesAffect = db.delete(ConstantsStringsDB.Table_Alert_Notification_Email_Response, ConstantsStringsDB.TANER_idUser + "=" + idUser, null);

        db.close();

        return rowFilesAffect;
    }

}
